import getAllTeachersAPI from './getAllTeachers';

const getAllTeachersDropdownAPI = (): Promise<any[]> => {
    return getAllTeachersAPI({}).then(data => data.map(x => ({
        label: `${x.lastName} ${x.firstName} ${x.patronymic}`,
        id: x.id
    })))
}

export default getAllTeachersDropdownAPI;