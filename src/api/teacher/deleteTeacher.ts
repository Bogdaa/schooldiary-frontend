import axiosInstance from '../index';

const deleteTeacherAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/teacher/delete/${id}`)
        .then(res => id);
};

export default deleteTeacherAPI;