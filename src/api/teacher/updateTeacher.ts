import axiosInstance from '../index';
import {AxiosResponse} from 'axios';
import {UpdateTeacherModel} from '../../pages/TeacherList/types/UpdateTeacherModel';
import {GetSingleTeacherModel} from '../../pages/TeacherList/types/GetSingleTeacherModel';

const updateTeacherAPI = (body: UpdateTeacherModel, id: number) => {
    return axiosInstance.patch<UpdateTeacherModel, AxiosResponse<GetSingleTeacherModel>>(`/api/teacher/edit/${id}`, body)
        .then(res => res.data);
}

export default updateTeacherAPI;