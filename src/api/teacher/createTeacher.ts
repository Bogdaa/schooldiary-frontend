import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {GetTeacherModel} from '../../pages/TeacherList/types/GetTeacherModel';

type CreateTeacherRequestBody = {
    user: {
        email: string;
        password: string;
    }
    teacher: {
        firstName: string;
        lastName: string;
        patronymic: string
    }
}

const createTeacherAPI = (body: CreateTeacherRequestBody): Promise<GetTeacherModel> => {
    return axiosInstance.post<CreateTeacherRequestBody, AxiosResponse<GetTeacherModel>>('/api/user/createTeacher', body)
        .then(res => res.data);
};

export default createTeacherAPI;