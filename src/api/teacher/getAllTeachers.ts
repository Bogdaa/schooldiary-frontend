import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListTeacherModel} from '../../pages/TeacherList/types/ListTeacherModel';
import {TeacherFilters} from '../../pages/TeacherList/types/TeacherFilters';

const getAllTeachersAPI = (filters: TeacherFilters): Promise<ListTeacherModel[]> => {
    return axiosInstance.post<TeacherFilters, AxiosResponse<ListTeacherModel[]>>('/api/teacher/search', filters)
        .then(res => res.data);
};

export default getAllTeachersAPI;