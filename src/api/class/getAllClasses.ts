import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListClassModel} from '../../pages/ClassList/types/ListClassModel';

const getAllClassesAPI = (): Promise<ListClassModel[]> => {
    return axiosInstance.get<any, AxiosResponse<ListClassModel[]>>('/api/studyClass/getAll')
        .then(res => res.data);
};

export default getAllClassesAPI;