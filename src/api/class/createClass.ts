import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {CreateClassModel} from '../../pages/ClassList/types/CreateClassModel';
import {GetClassModel} from '../../pages/ClassList/types/GetClassModel';

const createClassAPI = (body: CreateClassModel): Promise<GetClassModel> => {
    return axiosInstance.post<CreateClassModel, AxiosResponse<GetClassModel>>('/api/studyClass/create', body)
        .then(res => res.data);
};

export default createClassAPI;