import axiosInstance from '../index';

const deleteClassAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/studyClass/delete/${id}`)
        .then(res => id);
};

export default deleteClassAPI;