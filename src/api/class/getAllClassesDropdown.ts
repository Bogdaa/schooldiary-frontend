import getAllClassesAPI from './getAllClasses';

const getAllClassesDropdownAPI = (): Promise<any> => {
    return getAllClassesAPI().then(data => data.map(x => ({
        id: x.id,
        label: x.name
    })))
}

export default getAllClassesDropdownAPI;