import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {GetScheduleModel} from '../../pages/ScheduleList/types/GetScheduleModel';

const updateScheduleAPI = (body: any, id: number): Promise<GetScheduleModel> => {
    return axiosInstance.patch<any, AxiosResponse<any>>(`/api/schedule/edit/${id}`, body)
        .then(res => res.data);
};

export default updateScheduleAPI;