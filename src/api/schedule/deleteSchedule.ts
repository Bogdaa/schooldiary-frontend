import axiosInstance from '../index';

const deleteScheduleAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/schedule/delete/${id}`)
        .then(res => id);
};

export default deleteScheduleAPI;