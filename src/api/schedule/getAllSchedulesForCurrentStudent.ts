import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListScheduleModel} from '../../pages/ScheduleList/types/ListScheduleModel';

const getAllSchedulesForCurrentStudentAPI = (): Promise<ListScheduleModel[]> => {
    return axiosInstance.post<any, AxiosResponse<ListScheduleModel[]>>('/api/schedule/currentStudentSearch')
        .then(res => res.data);
};

export default getAllSchedulesForCurrentStudentAPI;