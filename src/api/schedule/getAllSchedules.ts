import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListScheduleModel} from '../../pages/ScheduleList/types/ListScheduleModel';
import {ScheduleFilters} from '../../pages/ScheduleList/types/ScheduleFilters';

const getAllSchedulesAPI = (filters: ScheduleFilters): Promise<ListScheduleModel[]> => {
    return axiosInstance.post<any, AxiosResponse<ListScheduleModel[]>>('/api/schedule/search', filters)
        .then(res => res.data);
};

export default getAllSchedulesAPI;