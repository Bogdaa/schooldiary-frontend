import {AxiosResponse} from 'axios';
import axiosInstance from '../index';

const getSingleStudentAPI = (id: number): Promise<any> => {
    return axiosInstance.get<any, AxiosResponse<any>>(`/api/schedule/edit/${id}`)
        .then(res => res.data);
};

export default getSingleStudentAPI;