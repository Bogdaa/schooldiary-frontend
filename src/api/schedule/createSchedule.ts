import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {CreateScheduleModel} from '../../pages/ScheduleList/types/CreateScheduleModel';
import {GetScheduleModel} from '../../pages/ScheduleList/types/GetScheduleModel';

const createScheduleAPI = (body: CreateScheduleModel): Promise<GetScheduleModel> => {
    return axiosInstance.post<CreateScheduleModel, AxiosResponse<GetScheduleModel>>('/api/schedule/create', body)
        .then(res => res.data);
};

export default createScheduleAPI;