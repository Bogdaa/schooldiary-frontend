import getAllSubjectsAPI from './getAllSubjects';

const getAllSubjectsDropdownAPI = (): Promise<any> => {
    return getAllSubjectsAPI({}).then(data => data.map(x => ({
        id: x.id,
        label: x.name
    })))
}

export default getAllSubjectsDropdownAPI;