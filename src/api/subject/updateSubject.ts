import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {GetSubjectModel} from '../../pages/SubjectList/types/GetSubjectModel';
import {CreateSubjectModel} from '../../pages/SubjectList/types/CreateSubjectModel';

const updateSubjectAPI = (body: CreateSubjectModel, id: number): Promise<GetSubjectModel> => {
    return axiosInstance.patch<CreateSubjectModel, AxiosResponse<GetSubjectModel>>(`/api/subject/edit/${id}`, body)
        .then(res => res.data);
};

export default updateSubjectAPI;