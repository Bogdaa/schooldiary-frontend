import axiosInstance from '../index';

const deleteSubjectAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/subject/${id}`)
        .then(res => id);
};

export default deleteSubjectAPI;