import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {GetSubjectModel} from '../../pages/SubjectList/types/GetSubjectModel';

const createSubjectAPI = (body: any): Promise<GetSubjectModel> => {
    return axiosInstance.post<any, AxiosResponse<GetSubjectModel>>('/api/subject/create', body)
        .then(res => res.data);
};

export default createSubjectAPI;