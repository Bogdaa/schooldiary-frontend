import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListSubjectModel} from '../../pages/SubjectList/types/ListSubjectModel';
import {SubjectFilters} from '../../pages/SubjectList/types/SubjectFilters';

const getAllSubjectsAPI = (filters: SubjectFilters): Promise<ListSubjectModel[]> => {
    return axiosInstance.post<SubjectFilters, AxiosResponse<ListSubjectModel[]>>('/api/subject/search', filters)
        .then(res => res.data);
};

export default getAllSubjectsAPI;