import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {CreateDateScheduleModel} from '../../pages/ScheduleList/types/CreateDateScheduleModel';
import {GetDateScheduleModel} from '../../pages/ScheduleList/types/GetDateScheduleModel';

const createDateScheduleAPI = (body: CreateDateScheduleModel) => {
    return axiosInstance.post<CreateDateScheduleModel, AxiosResponse<GetDateScheduleModel>>('/api/dateSchedule/create', body)
        .then(res => res.data)
}

export default createDateScheduleAPI;