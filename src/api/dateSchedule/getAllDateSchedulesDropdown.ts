import getAllDateSchedulesAPI from './getAllDateSchedules';
import dayjs from 'dayjs';

const getAllDateSchedulesDropdown = (): Promise<any> => {
    return getAllDateSchedulesAPI().then(data => data.map(x => ({
        label: dayjs(x.date).format('YYYY-MM-DD'),
        id: x.id
    })))
}

export default getAllDateSchedulesDropdown;