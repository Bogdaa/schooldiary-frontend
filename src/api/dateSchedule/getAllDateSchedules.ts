import axiosInstance from '../index';
import {AxiosResponse} from 'axios';

interface ListDateScheduleModel {
    id: number;
    date: string;
    schedule: {
        id: number;
        lessonNumber: number;
        weekday: string;
    } | null,
    homework: Array<{ id: number; description: string; deadline: string}>
}

const getAllDateSchedulesAPI = (): Promise<ListDateScheduleModel[]> => {
    return axiosInstance.get<any, AxiosResponse<ListDateScheduleModel[]>>('/api/dateSchedule/getAll')
        .then(res => res.data)
}

export default getAllDateSchedulesAPI;