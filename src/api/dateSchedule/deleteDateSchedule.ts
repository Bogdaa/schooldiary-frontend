import axiosInstance from '../index';

const deleteDateScheduleAPI = (id: number) => {
    return axiosInstance.delete(`/api/dateSchedule/delete/${id}`)
        .then(res => id)
}

export default deleteDateScheduleAPI;