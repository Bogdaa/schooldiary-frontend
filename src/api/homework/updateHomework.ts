import axiosInstance from '../index';
import {UpdateHomeworkModel} from '../../pages/HomeworksList/types/UpdateHomeworkModel';
import {GetHomeworkModel} from '../../pages/HomeworksList/types/GetHomeworkModel';

const updateHomeworkAPI = (data: UpdateHomeworkModel, id: number) => {
    return axiosInstance.patch<GetHomeworkModel>(`/api/homework/edit/${id}`, data)
        .then(res => res.data)
}

export default updateHomeworkAPI;