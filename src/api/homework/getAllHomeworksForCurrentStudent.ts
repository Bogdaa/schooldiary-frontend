import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListHomeworkModel} from '../../pages/HomeworksList/types/ListHomeworkModel';

const getAllHomeworksForCurrentStudentAPI = (): Promise<ListHomeworkModel[]> => {
    return axiosInstance.get<any, AxiosResponse<ListHomeworkModel[]>>('/api/homework/getAllForCurrentStudent')
        .then(res => res.data);
};

export default getAllHomeworksForCurrentStudentAPI;