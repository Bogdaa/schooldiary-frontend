import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {CreateHomeworkModel} from '../../pages/HomeworksList/types/CreateHomeworkModel';
import {GetHomeworkModel} from '../../pages/HomeworksList/types/GetHomeworkModel';

const createHomeworkAPI = (body: CreateHomeworkModel): Promise<GetHomeworkModel> => {
    return axiosInstance.post<CreateHomeworkModel, AxiosResponse<GetHomeworkModel>>('/api/homework/create', body)
        .then(res => res.data);
};

export default createHomeworkAPI;