import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListHomeworkModel} from '../../pages/HomeworksList/types/ListHomeworkModel';
import {HomeworksFilter} from '../../pages/HomeworksList/types/HomeworksFilter';

const getAllHomeworksAPI = (filters: HomeworksFilter): Promise<ListHomeworkModel[]> => {
    return axiosInstance.post<HomeworksFilter, AxiosResponse<ListHomeworkModel[]>>('/api/homework/search', filters)
        .then(res => res.data);
};

export default getAllHomeworksAPI;