import axiosInstance from '../index';

const deleteHomeworkAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/homework/${id}`)
        .then(res => id);
};

export default deleteHomeworkAPI;