import axiosInstance from '../index';
import {GetSingleGradeModel} from '../../pages/GradesList/types/GetSingleGrade';

const getSingleGrade = (id: number): Promise<GetSingleGradeModel> => {
    return axiosInstance.get(`/api/grade/${id}`)
        .then(res => res.data)
}

export default getSingleGrade;