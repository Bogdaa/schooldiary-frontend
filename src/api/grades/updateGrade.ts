import axiosInstance from '../index';
import UpdateGradeModel from '../../pages/GradesList/types/UpdateGradeModel';

const updateGradeAPI = (data: UpdateGradeModel, id: number) => {
    return axiosInstance.patch<any>(`/api/grade/edit/${id}`, data)
        .then(res => res.data)
}

export default updateGradeAPI;