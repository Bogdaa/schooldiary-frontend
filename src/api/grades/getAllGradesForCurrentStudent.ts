import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListGradeModel} from '../../pages/GradesList/types/ListGradeModel';

const getAllGradesForCurrentStudentAPI = (): Promise<ListGradeModel[]> => {
    return axiosInstance.get<any, AxiosResponse<ListGradeModel[]>>('/api/grade/getAllForCurrentStudent')
        .then(res => res.data);
};

export default getAllGradesForCurrentStudentAPI;