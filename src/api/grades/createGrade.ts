import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {CreateGradeModel} from '../../pages/GradesList/types/CreateGradeModel';
import {GetGradeModel} from '../../pages/GradesList/types/GetGradeModel';

const createGradeAPI = (body: CreateGradeModel): Promise<GetGradeModel> => {
    return axiosInstance.post<CreateGradeModel, AxiosResponse<GetGradeModel>>('/api/grade/create', body)
        .then(res => res.data);
};

export default createGradeAPI;