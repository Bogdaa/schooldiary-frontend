import axiosInstance from '../index';
import {ClassAverageGradeModel} from '../../pages/GradesList/types/ClassAverageGradeModel';
import {AxiosResponse} from 'axios';

const getAverageGradeForClassAPI = (data: ClassAverageGradeModel) => {
    return axiosInstance.post<ClassAverageGradeModel, AxiosResponse<{ averageGrade: number}>>(`/api/grade/getAverageForClass`, data)
        .then(res => res.data)
}

export default getAverageGradeForClassAPI;