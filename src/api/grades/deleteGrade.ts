import axiosInstance from '../index';

const deleteGradeAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/grade/${id}`)
        .then(res => id);
};

export default deleteGradeAPI;