import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {GradesFilter} from '../../pages/GradesList/types/GradesFilter';
import {ListGradeModel} from '../../pages/GradesList/types/ListGradeModel';

const getAllGradesAPI = (filters: GradesFilter): Promise<ListGradeModel[]> => {
    return axiosInstance.post<GradesFilter, AxiosResponse<ListGradeModel[]>>('/api/grade/search', filters)
        .then(res => res.data);
};

export default getAllGradesAPI;