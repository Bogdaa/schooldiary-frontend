import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {User} from '../../types/User';
import {LoginModel} from '../../pages/Authorization/types/LoginModel';

type LoginResponseBody = {
    accessToken: string;
    data: User
}

const loginAPI = (body: LoginModel): Promise<LoginResponseBody> => {
    return axiosInstance.post<LoginModel, AxiosResponse<LoginResponseBody>>('/api/auth/login', body)
        .then(res => res.data);
};

export default loginAPI;