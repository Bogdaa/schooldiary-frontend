import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {User} from '../../types/User';

const checkTokenAPI = () => {
    return axiosInstance.post<any, AxiosResponse<User>>('/api/auth/checkToken')
        .then(res => res.data);
}

export default checkTokenAPI;