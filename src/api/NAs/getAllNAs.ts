import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListNAModel} from '../../pages/NAList/types/ListNAModel';

const getAllNAsAPI = (filters: any): Promise<ListNAModel[]> => {
    return axiosInstance.post<any, AxiosResponse<ListNAModel[]>>('/api/na/search', filters)
        .then(res => res.data);
};

export default getAllNAsAPI;