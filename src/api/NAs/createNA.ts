import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {GetNAModel} from '../../pages/NAList/types/GetNAModel';
import {CreateNAModel} from '../../pages/NAList/types/CreateNAModel';

const createNAAPI = (body: CreateNAModel): Promise<GetNAModel> => {
    return axiosInstance.post<CreateNAModel, AxiosResponse<GetNAModel>>('/api/na/create', body)
        .then(res => res.data);
};

export default createNAAPI;