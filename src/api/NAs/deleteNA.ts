import axiosInstance from '../index';

const deleteNAAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/na/${id}`)
        .then(res => id);
};

export default deleteNAAPI;