import axiosInstance from '../index';
import {UpdateNAModel} from '../../pages/NAList/types/UpdateNAModel';
import {GetNAModel} from '../../pages/NAList/types/GetNAModel';

const updateNAAPI = (data: UpdateNAModel, id: number) => {
    return axiosInstance.patch<GetNAModel>(`/api/na/edit/${id}`, data)
        .then(res => res.data)
}

export default updateNAAPI;