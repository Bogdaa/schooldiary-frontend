import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListNAModel} from '../../pages/NAList/types/ListNAModel';

const getAllNAsForCurrentStudentAPI = (): Promise<ListNAModel[]> => {
    return axiosInstance.get<any, AxiosResponse<ListNAModel[]>>('/api/na/getAllForCurrentStudent')
        .then(res => res.data);
};

export default getAllNAsForCurrentStudentAPI;