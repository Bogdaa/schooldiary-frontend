import axiosInstance from '../index';
import {AxiosResponse} from 'axios';
import {CountNAModel} from '../../pages/NAList/types/CountNAModel';

const countNAsAPI = (body: CountNAModel): Promise<[any,number]> => {
    return axiosInstance.post<CountNAModel, AxiosResponse<[any,number]>>('/api/na/countNA', body)
        .then(res => res.data);
}

export default countNAsAPI;