import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {Student} from '../../types/Student';
import StudentFilters from '../../pages/StudentList/types/StudentFilters';

type GetAllStudentsResponseBody = Array<Student>;

const getAllStudentsAPI = (filters: StudentFilters): Promise<GetAllStudentsResponseBody> => {
    return axiosInstance.post<any, AxiosResponse<GetAllStudentsResponseBody>>('/api/student/search', filters)
        .then(res => res.data);
};

export default getAllStudentsAPI;