import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {UpdateStudentModel} from '../../pages/StudentList/types/UpdateStudentModel';

type CreateStudentResponseBody = {
    id: number;
    firstName: string;
    lastName: string;
    patronymic: string;
    studyCourses: Array<any>
    grades: Array<any>
    NAs: Array<any>
}


const updateStudentAPI = (body: UpdateStudentModel, id: number): Promise<CreateStudentResponseBody> => {
    return axiosInstance.patch<UpdateStudentModel, AxiosResponse<CreateStudentResponseBody>>(`/api/student/edit/${id}`, body)
        .then(res => res.data);
};

export default updateStudentAPI;