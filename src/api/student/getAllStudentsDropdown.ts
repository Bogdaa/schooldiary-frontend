import getAllStudentsAPI from './getAllStudents';

const getAllStudentsDropdown = () => {
    return getAllStudentsAPI({}).then(data => data.map(x => ({
        id: x.id,
        label: `${x.lastName} ${x.firstName} ${x.patronymic}`
    })))
}

export default getAllStudentsDropdown;