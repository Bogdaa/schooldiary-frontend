import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {Student} from '../../types/Student';
import {User} from '../../types/User';

type CreateStudentRequestBody = {
    user: Pick<User, 'email' | 'password'>
    student: {
        firstName: string;
        lastName: string;
        patronymic: string;
        studyYearId: number;
        studyClassId: number;
    }
}

type CreateStudentResponseBody = {
    student: Student
}

const createStudentAPI = (body: CreateStudentRequestBody): Promise<CreateStudentResponseBody> => {
    return axiosInstance.post<CreateStudentRequestBody, AxiosResponse<CreateStudentResponseBody>>('/api/user/createStudent', body)
        .then(res => res.data);
};

export default createStudentAPI;