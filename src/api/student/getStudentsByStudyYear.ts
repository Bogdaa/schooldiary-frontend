import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {Student} from '../../types/Student';

type GetAllStudentsResponseBody = Array<Student>;

const getStudentsByStudyYearAPI = (studyYearId: number): Promise<GetAllStudentsResponseBody> => {
    return axiosInstance.post<any, AxiosResponse<GetAllStudentsResponseBody>>(`/api/student/search`, { studyYearId })
        .then(res => res.data);
};

export default getStudentsByStudyYearAPI;