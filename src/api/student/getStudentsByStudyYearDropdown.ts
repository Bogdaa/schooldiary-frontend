import getStudentsByStudyYear from './getStudentsByStudyYear';

const getStudentsByStudyYearDropdown = (studyYearId: number): Promise<any> => {
    return getStudentsByStudyYear(studyYearId).then(data => data.map(x => ({
        id: x.id,
        label: `${x.lastName} ${x.firstName} ${x.patronymic}`
    })))
}

export default getStudentsByStudyYearDropdown;