import axiosInstance from '../index';

const deleteStudentAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/student/delete/${id}`)
        .then(res => id);
};

export default deleteStudentAPI;