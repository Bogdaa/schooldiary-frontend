import getAllCoursesAPI from './getAllCourses';

const getAllCoursesDropdownAPI = (): Promise<any> => {
    return getAllCoursesAPI({}).then(data => data.map(x => ({
        label: `${x.studyYear.startDate} - ${x.studyYear.endDate}, ${x.studyClass.name}`,
        id: x.id
    })))
}

export default getAllCoursesDropdownAPI;