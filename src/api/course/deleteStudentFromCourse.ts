import axiosInstance from '../index';
import {DeleteStudentFromCourseModel} from '../../pages/StudyCourseList/types/DeleteStudentFromCourseModel';

const deleteStudentFromCourseAPI = (studentId: number, studyCourseId: number): Promise<DeleteStudentFromCourseModel> => {
    return axiosInstance.patch(`/api/studyCourse/removeStudentFromStudyCourse`, {
        studentId,
        studyCourseId
    })
        .then(res => ({
            studentId,
            studyCourseId
        }));
};

export default deleteStudentFromCourseAPI;