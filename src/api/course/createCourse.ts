import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {GetCourseModel} from '../../pages/StudyCourseList/types/GetCourseModel';
import {CreateCourseModel} from '../../pages/StudyCourseList/types/CreateCourseModel';

const createCourseAPI = (body: CreateCourseModel): Promise<GetCourseModel> => {
    return axiosInstance.post<CreateCourseModel, AxiosResponse<GetCourseModel>>('/api/studyCourse/create', body)
        .then(res => res.data);
};

export default createCourseAPI;