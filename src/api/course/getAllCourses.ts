import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListStudyCourseModel} from '../../pages/StudyCourseList/types/ListStudyCourseModel';
import {StudyCourseFilters} from '../../pages/StudyCourseList/types/StudyCourseFilters';

const getAllCoursesAPI = (filters: StudyCourseFilters): Promise<ListStudyCourseModel[]> => {
    return axiosInstance.post<any, AxiosResponse<ListStudyCourseModel[]>>('/api/studyCourse/search', filters)
        .then(res => res.data);
};

export default getAllCoursesAPI;