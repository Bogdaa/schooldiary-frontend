import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {ListYearModel} from '../../pages/YearList/types/ListYearModel';

const getAllYearsAPI = (): Promise<ListYearModel[]> => {
    return axiosInstance.get<any, AxiosResponse<ListYearModel[]>>('/api/studyYear/getAll')
        .then(res => res.data);
};

export default getAllYearsAPI;