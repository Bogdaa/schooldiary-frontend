import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {CreateYearModel} from '../../pages/YearList/types/CreateYearModel';
import {GetYearModel} from '../../pages/YearList/types/GetYearModel';

const updateYearAPI = (body: CreateYearModel, id: number): Promise<GetYearModel> => {
    return axiosInstance.patch<CreateYearModel, AxiosResponse<GetYearModel>>(`/api/studyYear/edit/${id}`, body)
        .then(res => res.data);
};

export default updateYearAPI;