import {AxiosResponse} from 'axios';
import axiosInstance from '../index';
import {CreateYearModel} from '../../pages/YearList/types/CreateYearModel';
import {GetYearModel} from '../../pages/YearList/types/GetYearModel';

const createYearAPI = (body: CreateYearModel): Promise<GetYearModel> => {
    return axiosInstance.post<CreateYearModel, AxiosResponse<GetYearModel>>('/api/studyYear/create', body)
        .then(res => res.data);
};

export default createYearAPI;