import getAllYears from './getAllYears';

const getAllYearsDropdownAPI = (): Promise<any[]> => {
    return getAllYears().then(data => data.map(x => ({
        id: x.id,
        label: `${x.startDate} - ${x.endDate}`
    })));
}

export default getAllYearsDropdownAPI;