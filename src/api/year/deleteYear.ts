import axiosInstance from '../index';

const deleteYearAPI = (id: number): Promise<number> => {
    return axiosInstance.delete(`/api/studyYear/${id}`)
        .then(res => id);
};

export default deleteYearAPI;