import React, {useEffect, useState} from 'react';
import {
    Box,
    Button,
    Chip,
    Grid,
    Paper,
    TextField,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableRow,
    TableHead,
    TableContainer, IconButton
} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import {Autocomplete} from '@mui/lab';
import {TeacherFilters} from './types/TeacherFilters';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import NoItems from '../../components/UI/NoItems';
import subjects from '../../constants/subjects';
import getAllTeachers, {setGetAllTeachers} from '../../reducers/teacherReducer/getAllTeachers';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import deleteTeacher from '../../reducers/teacherReducer/deleteTeacher';
import DeleteDialog from '../../components/shared/DeleteDialog';
import CreateTeacherDialog from './components/CreateTeacherDialog';
import useStyles from './styles';
import UpdateTeacherDialog from './components/UpdateTeacherDialog';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import getAllSubjectsDropdown from '../../api/subject/getAllSubjectsDropdown';

const TeacherList = () => {
    const [deleteTeacherId, setDeleteTeacherId] = useState<number | null>(null);
    const [updateTeacherId, setUpdateTeacherId] = useState<number | null>(null);
    const [modal, setModal] = useState<'CreateTeacherDialog' | 'DeleteTeacherDialog' | 'UpdateTeacherDialog' | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const teachers = useAppSelector(s => s.teacher.dataList);
    const {control, handleSubmit} = useForm<TeacherFilters>({
        defaultValues: {
            firstName: '',
            lastName: '',
            patronymic: '',
            subjects: []
        },
    });

    useEffect(() => {
        dispatch(getAllTeachers({}));

        return () => {
            dispatch(setGetAllTeachers(null));
        };
    }, []);

    const handleSearch = (data: TeacherFilters) => {
        //@ts-ignore
        dispatch(getAllTeachers(Object.fromEntries(Object.entries(data)
            .filter(([_, value]) => value && value.length > 0)) as TeacherFilters))
    }

    const handleDeleteTeacher = () => {
        setModal(null);
        dispatch(deleteTeacher(deleteTeacherId!, () => setDeleteTeacherId(null)));
    };

    const handleDeleteModalOpen = (id: number) => () => {
        setDeleteTeacherId(id);
        setModal('DeleteTeacherDialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteTeacherId(null);
        setModal(null)
    };

    const handleCreateModalOpen = () => {
        setModal('CreateTeacherDialog')
    };

    const handleCreateModalClose = () => {
        setModal(null)
    };

    const handleUpdateModalOpen = (id: number) => () => {
        setUpdateTeacherId(id)
        setModal('UpdateTeacherDialog')
    }

    const handleUpdateModalClose = () => {
        setUpdateTeacherId(null);
        setModal(null)
    };

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Учителя</Typography>
                <Button
                    color='primary'
                    size='medium'
                    variant='contained'
                    onClick={handleCreateModalOpen}>
                    Создать учителя
                </Button>
            </Box>
            <form className={styles.filters} onSubmit={handleSubmit(handleSearch)}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='lastName'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Фамилия'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='firstName'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Имя'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='patronymic'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Отчество'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={8}>
                        <Controller
                            control={control}
                            name='subjects'
                            render={({field}) => (
                                <ServerAutocomplete
                                    multiple={true}
                                    onChange={(_, value) => {
                                        field.onChange(value.map(({ label }: { label: string }) => label))
                                    }}
                                    getDataRequest={getAllSubjectsDropdown}
                                    placeholder='Предметы'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}></Grid>
                    <Grid item xs={10}></Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            type='submit'
                            color='primary'
                            variant='contained'>
                            Искать
                        </Button>
                    </Grid>
                </Grid>
            </form>
            {teachers && teachers.length ? (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>ФИО</TableCell>
                                <TableCell align='left' width='auto'>Предметы</TableCell>
                                <TableCell align='center' width={70}>Изменить</TableCell>
                                <TableCell align='center' width={70}>Удалить</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {teachers.map((teacher) => (
                                <TableRow
                                    key={teacher.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {teacher.lastName} {teacher.firstName} {teacher.patronymic}
                                    </TableCell>
                                    <TableCell align='left'>{teacher.subjects.map(subject => subject.name).join(', ')}</TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='success' size='medium' onClick={handleUpdateModalOpen(teacher.id)}>
                                            <EditIcon/>
                                        </IconButton>
                                    </TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='error' size='medium'
                                                    onClick={handleDeleteModalOpen(teacher.id)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {teachers?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет учителей в системе'/>
            )}
            {modal === 'DeleteTeacherDialog' && (
                <DeleteDialog
                    title='Удаление учителя'
                    body='Вы действительно хотите удалить этого учителя?'
                    onDelete={handleDeleteTeacher}
                    onClose={handleDeleteModalClose} />
            )}
            {modal === 'CreateTeacherDialog' && <CreateTeacherDialog onClose={handleCreateModalClose}/>}
            {modal === 'UpdateTeacherDialog' && <UpdateTeacherDialog onClose={handleUpdateModalClose} id={updateTeacherId!} />}
        </Box>
    );
};

export default TeacherList;