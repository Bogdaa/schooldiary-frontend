import React from 'react';
import {
    Button,
    Dialog,
    DialogContent,
    DialogTitle,
    TextField
} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {CreateTeacherModel} from '../../types/CreateTeacherModel';
import createTeacher from '../../../../reducers/teacherReducer/createTeacher';
import useStyles from './styles';

interface CreateTeacherDialogProps {
    onClose: () => void;
}

const CreateTeacherDialog = (props: CreateTeacherDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<CreateTeacherModel>({
        defaultValues: {
            firstName: '',
            lastName: '',
            patronymic: '',
            email: '',
            password: ''
        },
    });

    const handleCreateTeacher = (data: CreateTeacherModel) => {
        props.onClose();
        dispatch(createTeacher(data));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Создать учителя
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateTeacher)}>
                    <Controller
                        control={control}
                        name='lastName'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Фамилия'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='firstName'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Имя'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='patronymic'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Отчество'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='email'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Логин'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='password'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                type='password'
                                autoComplete='off'
                                size='small'
                                placeholder='Пароль'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    );
};

export default CreateTeacherDialog;