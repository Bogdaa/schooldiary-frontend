import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import useStyles from './styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {UpdateTeacherModel} from '../../types/UpdateTeacherModel';
import updateTeacher from '../../../../reducers/teacherReducer/updateTeacher';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllSubjectsDropdown from '../../../../api/subject/getAllSubjectsDropdown';

interface UpdateTeacherDialogProps {
    onClose: () => void;
    id: number;
}

const UpdateTeacherDialog = (props: UpdateTeacherDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<UpdateTeacherModel>({
        defaultValues: {
            firstName: '',
            lastName: '',
            patronymic: '',
            subjectIds: []
        },
    });

    const handleUpdateTeacher = (data: UpdateTeacherModel) => {
        props.onClose();
        dispatch(updateTeacher(data, props.id))
    }

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Обновить учителя
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleUpdateTeacher)}>
                    <Controller
                        control={control}
                        name='lastName'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Фамилия'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='firstName'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Имя'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='patronymic'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Отчество'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='subjectIds'
                        render={({field}) => (
                            <ServerAutocomplete
                                multiple={true}
                                onChange={(_, value) => {
                                    field.onChange(value.map(({ id }: { id: number }) => id))
                                }}
                                getDataRequest={getAllSubjectsDropdown}
                                placeholder='Предметы'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Обновить
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default UpdateTeacherDialog;