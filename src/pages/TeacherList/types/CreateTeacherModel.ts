export interface CreateTeacherModel {
    firstName: string,
    lastName: string,
    patronymic: string;
    email: string;
    password: string;
}
