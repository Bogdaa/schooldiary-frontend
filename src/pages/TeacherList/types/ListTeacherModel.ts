export interface ListTeacherModel {
    id: number;
    firstName: string;
    lastName:  string;
    patronymic: string;
    subjects: Array<any>
}