export interface TeacherFilters {
    firstName?: string;
    lastName?: string;
    patronymic?: string;
    subjects?: Array<string>
}