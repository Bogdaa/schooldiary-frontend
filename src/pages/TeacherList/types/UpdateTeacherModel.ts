export interface UpdateTeacherModel {
    firstName: string;
    lastName: string;
    patronymic: string;
    subjectIds: Array<number>;
}