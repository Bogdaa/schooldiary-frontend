export interface GetSingleTeacherModel {
    id: number;
    firstName: string;
    lastName:  string;
    patronymic: string;
    subjects: Array<any>
}