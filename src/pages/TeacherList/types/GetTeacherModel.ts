import UserType from '../../../types/UserType';

export interface GetTeacherModel {
    email: string;
    password: string;
    role: UserType;
    id: number;
    createdAt: string;
    updatedAt: string;
    teacher: {
        firstName: string,
        lastName: string,
        patronymic: string,
        id: number,
        createdAt: string,
        updatedAt: string;
    }
}