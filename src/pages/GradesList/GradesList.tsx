import React, {useEffect, useState} from 'react';
import {
    Box,
    Button, ButtonGroup, Grid, IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField,
    Typography
} from '@mui/material';
import useStyles from './styles';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import getAllGrades, {setGetAllGrades} from '../../reducers/gradesReducer/getAllGrades';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import NoItems from '../../components/UI/NoItems';
import deleteGrade from '../../reducers/gradesReducer/deleteGrade';
import DeleteDialog from '../../components/shared/DeleteDialog';
import gradeTypeMap from '../../constants/gradeTypeMap';
import CreateGradeDialog from './components/CreateGradeDialog';
import UpdateGradeDialog from './components/UpdateGradeDialog';
import {Controller, useForm} from 'react-hook-form';
import {GradesFilter} from './types/GradesFilter';
import {Autocomplete} from '@mui/lab';
import gradeValues from '../../constants/gradeValues';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import getAllStudentsDropdown from '../../api/student/getAllStudentsDropdown';
import gradeTypes from '../../constants/gradeTypes';
import CountClassAverageGradeDialog from './components/CountClassAverageGradeDialog';
import Permission from '../../components/shared/Permission';

const GradesList = () => {
    const [modal, setModal] = useState<'DeleteGradeDialog' | 'CreateGradeDialog'
        | 'UpdateGradeDialog' | 'CountClassAverageGradeDialog' | null>(null);
    const [updateGradeId, setUpdateGradeId] = useState<number | null>(null);
    const [deleteGradeId, setDeleteGradeId] = useState<number | null>(null);

    const {control, handleSubmit} = useForm<GradesFilter>({
        defaultValues: {
            value: null,
            gradeType: null,
            studentId: null,
            subjectName: null,
        },
    });

    const styles = useStyles();
    const dispatch = useAppDispatch();
    const grades = useAppSelector(s => s.grade.dataList);

    useEffect(() => {
        dispatch(getAllGrades({}));

        return () => {
            dispatch(setGetAllGrades(null));
        };
    }, []);

    const handleDeleteGrade = () => {
        setModal(null);
        dispatch(deleteGrade(deleteGradeId!, () => setDeleteGradeId(null)));
    };

    const handleDeleteHomeworkOpen = (id: number) => () => {
        setDeleteGradeId(id);
        setModal('DeleteGradeDialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteGradeId(null);
        setModal(null);
    };

    const handleCreateModalOpen = () => {
        setModal('CreateGradeDialog');
    };

    const handleCountAverageClassModalOpen = () => {
        setModal('CountClassAverageGradeDialog');
    };

    const handleCreateModalClose = () => {
        setModal(null);
    };

    const handleUpdateGradeOpen = (id: number) => () => {
        setUpdateGradeId(id);
        setModal('UpdateGradeDialog');
    };

    const handleUpdateGradeClose = () => {
        setUpdateGradeId(null);
        setModal(null);
    };

    const handleSearch = (data: GradesFilter) => {
        //@ts-ignore
        dispatch(getAllGrades(Object.fromEntries(Object.entries(data)
            .filter(([_, value]) => value)) as GradesFilter));
    };

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Оценки</Typography>
                <ButtonGroup>
                    <Permission forRoles={['teacher']}>
                        <Button
                            color='primary'
                            size='medium'
                            variant='contained'
                            onClick={handleCountAverageClassModalOpen}>
                            Подсчитать средний балл
                        </Button>
                    </Permission>
                    <Permission forRoles={['admin', 'teacher']}>
                        <Button
                            color='primary'
                            size='medium'
                            variant='contained'
                            onClick={handleCreateModalOpen}>
                            Создать оценку
                        </Button>
                    </Permission>
                </ButtonGroup>
            </Box>
            <form className={styles.filters} onSubmit={handleSubmit(handleSearch)}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='value'
                            render={({field}) => (
                                <Autocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value!.value);
                                    }}
                                    isOptionEqualToValue={(option, value) => option.label === value.label}
                                    options={gradeValues}
                                    getOptionLabel={(option) => option.label}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            size='small'
                                            variant='outlined'
                                            label='Оценка'
                                        />
                                    )}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='gradeType'
                            render={({field}) => (
                                <Autocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value!.value);
                                    }}
                                    isOptionEqualToValue={(option, value) => option.label === value.label}
                                    options={gradeTypes}
                                    getOptionLabel={(option) => option.label}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            size='small'
                                            variant='outlined'
                                            label='Тип оценки'
                                        />
                                    )}
                                />
                            )}
                        />
                    </Grid>
                    <Permission forRoles={['admin', 'teacher']}>
                        <Grid item xs={4}>
                            <Controller
                                control={control}
                                name='studentId'
                                render={({field}) => (
                                    <ServerAutocomplete
                                        onChange={(_, value) => {
                                            field.onChange(value.id);
                                        }}
                                        getDataRequest={getAllStudentsDropdown}
                                        placeholder='Студент'
                                    />
                                )}
                            />
                        </Grid>
                    </Permission>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='subjectName'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Предмет'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={8}></Grid>
                    <Grid item xs={10}></Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            type='submit'
                            color='primary'
                            variant='contained'>
                            Искать
                        </Button>
                    </Grid>
                </Grid>
            </form>
            {grades && grades.length ? (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell align='left' width='auto'>Оценка</TableCell>
                                <TableCell align='left' width='auto'>Тип оценки</TableCell>
                                <TableCell align='left' width='auto'>Студент</TableCell>
                                <TableCell align='left' width='auto'>Дата</TableCell>
                                <TableCell align='left' width='auto'>Урок</TableCell>
                                <Permission forRoles={['admin', 'teacher']}>
                                    <TableCell align='center' width='auto'>Редактировать</TableCell>
                                    <TableCell align='center' width={70}>Удалить</TableCell>
                                </Permission>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {grades.map(grade => (
                                <TableRow
                                    key={grade.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {grade.value}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {gradeTypeMap[grade.gradeType]}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {grade.student.lastName} {grade.student.firstName} {grade.student.patronymic}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {grade.dateSchedule.date}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {grade.dateSchedule.schedule && grade.dateSchedule.schedule.subject.name}
                                    </TableCell>
                                    <Permission forRoles={['admin', 'teacher']}>
                                        <TableCell align='center'>
                                            <IconButton color='success' size='medium'
                                                        onClick={handleUpdateGradeOpen(grade.id)}>
                                                <EditIcon/>
                                            </IconButton>
                                        </TableCell>
                                        <TableCell align='center'>
                                            <IconButton color='error' size='medium'
                                                        onClick={handleDeleteHomeworkOpen(grade.id)}>
                                                <DeleteIcon/>
                                            </IconButton>
                                        </TableCell>
                                    </Permission>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {grades?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет оценок в системе'/>
            )}
            {modal === 'DeleteGradeDialog' && (
                <DeleteDialog
                    title='Удаление оценки'
                    body='Вы действительно хотите удалить эту оценку?'
                    onDelete={handleDeleteGrade}
                    onClose={handleDeleteModalClose}/>
            )}
            {modal === 'CreateGradeDialog' && (
                <CreateGradeDialog onClose={handleCreateModalClose}/>
            )}
            {modal === 'UpdateGradeDialog' && (
                <UpdateGradeDialog onClose={handleUpdateGradeClose} id={updateGradeId!}/>
            )}
            {modal === 'CountClassAverageGradeDialog' && (
                <CountClassAverageGradeDialog onClose={handleCreateModalClose}/>
            )}
        </Box>
    );
};

export default GradesList;