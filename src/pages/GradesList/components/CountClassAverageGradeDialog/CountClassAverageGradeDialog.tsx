import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import {Autocomplete} from '@mui/lab';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import gradeTypes from '../../../../constants/gradeTypes';
import useStyles from './styles';
import getAllClassesDropdownAPI from '../../../../api/class/getAllClassesDropdown';
import getAllYearsDropdownAPI from '../../../../api/year/getAllYearsDropdown';
import getAllSubjectsDropdown from '../../../../api/subject/getAllSubjectsDropdown';
import {ClassAverageGradeModel} from '../../types/ClassAverageGradeModel';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import getAverageForClass from '../../../../reducers/gradesReducer/getAverageForClass';

interface CountClassAverageGradeDialogProps {
    onClose: () => void
}

const CountClassAverageGradeDialog = (props: CountClassAverageGradeDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<ClassAverageGradeModel>({
        defaultValues: {
            classId: null,
            subjectId: null,
            gradeType: null,
            studyYearId: null
        }
    });

    const handleCountClassAverageGrade = (data: ClassAverageGradeModel) => {
        dispatch(getAverageForClass(data))
    }

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Подсчитать оценку
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCountClassAverageGrade)}>
                    <Controller
                        control={control}
                        name='subjectId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllSubjectsDropdown}
                                placeholder='Предмет'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='classId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllClassesDropdownAPI}
                                placeholder='Класс'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studyYearId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllYearsDropdownAPI}
                                placeholder='Учебный год'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='gradeType'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={gradeTypes}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Тип оценки'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Подсчитать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CountClassAverageGradeDialog;