import {makeStyles} from '@mui/styles';

const useStyles = makeStyles(() => ({
    input: {
        '&&': {
            marginTop: 15
        }
    },
    submitButton: {
        '&&': {
            marginBottom: 15
        }
    }
}));

export default useStyles;