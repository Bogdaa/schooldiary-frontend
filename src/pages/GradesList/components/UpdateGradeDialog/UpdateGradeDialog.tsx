import React, {useEffect} from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import useStyles from './styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {Controller, useForm} from 'react-hook-form';
import dayjs from 'dayjs';
import updateGrade from '../../../../reducers/gradesReducer/updateGrade';
import UpdateGradeModel from '../../types/UpdateGradeModel';
import getSingleGrade from '../../../../api/grades/getSingleGrade';
import {Autocomplete} from '@mui/lab';
import gradeValues from '../../../../constants/gradeValues';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllStudentsDropdown from '../../../../api/student/getAllStudentsDropdown';
import gradeTypes from '../../../../constants/gradeTypes';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';
import getAllSubjectsDropdown from '../../../../api/subject/getAllSubjectsDropdown';
import getAllTeachersDropdown from '../../../../api/teacher/getAllTeachersDropdown';
import lessonNumbers from '../../../../constants/lessonNumbers';

interface UpdateGradeDialog {
    onClose: () => void,
    id: number;
}

const UpdateGradeDialog = (props: UpdateGradeDialog) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<UpdateGradeModel>({
        shouldUnregister: false,
        defaultValues: {
            value: null,
            gradeType: null,
            date: dayjs().format('YYYY-MM-DD'),
            studentId: null,
            subjectId: null,
            lessonNumber: null,
            teacherId: null,
        }
    });

    const handleUpdateHomework = (data: UpdateGradeModel) => {
        props.onClose();
        dispatch(updateGrade(data, props.id));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Обновить оценку
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleUpdateHomework)}>
                    <Controller
                        control={control}
                        name='value'
                        render={({field}) => (
                                <Autocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value!.value)
                                    }}
                                    isOptionEqualToValue={(option, value) => option.label === value.label}
                                    options={gradeValues}
                                    getOptionLabel={(option) => option.label}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            size='small'
                                            variant='outlined'
                                            label='Оценка'
                                            className={styles.input}
                                        />
                                    )}
                                />
                        )}
                    />
                    <Controller
                        control={control}
                        name='date'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Дата'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studentId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllStudentsDropdown}
                                placeholder='Студент'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='subjectId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllSubjectsDropdown}
                                placeholder='Студент'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='teacherId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllTeachersDropdown}
                                placeholder='Студент'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='gradeType'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={gradeTypes}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Тип оценки'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='lessonNumber'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={lessonNumbers}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Номер урока'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default UpdateGradeDialog;