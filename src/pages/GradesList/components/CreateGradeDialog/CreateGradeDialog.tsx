import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import useStyles from './styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {Controller, useForm} from 'react-hook-form';
import createGrade from '../../../../reducers/gradesReducer/createGrade';
import {CreateGradeModel} from '../../types/CreateGradeModel';
import {Autocomplete} from '@mui/lab';
import gradeValues from '../../../../constants/gradeValues';
import gradeTypes from '../../../../constants/gradeTypes';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllDateSchedulesDropdown from '../../../../api/dateSchedule/getAllDateSchedulesDropdown';
import getAllStudentsDropdown from '../../../../api/student/getAllStudentsDropdown';

interface CreateGradeDialogProps {
    onClose: () => void;
}

const CreateGradeDialog = (props: CreateGradeDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<CreateGradeModel>({
        defaultValues: {
            studentId: null,
            value: null,
            dateScheduleId: null,
            gradeType: null
        }
    });

    const handleCreateGrade = (data: CreateGradeModel) => {
        props.onClose();
        dispatch(createGrade(data));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Создать оценку
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateGrade)}>
                    <Controller
                        control={control}
                        name='value'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={gradeValues}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Оценка'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='dateScheduleId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllDateSchedulesDropdown}
                                placeholder='Расписание, привязано к дате'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studentId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllStudentsDropdown}
                                placeholder='Студент'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='gradeType'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={gradeTypes}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Тип оценки'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CreateGradeDialog;