import {GradeType} from '../../../types/GradeType';

export interface GradesFilter {
    value?: number | null;
    gradeType?: GradeType | null;
    studentId?: number | null;
    subjectName?: string | null;
}