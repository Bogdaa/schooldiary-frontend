import {GradeType} from '../../../types/GradeType';

export interface CreateGradeModel {
    studentId: number | null;
    value: number | null;
    dateScheduleId: number | null;
    gradeType: GradeType | null;
}