import {GradeType} from '../../../types/GradeType';

export interface GetSingleGradeModel {
    id: number;
    value: number;
    gradeType: GradeType,
    dateSchedule: {
        id: number;
        date: string;
        schedule: {
            id: number;
            lessonNumber: number;
            weekday: string;
            subject: {
                id: number;
                name: string;
            }
        }
    },
    student: {
        id: number;
        firstName: string;
        lastName: string;
        patronymic: string;
        updatedAt: string;
    }
}