import {GradeType} from '../../../types/GradeType';

export interface ListGradeModel {
    id: number,
    value: number;
    gradeType: GradeType,
    student: {
        firstName: string;
        lastName: string;
        patronymic: string;
    },
    dateSchedule: {
        id: number;
        date: string;
        schedule: {
            id: number;
            lessonNumber: number;
            weekday: string;
            subject: {
                id: number;
                name: string;
            }
        } | null
    }
}