import {GradeType} from '../../../types/GradeType';

export interface ClassAverageGradeModel {
    classId: number | null,
    subjectId: number | null
    gradeType: GradeType | null;
    studyYearId: number | null
}