import {GradeType} from '../../../types/GradeType';

export interface UpdateGradeModel {
    value: number | null;
    gradeType: GradeType | null;
    date: string;
    studentId: number | null;
    subjectId: number | null;
    lessonNumber: number | null;
    teacherId: number | null;
}

export default UpdateGradeModel;