import {GradeType} from '../../../types/GradeType';

export interface GetGradeModel {
    student: {
        id: number;
        firstName: string;
        lastName: string;
        patronymic: string;
    },
    value: number;
    dateSchedule: {
        id: number;
        date: string;
        schedule: {
            id: number;
            lessonNumber: number;
            weekday: string;
            subject: {
                id: number;
                name: string;
            }
        },
        homework: Array<
            {
                id: number
                description: string;
                deadline: string;
            }
        >,
        NAs: [],
        grades: []
    },
    gradeType: GradeType;
    id: number;
    createdAt: string;
    updatedAt: string;
}