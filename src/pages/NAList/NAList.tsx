import React, {useEffect, useState} from 'react';
import {
    Box,
    Button, ButtonGroup, Grid, IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField,
    Typography
} from '@mui/material';
import useStyles from './styles';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import getAllNAs, {setGetAllNAs} from '../../reducers/naReducer/getAllNAs';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import NoItems from '../../components/UI/NoItems';
import deleteNA from '../../reducers/naReducer/deleteNA';
import dayjs from 'dayjs';
import DeleteDialog from '../../components/shared/DeleteDialog';
import CreateNADialog from './components/CreateNADialog';
import UpdateNADialog from './components/UpdateNADialog';
import {Controller, useForm} from 'react-hook-form';
import {NAFilters} from './types/NAFilters';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import getAllStudentsDropdown from '../../api/student/getAllStudentsDropdown';
import CountNAsDialog from './components/CountNAsDialog';
import Permission from '../../components/shared/Permission';

const NAList = () => {
    const [deleteNAId, setDeleteNAId] = useState<number | null>(null);
    const [updateNAId, setUpdateNAId] = useState<number | null>(null);
    const [modal, setModal] = useState<'DeleteNADialog' | 'CreateNADialog' | 'UpdateNADialog' | 'CountNADialog' | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const NAs = useAppSelector(s => s.na.dataList);

    const {control, handleSubmit} = useForm<NAFilters>({
        defaultValues: {
            date: dayjs().format('YYYY-MM-DD'),
            subject: '',
            studentId: null,
            reason: ''
        },
    });

    useEffect(() => {
        dispatch(getAllNAs({}));

        return () => {
            dispatch(setGetAllNAs(null));
        };
    }, []);

    const handleSearch = (data: NAFilters) => {
        //@ts-ignore
        dispatch(getAllNAs(Object.fromEntries(Object.entries(data)
            .filter(([_, value]) => value)) as NAFilters))
    }

    const handleDeleteNA = () => {
        setModal(null);
        dispatch(deleteNA(deleteNAId!, () => setDeleteNAId(null)));
    };

    const handleDeleteModalOpen = (id: number) => () => {
        setDeleteNAId(id);
        setModal('DeleteNADialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteNAId(null);
        setModal(null);
    };

    const handleUpdateHomeworkOpen = (id: number) => () => {
        setUpdateNAId(id);
        setModal('UpdateNADialog');
    };

    const handleUpdateModalClose = () => {
        setUpdateNAId(null);
        setModal(null);
    };

    const handleCreateModalOpen = () => {
        setModal('CreateNADialog');
    };

    const handleCountNAsModalOpen = () => {
        setModal('CountNADialog');
    }

    const handleCreateModalClose = () => {
        setModal(null);
    };

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Пропуск</Typography>
                <ButtonGroup>
                    <Permission forRoles={['teacher']}>
                        <Button
                            color='primary'
                            size='medium'
                            variant='contained'
                            onClick={handleCountNAsModalOpen}>
                            Подсчитать пропуски
                        </Button>
                    </Permission>
                    <Permission forRoles={['admin', 'teacher']}>
                        <Button
                            color='primary'
                            size='medium'
                            variant='contained'
                            onClick={handleCreateModalOpen}>
                            Создать пропуск
                        </Button>
                    </Permission>
                </ButtonGroup>
            </Box>
            <form className={styles.filters} onSubmit={handleSubmit(handleSearch)}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='reason'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Причина'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='subject'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Предмет'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='date'
                            render={({field}) => (
                                <DesktopDatePicker
                                    {...field}
                                    label='Дата'
                                    inputFormat='YYYY/MM/DD'
                                    renderInput={(params) => <TextField
                                        {...params}
                                        fullWidth
                                        autoComplete='off'
                                        size='small'
                                        variant='outlined'
                                    />}
                                />
                            )}
                        />
                    </Grid>
                    <Permission forRoles={["admin", "teacher"]}>
                        <Grid item xs={4}>
                            <Controller
                                control={control}
                                name='studentId'
                                render={({field}) => (
                                    <ServerAutocomplete
                                        onChange={(_, value) => {
                                            field.onChange(value.id)
                                        }}
                                        getDataRequest={getAllStudentsDropdown}
                                        placeholder='Студент'
                                    />
                                )}
                            />
                        </Grid>
                    </Permission>
                    <Grid item xs={8}></Grid>
                    <Grid item xs={10}></Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            type='submit'
                            color='primary'
                            variant='contained'>
                            Искать
                        </Button>
                    </Grid>
                </Grid>
            </form>
            {NAs && NAs.length ? (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell align='left' width='auto'>Студент</TableCell>
                                <TableCell align='left' width='auto'>Причина отсутствия</TableCell>
                                <TableCell align='left' width='auto'>Дата</TableCell>
                                <TableCell align='left' width='auto'>Урок</TableCell>
                                <Permission forRoles={['admin', 'teacher']}>
                                    <TableCell align='left' width='auto'>Редактировать</TableCell>
                                    <TableCell align='center' width={70}>Удалить</TableCell>
                                </Permission>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {NAs.map((na) => (
                                <TableRow
                                    key={na.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {na.student.lastName} {na.student.firstName} {na.student.patronymic}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {na.reason}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {dayjs(na.dateSchedule.date).format('YYYY-MM-DD')}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {na.dateSchedule.schedule.subject.name}
                                    </TableCell>
                                    <Permission forRoles={['admin', 'teacher']}>
                                        <TableCell align='center'>
                                            <IconButton color='success' size='medium'
                                                        onClick={handleUpdateHomeworkOpen(na.id)}>
                                                <EditIcon/>
                                            </IconButton>
                                        </TableCell>
                                        <TableCell align='center'>
                                            <IconButton color='error' size='medium' onClick={handleDeleteModalOpen(na.id)}>
                                                <DeleteIcon/>
                                            </IconButton>
                                        </TableCell>
                                    </Permission>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {NAs?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет пропусков в системе'/>
            )}
            {modal === 'DeleteNADialog' && (
                <DeleteDialog
                    title='Удаление пропуска'
                    body='Вы действительно хотите удалить этот пропуск?'
                    onDelete={handleDeleteNA}
                    onClose={handleDeleteModalClose}/>
            )}
            {modal === 'CreateNADialog' && (
                <CreateNADialog onClose={handleCreateModalClose}/>
            )}
            {modal === 'UpdateNADialog' && (
                <UpdateNADialog onClose={handleUpdateModalClose} id={updateNAId!}/>
            )}
            {modal === 'CountNADialog' && (
                <CountNAsDialog onClose={handleCreateModalClose} />
            )}
        </Box>
)
}

export default NAList;