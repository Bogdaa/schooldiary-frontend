import React from 'react';
import {Dialog, DialogTitle, DialogContent, TextField, Button} from '@mui/material';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {Controller, useForm} from 'react-hook-form';
import {CountNAModel} from '../../types/CountNAModel';
import dayjs from 'dayjs';
import countNAs from '../../../../reducers/naReducer/countNAs';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';
import useStyles from './styles';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllStudentsDropdown from '../../../../api/student/getAllStudentsDropdown';
import getAllClassesDropdownAPI from '../../../../api/class/getAllClassesDropdown';

interface CountNAsDialogProps {
    onClose: () => void
}

const CountNAsDialog = (props: CountNAsDialogProps) => {
    const dispatch = useAppDispatch();
    const styles = useStyles();

    const {control, handleSubmit} = useForm<CountNAModel>({
        defaultValues: {
            fromDate: dayjs().format('YYYY-MM-DD'),
            toDate: dayjs().format('YYYY-MM-DD'),
            studentId: null,
            studyClassId: null,
        }
    });

    const handleCountNAs = (data: CountNAModel) => {
        dispatch(countNAs(data))
    }

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Подсчитать количество пропусков
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCountNAs)}>
                    <Controller
                        control={control}
                        name='fromDate'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Дата от'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='toDate'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Дата от'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studentId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllStudentsDropdown}
                                placeholder='Студент'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studyClassId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllClassesDropdownAPI}
                                placeholder='Класс'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Подсчитать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CountNAsDialog;