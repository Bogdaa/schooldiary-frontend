import React from 'react';
import useStyles from '../../../HomeworksList/components/CreateHomeworkDialog/styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {Controller, useForm} from 'react-hook-form';
import {CreateNAModel} from '../../types/CreateNAModel';
import createNA from '../../../../reducers/naReducer/createNA';
import {Button, Dialog, DialogContent, DialogTitle, Grid, TextField} from '@mui/material';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllStudentsDropdown from '../../../../api/student/getAllStudentsDropdown';
import getAllDateSchedulesDropdown from '../../../../api/dateSchedule/getAllDateSchedulesDropdown';
import {UpdateNAModel} from '../../types/UpdateNAModel';
import updateNA from '../../../../reducers/naReducer/updateNA';

interface UpdateNADialogProps {
    onClose: () => void;
    id: number;
}

const UpdateNADialog = (props: UpdateNADialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<UpdateNAModel>({
        defaultValues: {
            studentId: null,
            dateScheduleId: null,
            reason: ''
        }
    });

    const handleUpdateNA = (data: UpdateNAModel) => {
        props.onClose();
        dispatch(updateNA(data, props.id));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Обновить пропуск
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleUpdateNA)}>
                    <Controller
                        control={control}
                        name='reason'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Причина'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='studentId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllStudentsDropdown}
                                    placeholder='Студент'
                                    className={styles.input}
                                />
                            )}
                        />
                    </Grid>
                    <Controller
                        control={control}
                        name='dateScheduleId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllDateSchedulesDropdown}
                                placeholder='Дата, подвязанная к расписанию'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default UpdateNADialog;