import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, Grid, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllDateSchedulesDropdown from '../../../../api/dateSchedule/getAllDateSchedulesDropdown';
import useStyles from '../../../HomeworksList/components/CreateHomeworkDialog/styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {CreateNAModel} from '../../types/CreateNAModel';
import getAllStudentsDropdown from '../../../../api/student/getAllStudentsDropdown';
import createNA from '../../../../reducers/naReducer/createNA';

interface CreateNADialogProps {
    onClose: () => void;
}

const CreateNADialog = (props: CreateNADialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<CreateNAModel>({
        defaultValues: {
            studentId: null,
            dateScheduleId: null,
            reason: ''
        }
    });

    const handleCreateNA = (data: CreateNAModel) => {
        props.onClose();
        dispatch(createNA(data));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Создать пропуск
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateNA)}>
                    <Controller
                        control={control}
                        name='reason'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Причина'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='studentId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllStudentsDropdown}
                                    placeholder='Студент'
                                    className={styles.input}
                                />
                            )}
                        />
                    </Grid>
                    <Controller
                        control={control}
                        name='dateScheduleId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllDateSchedulesDropdown}
                                placeholder='Дата, подвязанная к расписанию'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CreateNADialog;