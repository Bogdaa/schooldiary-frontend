export interface UpdateNAModel {
    dateScheduleId: number | null;
    reason: string;
    studentId: number | null;
}