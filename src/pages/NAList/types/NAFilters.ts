export interface NAFilters {
    date?: string;
    subject?: string;
    studentId?: number | null;
    reason?: string;
}