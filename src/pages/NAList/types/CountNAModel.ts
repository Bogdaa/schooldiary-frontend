export interface CountNAModel {
    fromDate: string;
    toDate: string;
    studentId: number | null;
    studyClassId: number | null;
}