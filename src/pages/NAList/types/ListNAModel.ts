export interface ListNAModel {
    id: number;
    reason: string;
    student: {
        id: number,
        firstName: string;
        lastName: string;
        patronymic: string;
    },
    dateSchedule: {
        id: number;
        date: string;
        schedule: {
            id: number;
            lessonNumber: number;
            weekday: string;
            subject: {
                id: number;
                name: string;
            }
        }
    }
}