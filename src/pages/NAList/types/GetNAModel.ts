import UserType from '../../../types/UserType';

export interface GetNAModel {
    student: {
        id: number;
        firstName: string;
        lastName: string;
        patronymic: string;
        updatedAt: string;
        studyCourses: Array<
            {
                id: number;
                studyClass: {
                    id: number;
                    name: string;
                }
            }>
        grades: Array<
            {
                id: number;
                value: number;
                gradeType: string;
                dateSchedule: {
                    id: number;
                    date: string;
                }
            }>
        NAs: [],
        user: {
            id: number;
            email: string;
            role: UserType
        }
    },
    dateSchedule: {
        id: number;
        date: string;
        schedule: {
            id: number;
            lessonNumber: number;
            weekday: string;
            subject: {
                id: number,
                name: string;
            },
            studyCourse: {
                id: string;
            }
        },
        homework: [],
        NAs: [],
        grades: []
    },
    reason: string;
    id: number;
    createdAt: string;
    updatedAt: string;
}