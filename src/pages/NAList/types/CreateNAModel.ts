export interface CreateNAModel {
    studentId: number | null;
    dateScheduleId: number | null;
    reason: string;
}