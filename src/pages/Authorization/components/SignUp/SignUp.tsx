import React, {useState} from 'react';
import {
    FormControl,
    InputLabel,
    Link,
    Typography,
    MenuItem,
    Select,
    SelectChangeEvent
} from '@mui/material';
import {Paper} from '@mui/material';
import useStyles from '../../styles';
import SignUpPupil from '../SignUpPupil';
import SignUpTeacher from '../SignUpTeacher';

interface SignUpProps {
    onGoToLogin: () => void
}

const SignUp = (props: SignUpProps) => {
    const styles = useStyles();
    const [type, setType] = useState<'teacher' | 'pupil'>('pupil');

    const handleChangeType = (event: SelectChangeEvent<'teacher' | 'pupil'>) => {
        const value = event.target.value as 'teacher' | 'pupil';
        setType(value);
    };

    const isTeacher = type === 'teacher';

    return (
        <Paper elevation={4} className={styles.formPaper}>
            <form className={styles.form}>
                <Typography variant='h5'>Зарегистрироваться</Typography>
                <FormControl fullWidth className={styles.input} size='small'>
                    <InputLabel id='type'>Зарегистрироваться как</InputLabel>
                    <Select
                        labelId='type'
                        label='Зарегистрироваться как'
                        value={type}
                        onChange={handleChangeType}>
                        <MenuItem value='teacher'>Учитель</MenuItem>
                        <MenuItem value='pupil'>Ученик</MenuItem>
                    </Select>
                </FormControl>
                {isTeacher ? <SignUpTeacher/> : <SignUpPupil/>}
                <Link onClick={props.onGoToLogin} className={styles.link}>
                    Уже есть аккаунт? Войти
                </Link>
            </form>
        </Paper>
    );
};

export default SignUp;