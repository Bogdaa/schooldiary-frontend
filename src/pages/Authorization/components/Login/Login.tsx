import React from 'react';
import {Button, Link, TextField, Typography} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import {Paper} from '@mui/material';
import useStyles from '../../styles';
import {LoginModel} from '../../types/LoginModel';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import login from '../../../../reducers/authReducer/login';
import {useNavigate} from 'react-router-dom';

interface LoginProps {
    onGoToSignUp: () => void
}

const Login = (props: LoginProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const {control, handleSubmit} = useForm<LoginModel>({
        defaultValues: {
            email: '',
            password: '',
        },
    });

    const handleLogin = (form: LoginModel) => {
        dispatch(login(form, () => navigate('/')))
    }

    return (
        <Paper elevation={4} className={styles.formPaper}>
            <form className={styles.form} onSubmit={handleSubmit(handleLogin)}>
                <Typography variant='h5'>Залогинится</Typography>
                <Controller
                    control={control}
                    name='email'
                    render={({field}) => (
                        <TextField
                            type='login'
                            autoComplete='off'
                            size='small'
                            placeholder='Логин'
                            variant='outlined'
                            {...field}
                            className={styles.input}
                        />
                    )}
                />
                <Controller
                    control={control}
                    name='password'
                    render={({field}) => (
                        <TextField
                            type='password'
                            autoComplete='off'
                            size='small'
                            placeholder='Пароль'
                            variant='outlined'
                            {...field}
                            className={styles.input}
                        />
                    )}
                />
                <Button
                    type='submit'
                    color='primary'
                    variant='contained'
                    className={styles.input}>
                    Войти
                </Button>
            </form>
        </Paper>
    );
};

export default Login;