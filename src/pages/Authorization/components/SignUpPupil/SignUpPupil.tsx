import React from 'react';
import {Controller, useForm} from 'react-hook-form';
import {Button, FormControl, InputLabel, MenuItem, Select, TextField} from '@mui/material';
import SignUpPupilForm from '../../types/SignUpPupilForm';
import useStyles from '../../styles';

const SignUpPupil = () => {
    const styles = useStyles();
    const {control} = useForm<SignUpPupilForm>({
        defaultValues: {
            login: '',
            password: '',
            name: '',
            schoolYear: '',
            classNumber: 5
        },
    });

    return (
        <>
            <Controller
                control={control}
                name='name'
                render={({field}) => (
                    <TextField
                        autoComplete='off'
                        size='small'
                        placeholder='ФИО'
                        variant='outlined'
                        {...field}
                        className={styles.input}
                    />
                )}
            />
            <Controller
                control={control}
                name='schoolYear'
                render={({field}) => (
                    <TextField
                        autoComplete='off'
                        size='small'
                        placeholder='Учебный год'
                        variant='outlined'
                        {...field}
                        className={styles.input}
                    />
                )}
            />
            <Controller
                control={control}
                name='classNumber'
                render={({field}) => (
                    <FormControl fullWidth className={styles.input} size='small'>
                        <InputLabel id='classNumber'>Класс</InputLabel>
                        <Select
                            labelId='classNumber'
                            label='Класс'
                            {...field}>
                            <MenuItem value={5}>5</MenuItem>
                            <MenuItem value={6}>6</MenuItem>
                            <MenuItem value={7}>7</MenuItem>
                            <MenuItem value={8}>8</MenuItem>
                            <MenuItem value={9}>9</MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={11}>11</MenuItem>
                        </Select>
                    </FormControl>
                )}/>
            <Controller
                control={control}
                name='login'
                render={({field}) => (
                    <TextField
                        type='login'
                        autoComplete='off'
                        size='small'
                        placeholder='Логин'
                        variant='outlined'
                        {...field}
                        className={styles.input}
                    />
                )}
            />
            <Controller
                control={control}
                name='password'
                render={({field}) => (
                    <TextField
                        type='password'
                        autoComplete='off'
                        size='small'
                        placeholder='Пароль'
                        variant='outlined'
                        {...field}
                        className={styles.input}
                    />
                )}
            />
            <Button
                type='submit'
                color='primary'
                variant='contained'
                className={styles.input}>
                Зарегистрироваться
            </Button>
        </>
    );
};

export default SignUpPupil;