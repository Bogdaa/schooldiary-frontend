import React from 'react';
import {Controller, useForm} from 'react-hook-form';
import SignUpTeacherForm from '../../types/SignUpTeacherForm';
import {Button, TextField} from '@mui/material';
import useStyles from '../../styles';
import {Autocomplete} from '@mui/lab';
import subjects from '../../../../constants/subjects';

const SignUpTeacher = () => {
    const styles = useStyles();
    const {control} = useForm<SignUpTeacherForm>({
        defaultValues: {
            login: '',
            password: '',
            name: '',
            subjects: []
        }
    });

    return (
        <>
            <Controller
                control={control}
                name='name'
                render={({field}) => (
                    <TextField
                        autoComplete='off'
                        size='small'
                        placeholder='ФИО'
                        variant='outlined'
                        {...field}
                        className={styles.input}
                    />
                )}
            />
            <Autocomplete
                multiple
                options={subjects}
                getOptionLabel={(option) => option.title}
                defaultValue={[subjects[0]]}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        size='small'
                        variant='outlined'
                        label='Предметы'
                        className={styles.input}
                    />
                )}
            />
            <Controller
                control={control}
                name='login'
                render={({field}) => (
                    <TextField
                        type='login'
                        autoComplete='off'
                        size='small'
                        placeholder='Логин'
                        variant='outlined'
                        {...field}
                        className={styles.input}
                    />
                )}
            />
            <Controller
                control={control}
                name='password'
                render={({field}) => (
                    <TextField
                        type='password'
                        autoComplete='off'
                        size='small'
                        placeholder='Пароль'
                        variant='outlined'
                        {...field}
                        className={styles.input}
                    />
                )}
            />
            <Button
                type='submit'
                color='primary'
                variant='contained'
                className={styles.input}>
                Зарегистрироваться
            </Button>
        </>
    );
};

export default SignUpTeacher;