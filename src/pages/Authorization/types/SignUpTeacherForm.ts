interface SignUpTeacherForm {
    login: string;
    password: string;
    name: string;
    subjects: Array<any>;
}

export default SignUpTeacherForm;