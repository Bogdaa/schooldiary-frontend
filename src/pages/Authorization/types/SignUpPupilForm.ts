interface SignUpPupilForm {
    login: string;
    password: string;
    name: string;
    schoolYear: string;
    classNumber: number;
}

export default SignUpPupilForm;