import {makeStyles} from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    tab: {
        padding: 0,
    },
    image: {
        width: '100%',
        height: '100%',
        objectFit: 'cover',
    },
    formPaper: {
        padding: 25
    },
    form: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    input: {
        '&&': {
            marginTop: 15,
            width: 400,
        }
    },
    link: {
        '&&': {
            marginTop: 15,
        }
    },
}));

export default useStyles;
