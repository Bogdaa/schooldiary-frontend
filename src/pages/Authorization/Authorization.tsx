import React, {useState} from 'react';
import {Box} from '@mui/material';
import {TabContext, TabPanel} from '@mui/lab';
import {Navigate} from 'react-router-dom';
import authBackground from './../../assets/authBackground.jpeg';
import useStyles from './styles';
import Login from './components/Login';
import SignUp from './components/SignUp';
import {useAppSelector} from '../../reducers/rootReducer';

const Authorization = () => {
    const user = useAppSelector(s => s.auth.user);
    const styles = useStyles();

    const [tab, setTab] = useState<'Login' | 'SignUp'>('Login');

    const handleGoToLogin = () => {
        setTab('Login');
    };

    const handleGoToSignUp = () => {
        setTab('SignUp');
    };

    if (user) {
        return <Navigate to='/protected' replace/>;
    }

    return (
        <Box display='flex' height='100vh'>
            <Box flex={1}>
                <img src={authBackground} className={styles.image} alt='Background'/>
            </Box>
            <Box
                flex={1}
                display='flex'
                alignItems='center'
                justifyContent='center'>
                <Login onGoToSignUp={handleGoToSignUp}/>
            </Box>
        </Box>
    );
};

export default Authorization;