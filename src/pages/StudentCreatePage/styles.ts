import {makeStyles} from '@mui/styles';

const useStyles = makeStyles(() => ({
    formContainer: {
        padding: 20
    },
    title: {
        '&&': {
            marginBottom: 15
        }
    },
    input: {
        '&&': {
            marginBottom: 25
        }
    }
}));

export default useStyles;