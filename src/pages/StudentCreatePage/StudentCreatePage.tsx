import React from 'react';
import {Box, Button, Grid, Paper, TextField, Typography} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import useStyles from './styles';
import {useAppDispatch} from '../../reducers/rootReducer';
import {useNavigate} from 'react-router-dom';
import CreateStudentModel from './types/CreateStudentModel';
import createStudent from '../../reducers/studentReducer/createStudent';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import getAllYearsDropdownAPI from '../../api/year/getAllYearsDropdown';
import getAllClassesDropdownAPI from '../../api/class/getAllClassesDropdown';

const StudentCreatePage = () => {
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const {control, handleSubmit} = useForm<CreateStudentModel>({
        defaultValues: {
            firstName: '',
            lastName: '',
            patronymic: '',
            email: '',
            password: '',
            studyYearId: null,
            studyClassId: null
        },
    });

    const handleCreateStudent = (data: CreateStudentModel) => {
        dispatch(createStudent(data, () => navigate('/students')));
    };

    return (
        <Box>
            <form onSubmit={handleSubmit(handleCreateStudent)}>
                <Typography variant='h4' className={styles.title}>Создать студента</Typography>
                <Paper className={styles.formContainer} elevation={4}>
                    <Typography variant='h5' className={styles.input}>Профиль</Typography>
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <Controller
                                control={control}
                                name='lastName'
                                render={({field}) => (
                                    <TextField
                                        fullWidth
                                        autoComplete='off'
                                        size='small'
                                        placeholder='Фамилия'
                                        variant='outlined'
                                        {...field}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <Controller
                                control={control}
                                name='firstName'
                                render={({field}) => (
                                    <TextField
                                        fullWidth
                                        autoComplete='off'
                                        size='small'
                                        placeholder='Имя'
                                        variant='outlined'
                                        {...field}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <Controller
                                control={control}
                                name='patronymic'
                                render={({field}) => (
                                    <TextField
                                        fullWidth
                                        autoComplete='off'
                                        size='small'
                                        placeholder='Отчество'
                                        variant='outlined'
                                        {...field}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Controller
                                control={control}
                                name='studyYearId'
                                render={({field}) => (
                                    <ServerAutocomplete
                                        onChange={(_, value) => {
                                            field.onChange(value.id)
                                        }}
                                        getDataRequest={getAllYearsDropdownAPI}
                                        placeholder='Учебный год'
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Controller
                                control={control}
                                name='studyClassId'
                                render={({field}) => (
                                    <ServerAutocomplete
                                        onChange={(_, value) => {
                                            field.onChange(value.id)
                                        }}
                                        getDataRequest={getAllClassesDropdownAPI}
                                        placeholder='Класс'
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <Controller
                                control={control}
                                name='email'
                                render={({field}) => (
                                    <TextField
                                        fullWidth
                                        autoComplete='off'
                                        size='small'
                                        placeholder='Логин'
                                        variant='outlined'
                                        {...field}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <Controller
                                control={control}
                                name='password'
                                render={({field}) => (
                                    <TextField
                                        fullWidth
                                        type='password'
                                        autoComplete='off'
                                        size='small'
                                        placeholder='Пароль'
                                        variant='outlined'
                                        {...field}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={10}>

                        </Grid>
                        <Grid item xs={2}>
                            <Button
                                fullWidth
                                type='submit'
                                color='primary'
                                variant='contained'>
                                Создать
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
            </form>
        </Box>
    );
};

export default StudentCreatePage;