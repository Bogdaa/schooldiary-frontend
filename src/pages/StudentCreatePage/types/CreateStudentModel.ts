interface CreateStudentModel {
    firstName: string,
    lastName: string,
    patronymic: string;
    email: string;
    password: string;
    studyYearId: number | null;
    studyClassId: number | null;
}

export default CreateStudentModel;