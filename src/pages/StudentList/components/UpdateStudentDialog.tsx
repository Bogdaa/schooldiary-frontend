import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import ServerAutocomplete from '../../../components/shared/ServerAutocomplete';
import getAllClassesDropdownAPI from '../../../api/class/getAllClassesDropdown';
import getAllYearsDropdownAPI from '../../../api/year/getAllYearsDropdown';
import useStyles from './styles';
import {useAppDispatch} from '../../../reducers/rootReducer';
import {UpdateStudentModel} from '../types/UpdateStudentModel';
import updateStudent from '../../../reducers/studentReducer/updateStudent';

interface UpdateStudentDialog {
    onClose: () => void;
    id: number;
}

const UpdateStudentDialog = (props: UpdateStudentDialog) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<UpdateStudentModel>({
        defaultValues: {
            firstName: '',
            lastName: '',
            patronymic: '',
            studyYearId: null,
            studyClassId: null
        }
    });

    const handleUpdateStudent = (data: UpdateStudentModel) => {
        props.onClose();
        dispatch(updateStudent(data, props.id));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Редактировать студента
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleUpdateStudent)}>
                    <Controller
                        control={control}
                        name='lastName'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Фамилия'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='firstName'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Имя'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='patronymic'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Отчество'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studyClassId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllClassesDropdownAPI}
                                placeholder='Класс'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studyYearId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllYearsDropdownAPI}
                                placeholder='Учебный год'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Редактировать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default UpdateStudentDialog;