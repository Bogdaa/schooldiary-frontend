import {makeStyles} from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    filters: {
        '&&': {
            marginTop: 15
        }
    },
    table: {
        '&&': {
            marginTop: 15
        },
    },
    noItemsIndicator: {
        '&&': {
            marginTop: 15
        },
    }
}));

export default useStyles;