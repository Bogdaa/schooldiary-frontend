import React, {useEffect, useState} from 'react';
import {
    Box,
    Button,
    FormControl,
    Grid, IconButton,
    InputLabel,
    MenuItem,
    Paper,
    Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow,
    TextField,
    Typography
} from '@mui/material';
import useStyles from './styles';
import {Controller, useForm} from 'react-hook-form';
import {useNavigate} from 'react-router-dom';
import StudentFilters from './types/StudentFilters';
import getAllStudents, {setGetAllStudents} from '../../reducers/studentReducer/getAllStudents';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import DeleteDialog from '../../components/shared/DeleteDialog';
import deleteStudent from '../../reducers/studentReducer/deleteStudent';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import NoItems from '../../components/UI/NoItems';
import UpdateStudentDialog from './components';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import getAllClassesDropdownAPI from '../../api/class/getAllClassesDropdown';
import getAllYearsDropdownAPI from '../../api/year/getAllYearsDropdown';

const StudentList = () => {
    const [updateStudentId, setUpdateStudentId] = useState<number | null>(null);
    const [deleteStudentId, setDeleteStudentId] = useState<number | null>(null);
    const [modal, setModal] = useState<'DeleteStudentDialog' | 'UpdateStudentDialog' | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const students = useAppSelector(s => s.student.dataList);
    const {control, handleSubmit} = useForm<StudentFilters>({
        defaultValues: {
            firstName: '',
            lastName: '',
            patronymic: '',
            studyYearId: null,
            studyClassId: null
        },
    });

    useEffect(() => {
        dispatch(getAllStudents({}));

        return () => {
            dispatch(setGetAllStudents(null));
        };
    }, []);

    const handleGoToCreateStudentPage = () => {
        navigate('/students/new');
    };

    const handleDeleteStudent = () => {
        setModal(null);
        dispatch(deleteStudent(deleteStudentId!, () => setDeleteStudentId(null)));
    };

    const handleDeleteModalOpen = (id: number) => () => {
        setDeleteStudentId(id);
        setModal('DeleteStudentDialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteStudentId(null);
        setModal(null);
    };

    const handleUpdateModalOpen = (id: number) => () => {
        setUpdateStudentId(id);
        setModal('UpdateStudentDialog');
    };

    const handleUpdateModalClose = () => {
        setUpdateStudentId(null);
        setModal(null);
    };

    const handleSearch = (data: StudentFilters) => {
        //@ts-ignore
        dispatch(getAllStudents(Object.fromEntries(Object.entries(data)
            .filter(([_, value]) => value)) as StudentFilters))
    }

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Ученики</Typography>
                <Button
                    color='primary'
                    size='medium'
                    variant='contained'
                    onClick={handleGoToCreateStudentPage}>
                    Создать ученика
                </Button>
            </Box>
            <form onSubmit={handleSubmit(handleSearch)} className={styles.filters}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='lastName'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Фамилия'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='firstName'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Имя'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='patronymic'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Отчество'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='studyClassId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllClassesDropdownAPI}
                                    placeholder='Класс'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='studyYearId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllYearsDropdownAPI}
                                    placeholder='Учебный год'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}></Grid>
                    <Grid item xs={10}></Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            type='submit'
                            color='primary'
                            variant='contained'>
                            Искать
                        </Button>
                    </Grid>
                </Grid>
            </form>
            {students && students.length !== 0 && (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>ФИО</TableCell>
                                <TableCell align='center' width={70}>Изменить</TableCell>
                                <TableCell align='center' width={70}>Удалить</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {students.map((student) => (
                                <TableRow
                                    key={student.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {student.lastName} {student.firstName} {student.patronymic}
                                    </TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='success' size='medium' onClick={handleUpdateModalOpen(student.id)}>
                                            <EditIcon/>
                                        </IconButton>
                                    </TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='error' size='medium'
                                                    onClick={handleDeleteModalOpen(student.id)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            )}
            {students?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет учеников в системе'/>
            )}
            {modal === 'DeleteStudentDialog' && (
                <DeleteDialog
                    title='Удаление студента'
                    body='Вы действительно хотите удалить этого студента?'
                    onDelete={handleDeleteStudent}
                    onClose={handleDeleteModalClose} />
            )}
            {modal === 'UpdateStudentDialog' && (
                <UpdateStudentDialog onClose={handleUpdateModalClose} id={updateStudentId!} />
            )}
        </Box>
    );
};

export default StudentList;