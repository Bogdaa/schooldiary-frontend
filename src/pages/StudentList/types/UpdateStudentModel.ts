export interface UpdateStudentModel {
    firstName: string;
    lastName: string;
    patronymic: string;
    studyYearId: number | null;
    studyClassId: number | null;
}