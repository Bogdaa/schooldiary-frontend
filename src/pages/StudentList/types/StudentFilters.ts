interface StudentFilters {
    firstName?: string;
    lastName?: string;
    patronymic?: string;
    studyYearId?: number | null;
    studyClassId?: number | null;
}

export default StudentFilters;