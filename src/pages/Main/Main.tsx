import React from 'react';
import {Box, Typography} from '@mui/material';
import useStyles from './styles';
import {useAppSelector} from '../../reducers/rootReducer';
import roleMap from '../../constants/roleMap';

const Main = () => {
    const styles = useStyles();
    const user = useAppSelector(s => s.auth.user);

    return (
        <Box>
            <Typography variant='h4'>Здравствуйте, {user!.email}</Typography>
            <Typography variant='h5' className={styles.subTitle}>Роль: {roleMap[user!.role]}</Typography>
        </Box>
    )
}

export default Main;