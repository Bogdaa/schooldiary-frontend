import {makeStyles} from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    subTitle: {
        '&&': {
            marginTop: 10,
        }
    }
}));

export default useStyles;