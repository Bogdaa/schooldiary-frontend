export interface HomeworksFilter {
    description?: string | null;
    deadline?: string | null;
    dateScheduleId?: number | null;
    subjectId?: number | null;
    teacherId?: number | null;
    studyClassId?: number | null;
}