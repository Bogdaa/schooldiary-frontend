export interface UpdateHomeworkModel {
    description: string;
    deadline: string;
    date: string;
}