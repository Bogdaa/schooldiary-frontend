export interface GetHomeworkModel {
    id: number,
    description: string;
    deadline: string;
    dateSchedule: {
        id: number;
        date: string;
        schedule: {
            id: number;
            lessonNumber: number;
            studyCourse: {
                id: number
                studyClass: {
                    id: number;
                    name: string;
                }
            }
            weekday: number;
            teacher: {
                id: number;
                firstName: string;
                lastName: string;
                patronymic: string;
            },
            subject: {
                id: number,
                name: string;
            }
        }
    } | null
    createdAt: string;
    updatedAt: string;
}