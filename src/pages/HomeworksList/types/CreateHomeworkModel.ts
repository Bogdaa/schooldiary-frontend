export interface CreateHomeworkModel {
    description: string;
    deadline: string;
    dateScheduleId: number | null;
}