import React, {useEffect, useState} from 'react';
import {
    Box,
    Button,
    Grid, IconButton,
    Paper,
    Table, TableBody, TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography
} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import {HomeworksFilter} from './types/HomeworksFilter';
import useStyles from './styles';
import getAllHomeworks, {setGetAllHomeworks} from '../../reducers/homeworkReducer/getAllHomeworks';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import NoItems from '../../components/UI/NoItems';
import getAllSubjectsDropdownAPI from '../../api/subject/getAllSubjectsDropdown';
import getAllTeachersDropdownAPI from '../../api/teacher/getAllTeachersDropdown';
import getAllClassesDropdownAPI from '../../api/class/getAllClassesDropdown';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import deleteHomework from '../../reducers/homeworkReducer/deleteHomework';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import DeleteDialog from '../../components/shared/DeleteDialog';
import CreateHomeworkDialog from './components/CreateHomeworkDialog';
import {ListHomeworkModel} from './types/ListHomeworkModel';
import UpdateHomeworkDialog from './components/UpdateHomeworkDialog';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';
import getAllDateSchedulesDropdown from '../../api/dateSchedule/getAllDateSchedulesDropdown';
import Permission from '../../components/shared/Permission';

const HomeworksList = () => {
    const [modal, setModal] = useState<'DeleteHomeworkDialog' | 'CreateHomeworkDialog' |
        'UpdateHomeworkDialog' | null>(null);
    const [deleteHomeworkId, setDeleteHomeworkId] = useState<number | null>(null);
    const [updateHomework, setUpdateHomework] = useState<ListHomeworkModel | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const homeworks = useAppSelector(s => s.homework.dataList);

    const {control, handleSubmit} = useForm<HomeworksFilter>({
        defaultValues: {
            description: null,
            deadline: null,
            dateScheduleId: null,
            subjectId: null,
            teacherId: null,
            studyClassId: null
        },
    });

    useEffect(() => {
        dispatch(getAllHomeworks({}))

        return () => {
            dispatch(setGetAllHomeworks(null))
        }
    }, [])

    const handleSearch = (data: HomeworksFilter) => {
        //@ts-ignore
        dispatch(getAllHomeworks(Object.fromEntries(Object.entries(data)
            .filter(([_, value]) => value)) as HomeworksFilter))
    }

    const handleDeleteHomework = () => {
        setModal(null);
        dispatch(deleteHomework(deleteHomeworkId!, () => setDeleteHomeworkId(null)));
    };

    const handleDeleteHomeworkOpen = (id: number) => () => {
        setDeleteHomeworkId(id);
        setModal('DeleteHomeworkDialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteHomeworkId(null);
        setModal(null);
    };

    const handleUpdateHomeworkOpen = (homework: ListHomeworkModel) => () => {
        setUpdateHomework(homework);
        setModal('UpdateHomeworkDialog');
    };

    const handleUpdateModalClose = () => {
        setUpdateHomework(null);
        setModal(null);
    };

    const handleCreateModalOpen = () => {
        setModal('CreateHomeworkDialog')
    };

    const handleCreateModalClose = () => {
        setModal(null)
    };

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Домашние задания</Typography>
                <Permission forRoles={['admin', 'teacher']}>
                    <Button
                        color='primary'
                        size='medium'
                        variant='contained'
                        onClick={handleCreateModalOpen}>
                        Создать домашнее задание
                    </Button>
                </Permission>
            </Box>
            <form className={styles.filters} onSubmit={handleSubmit(handleSearch)}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='description'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Описание'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='deadline'
                            render={({field}) => (
                                <DesktopDatePicker
                                    {...field}
                                    label='Крайний срок'
                                    inputFormat='YYYY/MM/DD'
                                    renderInput={(params) => <TextField
                                        {...params}
                                        fullWidth
                                        autoComplete='off'
                                        size='small'
                                        variant='outlined'
                                    />}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='dateScheduleId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllDateSchedulesDropdown}
                                    placeholder='Расписание, привязано к дате'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='subjectId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllSubjectsDropdownAPI}
                                    placeholder='Предмет'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='teacherId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllTeachersDropdownAPI}
                                    placeholder='Учитель'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='studyClassId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllClassesDropdownAPI}
                                    placeholder='Класс'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            type='submit'
                            color='primary'
                            variant='contained'>
                            Искать
                        </Button>
                    </Grid>
                </Grid>
            </form>
            {homeworks && homeworks.length ? (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell align='left' width='auto'>Описание</TableCell>
                                <TableCell align='left' width='auto'>Крайний срок</TableCell>
                                <TableCell align='left' width='auto'>Учитель</TableCell>
                                <TableCell align='left' width='auto'>Урок</TableCell>
                                <TableCell align='left' width='auto'>Класс</TableCell>
                                <Permission forRoles={['admin', 'teacher']}>
                                    <TableCell align='center' width='auto'>Редактировать</TableCell>
                                    <TableCell align='center' width={70}>Удалить</TableCell>
                                </Permission>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {homeworks.map((homework) => (
                                <TableRow
                                    key={homework.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {/*@ts-ignore*/}
                                        {homework.description}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {homework.deadline}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {homework.dateSchedule?.schedule && `
                                            ${homework.dateSchedule.schedule.teacher.lastName} 
                                            ${homework.dateSchedule.schedule.teacher.firstName} 
                                            ${homework.dateSchedule.schedule.teacher.patronymic}
                                        `}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {homework.dateSchedule?.schedule && homework.dateSchedule.schedule.subject.name}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {homework.dateSchedule?.schedule && homework.dateSchedule?.schedule.studyCourse.studyClass.name}
                                    </TableCell>
                                    <Permission forRoles={['admin', 'teacher']}>
                                        <TableCell align='center'>
                                            <IconButton color='success' size='medium'
                                                        onClick={handleUpdateHomeworkOpen(homework)}>
                                                <EditIcon/>
                                            </IconButton>
                                        </TableCell>
                                        <TableCell align='center'>
                                            <IconButton color='error' size='medium'
                                                        onClick={handleDeleteHomeworkOpen(homework.id)}>
                                                <DeleteIcon/>
                                            </IconButton>
                                        </TableCell>
                                    </Permission>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {homeworks?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет домашних заданий в системе'/>
            )}
            {modal === 'DeleteHomeworkDialog' && (
                <DeleteDialog
                    title='Удаление дз'
                    body='Вы действительно хотите удалить это дз?'
                    onDelete={handleDeleteHomework}
                    onClose={handleDeleteModalClose}/>
            )}
            {modal === 'CreateHomeworkDialog' && (
                <CreateHomeworkDialog onClose={handleCreateModalClose}/>
            )}
            {modal === 'UpdateHomeworkDialog' && (
                <UpdateHomeworkDialog onClose={handleUpdateModalClose} homework={updateHomework}/>
            )}
        </Box>
    )
}

export default HomeworksList;