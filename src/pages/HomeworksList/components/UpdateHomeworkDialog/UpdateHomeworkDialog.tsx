import React, {useEffect} from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import dayjs from 'dayjs';
import updateHomework from '../../../../reducers/homeworkReducer/updateHomework';
import {UpdateHomeworkModel} from '../../types/UpdateHomeworkModel';
import useStyles from './styles';
import {ListHomeworkModel} from '../../types/ListHomeworkModel';

interface UpdateHomeworkDialogProps {
    onClose: () => void;
    homework: ListHomeworkModel | null
}

const UpdateHomeworkDialog = (props: UpdateHomeworkDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit, setValue} = useForm<UpdateHomeworkModel>({
        defaultValues: {
            description: '',
            deadline: dayjs().format('YYYY-MM-DD'),
            date: ''
        }
    });

    useEffect(() => {
        setValue('description', props.homework!.description)
        setValue('deadline', dayjs(props.homework!.deadline).format('YYYY-MM-DD'))
        setValue('date', dayjs(props.homework!.dateSchedule?.date).format('YYYY-MM-DD'))
    }, [])

    const handleUpdateHomework = (data: UpdateHomeworkModel) => {
        props.onClose();
        dispatch(updateHomework(data, props.homework!.id));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Обновить дз
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleUpdateHomework)}>
                    <Controller
                        control={control}
                        name='description'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Описание'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='deadline'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Крайний срок'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='date'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Дата'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Обновить
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default UpdateHomeworkDialog;