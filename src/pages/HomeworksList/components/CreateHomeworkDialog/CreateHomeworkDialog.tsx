import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import useStyles from './styles';
import dayjs from 'dayjs';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {CreateHomeworkModel} from '../../types/CreateHomeworkModel';
import createHomework from '../../../../reducers/homeworkReducer/createHomework';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllDateSchedulesDropdown from '../../../../api/dateSchedule/getAllDateSchedulesDropdown';

interface CreateHomeworkDialogProps {
    onClose: () => void;
}

const CreateHomeworkDialog = (props: CreateHomeworkDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<CreateHomeworkModel>({
        defaultValues: {
            description: '',
            deadline: dayjs().format('YYYY-MM-DD'),
            dateScheduleId: null,
        }
    });

    const handleCreateHomework = (data: CreateHomeworkModel) => {
        props.onClose();
        dispatch(createHomework(data));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Создать дз
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateHomework)}>
                    <Controller
                        control={control}
                        name='description'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Описание'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='deadline'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Крайний срок'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='dateScheduleId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllDateSchedulesDropdown}
                                placeholder='Дата, подвязанная к расписанию'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CreateHomeworkDialog;