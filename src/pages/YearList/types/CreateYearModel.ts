export interface CreateYearModel {
    startDate: string;
    endDate: string;
}
