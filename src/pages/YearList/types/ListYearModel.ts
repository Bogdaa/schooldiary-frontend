export interface ListYearModel {
    id: number,
    startDate: string,
    endDate: string;
}