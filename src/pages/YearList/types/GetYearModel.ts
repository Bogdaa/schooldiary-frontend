export interface GetYearModel {
    start_date: string;
    end_date: string;
    id: number;
    createdAt: string;
    updatedAt: string;
}