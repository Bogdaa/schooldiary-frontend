import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import useStyles from './styles';
import {CreateYearModel} from '../../types/CreateYearModel';

interface YearDialogProps {
    onClose: () => void;
    title: string;
    buttonText: string;
    onSubmit: (subject: CreateYearModel, id: number | null) => void,
    id: number | null;
}

const YearDialog = (props: YearDialogProps) => {
    const styles = useStyles();

    const {control, handleSubmit} = useForm<CreateYearModel>({
        defaultValues: {
            startDate: dayjs().format('YYYY-MM-DD'),
            endDate: dayjs().format('YYYY-MM-DD')
        },
    });

    const handleCreateYear = (data: CreateYearModel) => {
        props.onClose();
        props.onSubmit(data, props.id);
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                {props.title}
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateYear)}>
                    <Controller
                        control={control}
                        name='startDate'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Начало периода'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='endDate'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Конец периода'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        {props.buttonText}
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    );
};

export default YearDialog;