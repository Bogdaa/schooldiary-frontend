import React, {useEffect, useState} from 'react';
import {
    Box,
    Button, IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography
} from '@mui/material';
import useStyles from './styles';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import NoItems from '../../components/UI/NoItems';
import getAllYears, {setGetAllYears} from '../../reducers/yearReducer/getAllYears';
import dayjs from 'dayjs/esm';
import YearDialog from './components/YearDialog';
import DeleteIcon from '@mui/icons-material/Delete';
import deleteYear from '../../reducers/yearReducer/deleteYear';
import DeleteDialog from '../../components/shared/DeleteDialog';
import createYear from '../../reducers/yearReducer/createYear';
import {CreateYearModel} from './types/CreateYearModel';
import updateYear from '../../reducers/yearReducer/updateYear';
import EditIcon from '@mui/icons-material/Edit';

const YearList = () => {
    const [deleteYearId, setDeleteYearId] = useState<number | null>(null);
    const [updateYearId, setUpdateYearId] = useState<number | null>(null);
    const [modal, setModal] = useState<'CreateYearDialog' | 'DeleteYearDialog' | 'UpdateYearDialog' | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const years = useAppSelector(s => s.year.dataList);

    useEffect(() => {
        dispatch(getAllYears);

        return () => {
            dispatch(setGetAllYears(null));
        };
    }, []);

    const handleDeleteYear = () => {
        setModal(null);
        dispatch(deleteYear(deleteYearId!, () => setDeleteYearId(null)));
    };

    const handleDeleteModalOpen = (id: number) => () => {
        setDeleteYearId(id);
        setModal('DeleteYearDialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteYearId(null);
        setModal(null)
    };

    const handleCreateModalOpen = () => {
        setModal('CreateYearDialog')
    };

    const handleCreateModalClose = () => {
        setModal(null)
    };

    const handleCreateYear = (data: CreateYearModel, id: number | null) => {
        dispatch(createYear(data, id));
    };

    const handleUpdateYear = (data: CreateYearModel, id: number | null) => {
        dispatch(updateYear(data, id!));
    };

    const handleUpdateModalOpen = (id: number) => () => {
        setUpdateYearId(id);
        setModal('UpdateYearDialog');
    };

    const handleUpdateModalClose = () => {
        setUpdateYearId(null);
        setModal(null);
    };

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Периоды</Typography>
                <Button
                    color='primary'
                    size='medium'
                    variant='contained'
                    onClick={handleCreateModalOpen}>
                    Создать период
                </Button>
            </Box>
            {years && years.length ? (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Начало года</TableCell>
                                <TableCell align='left' width='auto'>Конец года</TableCell>
                                <TableCell align='left' width={70}>Редактировать</TableCell>
                                <TableCell align='center' width={70}>Удалить</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {years.map((year) => (
                                <TableRow
                                    key={year.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {dayjs(year.startDate).format('YYYY-MM-DD')}
                                    </TableCell>
                                    <TableCell align='left'>
                                        {dayjs(year.endDate).format('YYYY-MM-DD')}
                                    </TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='success' size='medium'
                                                    onClick={handleUpdateModalOpen(year.id)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='error' size='medium'
                                                    onClick={handleDeleteModalOpen(year.id)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {years?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет периодов в системе'/>
            )}
            {modal === 'CreateYearDialog' && (
                <YearDialog
                    title='Создание периода'
                    buttonText='Создать'
                    onClose={handleCreateModalClose}
                    onSubmit={handleCreateYear}
                    id={null}/>
            )}
            {modal === 'UpdateYearDialog' && (
                <YearDialog
                    title='Редактирование периода'
                    buttonText='Создать'
                    onClose={handleUpdateModalClose}
                    onSubmit={handleUpdateYear}
                    id={updateYearId}/>
            )}
            {modal === 'DeleteYearDialog' && (
                <DeleteDialog
                    title='Удаление периода'
                    body='Вы действительно хотите удалить этот период?'
                    onDelete={handleDeleteYear}
                    onClose={handleDeleteModalClose} />
            )}
        </Box>
    )
}

export default YearList;