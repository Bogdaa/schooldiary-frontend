import React, {useEffect, useState} from 'react';
import {
    Box,
    Button, Grid, IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField,
    Typography
} from '@mui/material';
import getAllCourses, {setGetAllCourses} from '../../reducers/studyCourseReducer/gelAllCourses';
import useStyles from '../SubjectList/styles';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import EditIcon from '@mui/icons-material/Edit';
import NoItems from '../../components/UI/NoItems';
import CreateCourseDialog from './components/CreateCourseDialog/CreateCourseDialog';
import DeleteStudentFromCourse from './components/DeleteStudentFromCourse/DeleteStudentFromCourse';
import {Controller, useForm} from 'react-hook-form';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import {StudyCourseFilters} from './types/StudyCourseFilters';
import getAllClassesDropdownAPI from '../../api/class/getAllClassesDropdown';
import getAllStudentsDropdown from '../../api/student/getAllStudentsDropdown';
import getAllYearsDropdown from '../../api/year/getAllYearsDropdown';


const StudyCourseList = () => {
    const [courseId, setCourseId] = useState<number | null>(null);
    const [modal, setModal] = useState<'CreateStudyCourseDialog' | 'DeleteStudentFromCourseDialog' | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const courses = useAppSelector(s => s.studyCourse.dataList);
    const {control, handleSubmit} = useForm<StudyCourseFilters>({
        defaultValues: {
            studentId: null,
            classId: null,
            studyYearId: null
        },
    });

    useEffect(() => {
        dispatch(getAllCourses({}));

        return () => {
            dispatch(setGetAllCourses(null));
        };
    }, [])

    const handleCreateModalOpen = () => {
        setModal('CreateStudyCourseDialog');
    };

    const handleCreateModalClose = () => {
        setModal(null);
    };

    const handleDeleteStudentFromCourseModalOpen = (id: number) => () => {
        setCourseId(id);
        setModal('DeleteStudentFromCourseDialog');
    };

    const handleDeleteStudentFromCourseModalClose = () => {
        setCourseId(null);
        setModal(null);
    };

    const handleSearch = (data: StudyCourseFilters) => {
        //@ts-ignore
        dispatch(getAllCourses(Object.fromEntries(Object.entries(data)
            .filter(([_, value]) => value)) as StudyCourseFilters))
    }

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Курсы</Typography>
                <Button
                    color='primary'
                    size='medium'
                    variant='contained'
                    onClick={handleCreateModalOpen}>
                    Создать курс
                </Button>
            </Box>
            <form className={styles.filters} onSubmit={handleSubmit(handleSearch)}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='classId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllClassesDropdownAPI}
                                    placeholder='Класс'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='studyYearId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllYearsDropdown}
                                    placeholder='Учебный год'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='studentId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllStudentsDropdown}
                                    placeholder='Студент'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            type='submit'
                            color='primary'
                            variant='contained'>
                            Искать
                        </Button>
                    </Grid>
                </Grid>
            </form>
            {courses && courses.length !== 0 && (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Класс</TableCell>
                                <TableCell>Начало года</TableCell>
                                <TableCell>Конец года</TableCell>
                                <TableCell align='center' width={70}>Удалить ученика</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {courses.map((course) => (
                                <TableRow
                                    key={course.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>{course.studyClass.name}</TableCell>
                                    <TableCell component='th'>{course.studyYear.startDate}</TableCell>
                                    <TableCell component='th'>{course.studyYear.endDate}</TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='success' size='medium' onClick={handleDeleteStudentFromCourseModalOpen(course.id)}>
                                            <EditIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            )}
            {courses?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет курсов в системе'/>
            )}
            {modal === 'CreateStudyCourseDialog' && <CreateCourseDialog onClose={handleCreateModalClose} />}
            {modal === 'DeleteStudentFromCourseDialog' &&
                <DeleteStudentFromCourse
                    onClose={handleDeleteStudentFromCourseModalClose}
                    courseId={courseId!} />
            }
        </Box>
    )
}

export default StudyCourseList;