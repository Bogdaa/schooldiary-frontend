export interface GetCourseModel {
    studyClass: {
        id: number,
        name: string,
    },
    studyYear: {
        id: number,
        startDate: string,
        endDate: string,
    },
    id: number,
    createdAt: string,
    updatedAt: string
}