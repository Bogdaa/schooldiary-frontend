export interface CreateCourseModel {
    classId: number | null;
    studyYearId: number | null;
}