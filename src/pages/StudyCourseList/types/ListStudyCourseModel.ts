export interface ListStudyCourseModel {
    id: number,
    studyClass: {
        id: number,
        name: string
    },
    studyYear: {
        id: number,
        startDate: string,
        endDate: string
    }
}