export interface DeleteStudentFromCourseModel {
    studentId: number | null;
    studyCourseId: number;
}