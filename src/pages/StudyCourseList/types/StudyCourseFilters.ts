export interface StudyCourseFilters {
    studentId?: number | null;
    classId?: number | null;
    studyYearId?: number | null
}