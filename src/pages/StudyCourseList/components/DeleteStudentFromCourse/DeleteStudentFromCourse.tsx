import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import useStyles from './styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {DeleteStudentFromCourseModel} from '../../types/DeleteStudentFromCourseModel';
import getStudentsByStudyYearDropdown from '../../../../api/student/getStudentsByStudyYearDropdown';
import deleteStudentFromCourse from '../../../../reducers/studyCourseReducer/deleteStudentFromCourse';

interface CreateCourseDialogProps {
    onClose: () => void;
    courseId: number;
}

const DeleteStudentFromCourse = (props: CreateCourseDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<Pick<DeleteStudentFromCourseModel, 'studentId'>>({
        defaultValues: {
            studentId: null
        }
    });

    const handleDeleteStudentFromCourse = (data: Pick<DeleteStudentFromCourseModel, 'studentId'>) => {
        props.onClose();
        dispatch(deleteStudentFromCourse(data.studentId!, props.courseId))
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Удалить студента из курса
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleDeleteStudentFromCourse)}>
                    <Controller
                        control={control}
                        name='studentId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={() => getStudentsByStudyYearDropdown(props.courseId)}
                                placeholder='Студент'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Удалить
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default DeleteStudentFromCourse;