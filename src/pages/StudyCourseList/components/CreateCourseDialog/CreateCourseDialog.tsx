import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import useStyles  from './styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {CreateCourseModel} from '../../types/CreateCourseModel';
import createCourse from '../../../../reducers/studyCourseReducer/createCourse';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllClassesDropdownAPI from '../../../../api/class/getAllClassesDropdown';
import getAllYearsDropdownAPI from '../../../../api/year/getAllYearsDropdown';

interface CreateCourseDialogProps {
    onClose: () => void;
}

const CreateCourseDialog = (props: CreateCourseDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<CreateCourseModel>({
        defaultValues: {
            classId: null,
            studyYearId: null,
        }
    });

    const handleCreateClass = (data: CreateCourseModel) => {
        props.onClose();
        dispatch(createCourse(data));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Создать курс
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateClass)}>
                    <Controller
                        control={control}
                        name='classId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllClassesDropdownAPI}
                                placeholder='Класс'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studyYearId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllYearsDropdownAPI}
                                placeholder='Учебный год'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CreateCourseDialog;