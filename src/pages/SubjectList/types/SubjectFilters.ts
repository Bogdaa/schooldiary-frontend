export interface SubjectFilters {
    subjectName?: string;
    teacherId?: number | null;
}