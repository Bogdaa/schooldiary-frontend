export interface CreateSubjectModel {
    name: string;
    teacherId: number | null;
}