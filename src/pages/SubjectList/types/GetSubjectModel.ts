export interface GetSubjectModel {
    name: string;
    teachers: Array<{
        id: number;
        firstName: string;
        lastName: string;
        patronymic: string;
    }>;
    id: number;
    createdAt: string;
    updatedAt: string;
}