export interface ListSubjectModel {
    id: number;
    name: string;
    teachers: Array<{
        id: number;
        firstName: string;
        lastName: string;
        patronymic: string;
    }>
}