import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import useStyles  from './styles';
import {CreateSubjectModel} from '../../types/CreateSubjectModel';
import getAllTeachersDropdownAPI from '../../../../api/teacher/getAllTeachersDropdown';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';

interface SubjectDialogProps {
    onClose: () => void;
    title: string;
    buttonText: string;
    onSubmit: (subject: CreateSubjectModel, id: number | null) => void,
    id: number | null;
}

const SubjectDialog = (props: SubjectDialogProps) => {
    const styles = useStyles();

    const {control, handleSubmit} = useForm<CreateSubjectModel>({
        defaultValues: {
            name: '',
            teacherId: null
        },
    });

    const handleCreateClass = (data: CreateSubjectModel) => {
        props.onClose();
        props.onSubmit(data, props.id);
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                {props.title}
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateClass)}>
                    <Controller
                        control={control}
                        name='name'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Название'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='teacherId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllTeachersDropdownAPI}
                                placeholder='Учитель'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        {props.buttonText}
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default SubjectDialog;