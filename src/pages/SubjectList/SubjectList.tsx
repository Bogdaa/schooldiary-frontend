import React, {useEffect, useState} from 'react';
import {
    Box, Button,
    Grid, IconButton,
    Paper,
    Table, TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography
} from '@mui/material';
import useStyles from './styles';
import {Controller, useForm} from 'react-hook-form';
import {SubjectFilters} from './types/SubjectFilters';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import getAllSubjects, {setGetAllSubjects} from '../../reducers/subjectReducer/getAllSubjects';
import deleteSubject from '../../reducers/subjectReducer/deleteSubject';
import SubjectDialog from './components/SubjectDialog';
import NoItems from '../../components/UI/NoItems';
import DeleteDialog from '../../components/shared/DeleteDialog';
import {CreateSubjectModel} from './types/CreateSubjectModel';
import createSubject from '../../reducers/subjectReducer/createSubject';
import updateSubject from '../../reducers/subjectReducer/updateSubject';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import getAllTeachersDropdownAPI from '../../api/teacher/getAllTeachersDropdown';

const SubjectList = () => {
    const [modal, setModal] = useState<'CreateSubjectDialog' | 'DeleteSubjectDialog'
        | 'UpdateSubjectDialog' | null>(null);
    const [deleteSubjectId, setDeleteSubjectId] = useState<number | null>(null);
    const [updateSubjectId, setUpdateSubjectId] = useState<number | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const subjects = useAppSelector(s => s.subject.dataList);
    const {control, handleSubmit} = useForm<SubjectFilters>({
        defaultValues: {
            subjectName: '',
            teacherId: null
        },
    });

    useEffect(() => {
        dispatch(getAllSubjects({}));

        return () => {
            dispatch(setGetAllSubjects(null));
        };
    }, []);

    const handleSearch = (data: SubjectFilters) => {
        //@ts-ignore
        dispatch(getAllSubjects(Object.fromEntries(Object.entries(data)
            .filter(([_, value]) => value)) as SubjectFilters))
    }


    const handleCreateModalOpen = () => {
        setModal('CreateSubjectDialog');
    };

    const handleCreateModalClose = () => {
        setModal(null);
    };

    const handleDeleteSubject = () => {
        setModal(null);
        dispatch(deleteSubject(deleteSubjectId!, () => setDeleteSubjectId(null)));
    };

    const handleCreateSubject = (data: CreateSubjectModel, id: number | null) => {
        dispatch(createSubject(data, id));
    };

    const handleUpdateSubject = (data: CreateSubjectModel, id: number | null) => {
        dispatch(updateSubject(data, id!));
    };

    const handleDeleteModalOpen = (id: number) => () => {
        setDeleteSubjectId(id);
        setModal('DeleteSubjectDialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteSubjectId(null);
        setModal(null);
    };

    const handleUpdateModalOpen = (id: number) => () => {
        setUpdateSubjectId(id);
        setModal('UpdateSubjectDialog');
    };

    const handleUpdateModalClose = () => {
        setUpdateSubjectId(null);
        setModal(null);
    };

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Предметы</Typography>
                <Button
                    color='primary'
                    size='medium'
                    variant='contained'
                    onClick={handleCreateModalOpen}>
                    Создать предмет
                </Button>
            </Box>
            <form className={styles.filters} onSubmit={handleSubmit(handleSearch)}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='teacherId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllTeachersDropdownAPI}
                                    placeholder='Учитель'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='subjectName'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Название предмета'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}></Grid>
                    <Grid item xs={10}></Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            type='submit'
                            color='primary'
                            variant='contained'>
                            Искать
                        </Button>
                    </Grid>
                </Grid>
            </form>
            {subjects && subjects.length ? (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Название</TableCell>
                                <TableCell align='left' width='auto'>Учителя</TableCell>
                                <TableCell align='left' width={70}>Редактировать</TableCell>
                                <TableCell align='center' width={70}>Удалить</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {subjects.map((subject) => (
                                <TableRow
                                    key={subject.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {subject.name}
                                    </TableCell>
                                    <TableCell align='left'>{subject.teachers.map(teacher => {
                                        return `${teacher.lastName} ${teacher.firstName} ${teacher.patronymic}`;
                                    }).join(',')}</TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='success' size='medium'
                                                    onClick={handleUpdateModalOpen(subject.id)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='error' size='medium'
                                                    onClick={handleDeleteModalOpen(subject.id)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {subjects?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет предметов в системе'/>
            )}
            {modal === 'DeleteSubjectDialog' && (
                <DeleteDialog
                    title='Удаление предмета'
                    body='Вы действительно хотите удалить этот предмет?'
                    onDelete={handleDeleteSubject}
                    onClose={handleDeleteModalClose}/>
            )}
            {modal === 'CreateSubjectDialog' && (
                <SubjectDialog
                    onClose={handleCreateModalClose}
                    onSubmit={handleCreateSubject}
                    title='Создать предмет'
                    buttonText='Создать'
                    id={null}
                />
            )}
            {modal === 'UpdateSubjectDialog' && (
                <SubjectDialog
                    onClose={handleUpdateModalClose}
                    onSubmit={handleUpdateSubject}
                    title='Редактировать предмет'
                    buttonText='Редактировать'
                    id={updateSubjectId}
                />
            )}
        </Box>
    );
};

export default SubjectList;