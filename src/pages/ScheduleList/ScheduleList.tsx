import React, {useEffect, useState} from 'react';
import {
    Box,
    Button, Grid, IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField,
    Typography
} from '@mui/material';
import getAllSchedules, {setGetAllSchedules} from '../../reducers/scheduleReducer/getAllSchedules';
import useStyles from '../ClassList/styles';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import DeleteIcon from '@mui/icons-material/Delete';
import NoItems from '../../components/UI/NoItems';
import weekdayMap from '../../constants/weekdayMap';
import deleteSchedule from '../../reducers/scheduleReducer/deleteSchedule';
import DeleteDialog from '../../components/shared/DeleteDialog';
import CreateScheduleDialog from './components/CreateScheduleDialog';
import CreateDateScheduleDialog from './components/CreateDateScheduleDialog';
import EditIcon from '@mui/icons-material/Edit';
import DeleteDateScheduleDialog from './components/DeleteDateScheduleDialog';
import UpdateScheduleDialog from './components/UpdateScheduleDialog';
import {Controller, useForm} from 'react-hook-form';
import ServerAutocomplete from '../../components/shared/ServerAutocomplete';
import getAllTeachersDropdownAPI from '../../api/teacher/getAllTeachersDropdown';
import {ScheduleFilters} from './types/ScheduleFilters';
import getAllCoursesDropdownAPI from '../../api/course/getAllCoursesDropdown';
import getAllDateSchedulesDropdown from '../../api/dateSchedule/getAllDateSchedulesDropdown';
import Permission from '../../components/shared/Permission';

const ScheduleList = () => {
    const [modal, setModal] = useState<'DeleteScheduleDialog' | 'CreateScheduleDialog'
        | 'CreateDateScheduleDialog' | 'DeleteDateScheduleDialog' | 'UpdateScheduleDialog' | null>(null);
    const [updateScheduleId, setUpdateScheduleId] = useState<number | null>(null);
    const [deleteScheduleId, setDeleteScheduleId] = useState<number | null>(null);
    const [createDateScheduleId, setCreateDateScheduleId] = useState<number | null>(null);
    const [deleteDateSchedule, setDeleteDateSchedule] = useState<{
        id: number,
        dateSchedule: Array<{ id: number, date: string }>
    } | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const schedules = useAppSelector(s => s.schedule.dataList);
    const {control, handleSubmit} = useForm<ScheduleFilters>({
        defaultValues: {
            className: '',
            dateScheduleId: null,
            teacherId: null,
            studyCourseId: null
        },
    });

    useEffect(() => {
        dispatch(getAllSchedules({}))

        return () => {
            dispatch(setGetAllSchedules(null))
        }
    }, []);

    const handleSearch = (filters: ScheduleFilters) => {
        //@ts-ignore
        dispatch(getAllSchedules(Object.fromEntries(Object.entries(filters)
            .filter(([_, value]) => value)) as ScheduleFilters))
    }

    const handleDeleteSchedule = () => {
        setModal(null);
        dispatch(deleteSchedule(deleteScheduleId!, () => setDeleteScheduleId(null)));
    };

    const handleDeleteModalOpen = (id: number) => () => {
        setDeleteScheduleId(id);
        setModal('DeleteScheduleDialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteScheduleId(null);
        setModal(null);
    };

    const handleCreateModalOpen = () => {
        setModal('CreateScheduleDialog')
    };

    const handleCreateModalClose = () => {
        setModal(null)
    };

    const handleCreateDateModalOpen = (id: number) => () => {
        setModal('CreateDateScheduleDialog');
        setCreateDateScheduleId(id);
    }

    const handleCreateDateModalClose = () => {
        setModal(null)
        setCreateDateScheduleId(null);
    }

    const handleDeleteDateModalOpen = (id: number, dateSchedule: Array<{ id: number, date: string }>) => () => {
        setModal('DeleteDateScheduleDialog');
        setDeleteDateSchedule({
            id: id,
            dateSchedule: dateSchedule
        });
    }

    const handleDeleteDateModalClose = () => {
        setModal(null)
        setDeleteDateSchedule(null);
    }

    const handleUpdateModalOpen = (id: number) => () => {
        setUpdateScheduleId(id);
        setModal('UpdateScheduleDialog');
    };

    const handleUpdateModalClose = () => {
        setUpdateScheduleId(null);
        setModal(null);
    };

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Расписание</Typography>
                <Permission forRoles={['admin']}>
                    <Button
                        color='primary'
                        size='medium'
                        variant='contained'
                        onClick={handleCreateModalOpen}>
                        Создать расписание
                    </Button>
                </Permission>
            </Box>
            <form className={styles.filters} onSubmit={handleSubmit(handleSearch)}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='teacherId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllTeachersDropdownAPI}
                                    placeholder='Учитель'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='className'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Класс'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='studyCourseId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllCoursesDropdownAPI}
                                    placeholder='Курс'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='dateScheduleId'
                            render={({field}) => (
                                <ServerAutocomplete
                                    onChange={(_, value) => {
                                        field.onChange(value.id)
                                    }}
                                    getDataRequest={getAllDateSchedulesDropdown}
                                    placeholder='Расписание, привязано к дате'
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={8}></Grid>
                    <Grid item xs={10}></Grid>
                    <Grid item xs={2}>
                        <Button
                            fullWidth
                            type='submit'
                            color='primary'
                            variant='contained'>
                            Искать
                        </Button>
                    </Grid>
                </Grid>
            </form>
            {schedules && schedules.length ? (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table sx={{ minWidth: 700 }}>
                        <TableHead>
                            <TableRow>
                                <TableCell align='left' width='auto'>День недели</TableCell>
                                <TableCell align='left' width='auto'>Урок</TableCell>
                                <TableCell align='left' width='auto'>Учитель</TableCell>
                                <TableCell align='left' width='auto'>Класс</TableCell>
                                <TableCell align='left' width='auto'>Номер урока в расписании</TableCell>
                                <Permission forRoles={['admin']}>
                                    <TableCell align='left' width='auto'>Подвязать к дате</TableCell>
                                    <TableCell align='left' width='auto'>Отвязать от даты</TableCell>
                                    <TableCell align='left' width={70}>Редактировать</TableCell>
                                    <TableCell align='center' width={70}>Удалить</TableCell>
                                </Permission>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {schedules.map((schedule) => (
                                <TableRow
                                    key={schedule.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {/*@ts-ignore*/}
                                        {weekdayMap[schedule.weekday]}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {schedule.subject.name}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {`${schedule.teacher.lastName} ${schedule.teacher.firstName} ${schedule.teacher.patronymic}`}
                                    </TableCell>
                                    <TableCell>
                                        {schedule.studyCourse.studyClass.name}
                                    </TableCell>
                                    <TableCell component='th'>
                                        {schedule.lessonNumber}
                                    </TableCell>
                                    <Permission forRoles={['admin']}>
                                        <TableCell align='center'>
                                            <IconButton color='success' size='medium' onClick={handleCreateDateModalOpen(schedule.id)}>
                                                <EditIcon />
                                            </IconButton>
                                        </TableCell>
                                        <TableCell align='center'>
                                            <IconButton color='success' size='medium' onClick={handleDeleteDateModalOpen(schedule.id, schedule.dateSchedule)}>
                                                <EditIcon />
                                            </IconButton>
                                        </TableCell>
                                        <TableCell align='center'>
                                            <IconButton color='success' size='medium' onClick={handleUpdateModalOpen(schedule.id)}>
                                                <EditIcon />
                                            </IconButton>
                                        </TableCell>
                                        <TableCell align='center'>
                                            <IconButton color='error' size='medium' onClick={handleDeleteModalOpen(schedule.id)}>
                                                <DeleteIcon/>
                                            </IconButton>
                                        </TableCell>
                                    </Permission>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {schedules?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет расписаний в системе'/>
            )}
            {modal === 'DeleteScheduleDialog' && (
                <DeleteDialog
                    title='Удаление расписания'
                    body='Вы действительно хотите удалить это расписание?'
                    onDelete={handleDeleteSchedule}
                    onClose={handleDeleteModalClose}/>
            )}
            {modal === 'CreateScheduleDialog' && (
                <CreateScheduleDialog onClose={handleCreateModalClose} />
            )}
            {modal === 'CreateDateScheduleDialog' && (
                <CreateDateScheduleDialog onClose={handleCreateDateModalClose} scheduleId={createDateScheduleId!} />
            )}
            {modal === 'DeleteDateScheduleDialog' && (
                <DeleteDateScheduleDialog
                    onClose={handleDeleteDateModalClose}
                    scheduleId={deleteDateSchedule!.id}
                    dateSchedules={deleteDateSchedule!.dateSchedule} />
            )}
            {modal === 'UpdateScheduleDialog' && (
                <UpdateScheduleDialog
                    onClose={handleUpdateModalClose}
                    id={updateScheduleId!} />
            )}
        </Box>
    )
};

export default ScheduleList;