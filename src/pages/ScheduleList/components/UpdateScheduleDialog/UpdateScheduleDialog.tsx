import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, Grid, TextField} from '@mui/material';
import {Autocomplete} from '@mui/lab';
import lessonNumbers from '../../../../constants/lessonNumbers';
import {Controller, useForm} from 'react-hook-form';
import weekdays from '../../../../constants/weekdays';
import useStyles from './styles';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllSubjectsDropdown from '../../../../api/subject/getAllSubjectsDropdown';
import getAllYearsDropdown from '../../../../api/year/getAllYearsDropdown';
import getAllTeachersDropdownAPI from '../../../../api/teacher/getAllTeachersDropdown';
import getAllClassesDropdownAPI from '../../../../api/class/getAllClassesDropdown';
import {UpdateScheduleModel} from '../../types/UpdateScheduleModel';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import updateSchedule from '../../../../reducers/scheduleReducer/updateSchedule';

interface UpdateScheduleDialogProps {
    onClose: () => void,
    id: number;
}

const UpdateScheduleDialog = (props: UpdateScheduleDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<UpdateScheduleModel>({
        defaultValues: {
            subjectId: null,
            studyClassId: null,
            weekday: '',
            lessonNumber: null,
            teacherId: null,
            studyYearId: null,
        }
    })

    const handleUpdateSchedule = (data: UpdateScheduleModel) => {
        props.onClose();
        dispatch(updateSchedule(data, props.id))
    }

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Обновить расписание
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleUpdateSchedule)}>
                    <Controller
                        control={control}
                        name='lessonNumber'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={lessonNumbers}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Номер урока'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='weekday'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={weekdays}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='День недели'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='subjectId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllSubjectsDropdown}
                                placeholder='Предмет'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studyYearId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllYearsDropdown}
                                placeholder='Учебный год'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='teacherId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllTeachersDropdownAPI}
                                placeholder='Учитель'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='studyClassId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllClassesDropdownAPI}
                                placeholder='Класс'
                                className={styles.input}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Обновить
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default UpdateScheduleDialog;