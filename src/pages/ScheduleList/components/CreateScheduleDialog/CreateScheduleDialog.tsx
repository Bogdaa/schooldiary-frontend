import React from 'react';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {Controller, useForm} from 'react-hook-form';
import {CreateScheduleModel} from '../../types/CreateScheduleModel';
import createSchedule from '../../../../reducers/scheduleReducer/createSchedule';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import ServerAutocomplete from '../../../../components/shared/ServerAutocomplete';
import getAllTeachersDropdownAPI from '../../../../api/teacher/getAllTeachersDropdown';
import useStyles from './styles';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';
import subjects from '../../../../constants/subjects';
import {Autocomplete} from '@mui/lab';
import weekdays from '../../../../constants/weekdays';
import lessonNumbers from '../../../../constants/lessonNumbers';
import getAllCoursesDropdownAPI from '../../../../api/course/getAllCoursesDropdown';
import dayjs from 'dayjs';

interface CreateScheduleDialogProps {
    onClose: () => void;
}

const CreateScheduleDialog = (props: CreateScheduleDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<CreateScheduleModel>({
        defaultValues: {
            studyCourseId: null,
            teacherId: null,
            subjectName: '',
            date: dayjs().format('YYYY-MM-DD'),
            lessonNumber: null,
            weekday: null,
        }
    });

    const handleCreateSchedule = (data: CreateScheduleModel) => {
        props.onClose();
        dispatch(createSchedule(data));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Создать расписание
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateSchedule)}>
                    <Controller
                        control={control}
                        name='studyCourseId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllCoursesDropdownAPI}
                                placeholder='Курс'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='teacherId'
                        render={({field}) => (
                            <ServerAutocomplete
                                onChange={(_, value) => {
                                    field.onChange(value.id)
                                }}
                                getDataRequest={getAllTeachersDropdownAPI}
                                placeholder='Учитель'
                                className={styles.input}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='subjectName'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Предмет'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='date'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Дата'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='lessonNumber'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={lessonNumbers}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Номер урока'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Controller
                        control={control}
                        name='weekday'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.value)
                                }}
                                isOptionEqualToValue={(option, value) => option.label === value.label}
                                options={weekdays}
                                getOptionLabel={(option) => option.label}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='День недели'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CreateScheduleDialog;