import React from 'react';
import {DialogTitle, Dialog, DialogContent, TextField, Button} from '@mui/material';
import useStyles from '../CreateScheduleDialog/styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {Controller, useForm} from 'react-hook-form';
import dayjs from 'dayjs';
import createDateSchedule from '../../../../reducers/scheduleReducer/createDateSchedule';
import {DesktopDatePicker} from '@mui/x-date-pickers/DesktopDatePicker';

interface CreateDateScheduleDialogProps {
    onClose: () => void;
    scheduleId: number;
}

const CreateDateScheduleDialog = (props: CreateDateScheduleDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<{ date: string }>({
        defaultValues: {
            date: dayjs().format('YYYY-MM-DD')
        }
    });

    const handleCreateDateSchedule = (data: { date: string }) => {
        props.onClose();
        dispatch(createDateSchedule({...data, scheduleId: props.scheduleId}));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Подвязать раписание к дате
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateDateSchedule)}>
                    <Controller
                        control={control}
                        name='date'
                        render={({field}) => (
                            <DesktopDatePicker
                                {...field}
                                label='Дата'
                                inputFormat='YYYY/MM/DD'
                                renderInput={(params) => <TextField
                                    {...params}
                                    className={styles.input}
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    variant='outlined'
                                />}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Подвязать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    );
};

export default CreateDateScheduleDialog;