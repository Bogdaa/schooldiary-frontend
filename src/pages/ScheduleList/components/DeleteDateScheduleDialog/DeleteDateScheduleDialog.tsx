import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import useStyles from '../CreateScheduleDialog/styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import deleteDateSchedule from '../../../../reducers/scheduleReducer/deleteDateSchedule';
import {Autocomplete} from '@mui/lab';

interface DeleteDateScheduleDialogProps {
    onClose: () => void;
    scheduleId: number;
    dateSchedules: Array<{ id: number, date: string }>
}

const DeleteDateScheduleDialog = (props: DeleteDateScheduleDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<{ id: number | null }>({
        defaultValues: {
            id: null
        }
    });

    const handleDeleteDateSchedule = (data: { id: number | null }) => {
        props.onClose();
        dispatch(deleteDateSchedule({ id: data.id!, scheduleId: props.scheduleId}));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Отвязать раписание от даты
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleDeleteDateSchedule)}>
                    <Controller
                        control={control}
                        name='id'
                        render={({field}) => (
                            <Autocomplete
                                onChange={(_, value) => {
                                    field.onChange(value!.id)
                                }}
                                isOptionEqualToValue={(option, value) => option.date === value.date}
                                options={props.dateSchedules}
                                getOptionLabel={(option) => option.date}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Дата расписания'
                                        className={styles.input}
                                    />
                                )}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Отвязать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default DeleteDateScheduleDialog;