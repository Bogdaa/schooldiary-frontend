export interface UpdateScheduleModel {
    subjectId: number | null;
    studyClassId: number | null;
    weekday: string;
    lessonNumber: number | null;
    teacherId: number | null;
    studyYearId: number | null;
}