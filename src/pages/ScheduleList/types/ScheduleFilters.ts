export interface ScheduleFilters {
    className?: string;
    dateScheduleId?: number | null;
    teacherId?: number | null;
    studyCourseId?: number | null;
}