export interface GetScheduleModel {
    teacher: {
        id: number;
        firstName: string;
        lastName: string;
        patronymic: string;
    },
    subject: {
        id: number;
        name: string;
    },
    studyCourse: {
        id: number;
        studyClass: {
            id: number;
            name: string;
        }
    },
    dateSchedule: Array<{
        date: string;
        id: number;
        createdAt: string;
        updatedAt: string;
    }>,
    lessonNumber: number;
    weekday: string;
    id: number;
    createdAt: string;
    updatedAt: string;
}