export interface ListScheduleModel {
    id: number;
    lessonNumber: number;
    weekday: string;
    subject: {
        id: number;
        name: string;
    },
    dateSchedule: Array<{ id: number, date: string; }>,
    teacher: {
        id: number;
        firstName: string;
        lastName: string;
        patronymic: string;
    },
    studyCourse: {
        id: number;
        studyClass: {
            id: number;
            name: string;
        }
    }
}