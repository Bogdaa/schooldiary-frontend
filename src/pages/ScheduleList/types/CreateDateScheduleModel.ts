export interface CreateDateScheduleModel {
    scheduleId: number;
    date: string;
}