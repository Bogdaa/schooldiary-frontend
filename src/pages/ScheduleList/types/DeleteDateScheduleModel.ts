export interface DeleteDateScheduleModel {
    id: number;
    scheduleId: number;
}