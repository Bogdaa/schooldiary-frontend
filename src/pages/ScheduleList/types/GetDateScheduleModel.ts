export interface GetDateScheduleModel {
    date: string,
    id: number;
    createdAt: string;
    updatedAt: string;
}