export interface CreateScheduleModel {
    studyCourseId: number | null;
    teacherId: number | null;
    subjectName: string;
    date: string;
    lessonNumber: number | null;
    weekday: string | null;
}