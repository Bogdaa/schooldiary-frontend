import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, TextField} from '@mui/material';
import {Controller, useForm} from 'react-hook-form';
import useStyles  from './styles';
import {useAppDispatch} from '../../../../reducers/rootReducer';
import {CreateClassModel} from '../../types/CreateClassModel';
import createClass from '../../../../reducers/classReducer/createClass';

interface CreateClassDialogProps {
    onClose: () => void;
}

const CreateClassDialog = (props: CreateClassDialogProps) => {
    const styles = useStyles();
    const dispatch = useAppDispatch();

    const {control, handleSubmit} = useForm<CreateClassModel>({
        defaultValues: {
            name: ''
        },
    });

    const handleCreateClass = (data: CreateClassModel) => {
        props.onClose();
        dispatch(createClass(data));
    };

    return (
        <Dialog open={true} onClose={props.onClose}>
            <DialogTitle>
                Создать класс
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit(handleCreateClass)}>
                    <Controller
                        control={control}
                        name='name'
                        render={({field}) => (
                            <TextField
                                className={styles.input}
                                fullWidth
                                autoComplete='off'
                                size='small'
                                placeholder='Название'
                                variant='outlined'
                                {...field}
                            />
                        )}
                    />
                    <Button
                        className={`${styles.input} ${styles.submitButton}`}
                        fullWidth
                        type='submit'
                        color='primary'
                        variant='contained'>
                        Создать
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CreateClassDialog;