export interface GetClassModel {
    name: string,
    id: number,
    createdAt: string;
    updatedAt: string;
}