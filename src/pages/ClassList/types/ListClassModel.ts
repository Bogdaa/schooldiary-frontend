export interface ListClassModel {
    name: string;
    id: number;
    studyCourses: Array<any>;
}