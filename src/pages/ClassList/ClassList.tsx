import {
    Box,
    Button,
    Grid, IconButton,
    Paper,
    Table, TableBody, TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography
} from '@mui/material';
import React, {useEffect, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import useStyles from './styles';
import ClassFilters from './types/ClassFilters';
import DeleteIcon from '@mui/icons-material/Delete';
import NoItems from '../../components/UI/NoItems';
import {useAppDispatch, useAppSelector} from '../../reducers/rootReducer';
import getAllClasses, {setGetAllClasses} from '../../reducers/classReducer/getAllClasses';
import CreateClassDialog from './components/CreateClassDialog';
import DeleteDialog from '../../components/shared/DeleteDialog';
import deleteClass from '../../reducers/classReducer/deleteClass';

const ClassList = () => {
    const [modal, setModal] = useState<'CreateClassDialog' | 'DeleteClassDialog' | null>(null);
    const [deleteClassId, setDeleteClassId] = useState<number | null>(null);
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const classes = useAppSelector(s => s.class.dataList);

    const {control} = useForm<ClassFilters>({
        defaultValues: {
            name: '',
        },
    });

    useEffect(() => {
        dispatch(getAllClasses);

        return () => {
            dispatch(setGetAllClasses(null));
        };
    }, []);

    const handleCreateModalOpen = () => {
        setModal('CreateClassDialog');
    };

    const handleCreateModalClose = () => {
        setModal(null);
    };

    const handleDeleteClass = () => {
        setModal(null);
        dispatch(deleteClass(deleteClassId!, () => setDeleteClassId(null)));
    };

    const handleDeleteModalOpen = (id: number) => () => {
        setDeleteClassId(id);
        setModal('DeleteClassDialog');
    };

    const handleDeleteModalClose = () => {
        setDeleteClassId(null);
        setModal(null);
    };

    return (
        <Box>
            <Box display='flex' justifyContent='space-between'>
                <Typography variant='h4'>Классы</Typography>
                <Button
                    color='primary'
                    size='medium'
                    variant='contained'
                    onClick={handleCreateModalOpen}>
                    Создать класс
                </Button>
            </Box>
            <Box flexGrow={1} className={styles.filters}>
                <Grid container>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name='name'
                            render={({field}) => (
                                <TextField
                                    fullWidth
                                    autoComplete='off'
                                    size='small'
                                    placeholder='Название'
                                    variant='outlined'
                                    {...field}
                                />
                            )}
                        />
                    </Grid>
                </Grid>
            </Box>
            {classes && classes.length ? (
                <TableContainer component={Paper} className={styles.table} elevation={2}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell align='left' width='auto'>Название</TableCell>
                                <TableCell align='center' width={70}>Удалить</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {classes.map((clazz) => (
                                <TableRow
                                    key={clazz.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component='th'>
                                        {clazz.name}
                                    </TableCell>
                                    <TableCell align='center'>
                                        <IconButton color='error' size='medium' onClick={handleDeleteModalOpen(clazz.id)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : null}
            {classes?.length === 0 && (
                <NoItems
                    className={styles.noItemsIndicator}
                    text='У вас пока нет классов в системе'/>
            )}
            {modal === 'DeleteClassDialog' && (
                <DeleteDialog
                    title='Удаление класса'
                    body='Вы действительно хотите удалить этот класс?'
                    onDelete={handleDeleteClass}
                    onClose={handleDeleteModalClose}/>
            )}
            {modal === 'CreateClassDialog' && <CreateClassDialog onClose={handleCreateModalClose}/>}
        </Box>
    );
};

export default ClassList;