import React, {useEffect, useState} from 'react';
import {Route, Routes, Navigate, useNavigate} from 'react-router-dom';
import Authorization from './pages/Authorization';
import RequireAuth from './components/shared/PrivateRoutes';
import Layout from './components/shared/Layout';
import TeacherList from './pages/TeacherList';
import ClassList from './pages/ClassList';
import StudentList from './pages/StudentList';
import SubjectList from './pages/SubjectList';
import YearList from './pages/YearList';
import StudentCreatePage from './pages/StudentCreatePage/StudentCreatePage';
import StudyCourseList from './pages/StudyCourseList';
import ScheduleList from './pages/ScheduleList';
import MarksList from './pages/HomeworksList';
import {useAppDispatch, useAppSelector} from './reducers/rootReducer';
import checkToken from './reducers/authReducer/checkToken';
import Loading from './components/shared/Loading';
import GradesList from './pages/GradesList';
import NAList from './pages/NAList';
import {Typography} from '@mui/material';
import Permission from './components/shared/Permission';
import Main from './pages/Main';

const App = () => {
    const [loading, setLoading] = useState(true);
    const dispatch = useAppDispatch();
    const user = useAppSelector(s => s.auth.user);
    const navigate = useNavigate();

    useEffect(() => {
        if (document.cookie.includes('token')) {
            dispatch(checkToken(() => navigate('/')));
        }
        setLoading(false);
    }, []);

    if (loading) {
        return <Loading/>;
    }

    if (!user) {
        return (
            <Routes>
                <Route path='/' element={<Authorization/>}/>
            </Routes>
        );
    }

    return (
        <Routes>
            <Route path='/' element={<Layout/>}>
                <Route path='/' element={
                    <Main />
                }/>
                <Route path='/teachers' element={
                    <RequireAuth forRoles={['admin']}><TeacherList/></RequireAuth>
                }/>
                <Route path='/classes' element={
                    <RequireAuth forRoles={['admin']}><ClassList/></RequireAuth>
                }/>
                <Route path='/students' element={
                    <RequireAuth forRoles={['admin']}><StudentList/></RequireAuth>
                }/>
                <Route path='/students/new' element={
                    <RequireAuth forRoles={['admin']}><StudentCreatePage/></RequireAuth>
                }/>
                <Route path='/subjects' element={
                    <RequireAuth forRoles={['admin']}><SubjectList/></RequireAuth>
                }/>
                <Route path='/years' element={
                    <RequireAuth forRoles={['admin']}><YearList/></RequireAuth>
                }/>
                <Route path='/study-courses' element={
                    <RequireAuth forRoles={['admin']}><StudyCourseList/></RequireAuth>
                }/>
                <Route path='/schedules' element={
                    <RequireAuth><ScheduleList/></RequireAuth>
                }/>
                <Route path='/homeworks' element={
                    <RequireAuth><MarksList/></RequireAuth>
                }/>
                <Route path='/grades' element={
                    <RequireAuth><GradesList/></RequireAuth>
                }/>
                <Route path='/nas' element={
                    <RequireAuth><NAList/></RequireAuth>
                }/>
            </Route>
            <Route path='*' element={<Navigate to='/' replace/>}/>
        </Routes>
);
};

export default App;
