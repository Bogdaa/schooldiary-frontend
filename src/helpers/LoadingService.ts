import {debounce} from '@mui/material';
import {LOADING_SET_LOADING} from '../reducers/loadingReducer/setLoading';
import uniqueId from './uniqueId';
import store from '../reducers/rootReducer';

class LoadingService {
    private setOfRequests = new Set<string>();
    private setDebounceLoading = debounce((loading) => store.dispatch({
        type: LOADING_SET_LOADING,
        payload: loading
    }), 300)
    private preloaderStopTimer: any;

    start(): string {
        store.dispatch({
            type: LOADING_SET_LOADING,
            payload: true
        })
        const requestId = uniqueId();
        this.setOfRequests.add(requestId);
        this.stopPreloader(30 * 1000);
        return requestId;
    }

    stop(requestId: string): void {
        this.setOfRequests.delete(requestId);
        if (this.setOfRequests.size === 0){
            this.stopPreloader()
        }
    }

    stopPreloader(interval = 300): void {
        if (!!this.preloaderStopTimer) {
            clearTimeout(this.preloaderStopTimer);
        }
        this.preloaderStopTimer = setTimeout(() => {
            this.setOfRequests.clear();
            store.dispatch({
                type: LOADING_SET_LOADING,
                payload: false
            })
        }, interval);
    }
}

export default LoadingService;