import {createTheme} from '@mui/material';

const theme = createTheme({
    components: {
        MuiLink: {
            styleOverrides: {
                root: {
                    cursor: 'pointer',
                    textDecoration: 'none'
                }
            }
        },
        MuiDialog: {
            defaultProps: {
                transitionDuration: 0,
                maxWidth: 'sm',
                fullWidth: true
            },
            styleOverrides: {
                container: {
                    zIndex: 1
                }
            }
        },
        MuiAutocomplete: {
            defaultProps: {
                disableClearable: true
            }
        }
    }
});

export default theme;
