import React from 'react';
import {Link as MuiLink} from '@mui/material';
import {Link} from 'react-router-dom';

interface CustomLinkCellProps {
    linkText: string;
    to: string;
}

const CustomLinkCell = (props: CustomLinkCellProps) => {
    return (
        <MuiLink component={Link} to={props.to}>
            {props.linkText}
        </MuiLink>
    );
};

export default CustomLinkCell;
