import React from 'react';
import {Typography} from '@mui/material';

interface NoItemsProps {
    text: string,
    className: string;
}

const NoItems = (props: NoItemsProps) => {
    return (
        <Typography className={props.className} variant='body1' align='center'>{props.text}</Typography>
    );
};

export default NoItems;