import React from 'react';
import {Navigate} from 'react-router-dom';
import {useAppSelector} from '../../../reducers/rootReducer';
import UserType from '../../../types/UserType';

const PrivateRoutes = ({children, forRoles}: { children: JSX.Element, forRoles?: Array<UserType> }) => {
    const user = useAppSelector(s => s.auth.user);

    if (!user) {
        return <Navigate to='/login' replace/>;
    }

    if(user && forRoles && !forRoles.includes(user.role)) {
        return <Navigate to='/' replace />
    }

    return children;
};

export default PrivateRoutes;
