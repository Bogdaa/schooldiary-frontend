import {useEffect} from 'react';
import {useSnackbar, OptionsObject, SnackbarKey} from 'notistack';

type Notification = {
    invoke: (message: string, options: OptionsObject) => SnackbarKey,
    remove: (key: SnackbarKey) => void
}

const notification: Notification = {
    invoke: () => 1,
    remove: () => {
    }
};

const NotificationService = () => {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    useEffect(() => {
        notification.invoke = (message, options) => {
            return enqueueSnackbar(message, options);
        };

        notification.remove = (key) => {
            closeSnackbar(key);
        };
    }, []);
    return null;
};

export {NotificationService, notification};