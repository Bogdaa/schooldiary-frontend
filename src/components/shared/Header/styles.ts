import {makeStyles} from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    navMenu: {
        flexGrow: 1,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    navMenuLink: {
        '&&': {
            color: 'white',
            display: 'block'
        }
    }
}));

export default useStyles;
