import React, {useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {
    AppBar,
    Container,
    Toolbar,
    Box,
    Button, Typography, IconButton, Menu, MenuItem,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import useStyles from './styles';
import Permission from '../Permission';

const Header = () => {
    const styles = useStyles();
    const navigate = useNavigate();
    const [personMenuAnchorEl, setPersonMenuAnchorEl] = useState<null | HTMLElement>(null);

    const handleClickMenu = (event: React.MouseEvent<HTMLElement>) => {
        setPersonMenuAnchorEl(event.currentTarget);
    };

    const handleMenuClose = () => {
        setPersonMenuAnchorEl(null)
    }

    const handleClickMenuItem = (path: string) => () => {
        navigate(path);
        setPersonMenuAnchorEl(null);
    };

    return (
        <AppBar position='static'>
            <Container maxWidth='md'>
                <Toolbar disableGutters>
                    <Typography variant='h6'>
                        Schooldiary
                    </Typography>
                    <Box className={styles.navMenu}>
                        <Permission forRoles={['teacher', 'student']}>
                            <Button className={styles.navMenuLink} component={Link} to='/schedules'>
                                Расписания
                            </Button>
                            <Button className={styles.navMenuLink} component={Link} to='/homeworks'>
                                Домашние задания
                            </Button>
                            <Button className={styles.navMenuLink} component={Link} to='/grades'>
                                Оценки
                            </Button>
                            <Button className={styles.navMenuLink} component={Link} to='/nas'>
                                Пропуски
                            </Button>
                        </Permission>
                        <Permission forRoles={['admin']}>
                            <IconButton
                                className={styles.navMenuLink}
                                size='large'
                                color='inherit'
                                onClick={handleClickMenu}>
                                <MenuIcon />
                            </IconButton>
                            <Menu
                                anchorEl={personMenuAnchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(personMenuAnchorEl)}
                                onClick={handleMenuClose}
                            >
                                <MenuItem onClick={handleClickMenuItem('/teachers')}>Учителя</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/students')}>Ученики</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/classes')}>Классы</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/homeworks')}>ДЗ</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/subjects')}>Предметы</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/years')}>Периоды</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/study-courses')}>Курсы</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/schedules')}>Расписания</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/grades')}>Оценки</MenuItem>
                                <MenuItem onClick={handleClickMenuItem('/nas')}>Пропуски</MenuItem>
                            </Menu>
                        </Permission>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default Header;