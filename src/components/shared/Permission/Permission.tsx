import React, {PropsWithChildren, ReactFragment} from 'react';
import UserType from '../../../types/UserType';
import {useAppSelector} from '../../../reducers/rootReducer';
import {Navigate} from 'react-router-dom';

interface PermissionProps {
    forRoles: Array<UserType>
    children: React.ReactNode
}

const Permission = (props: PermissionProps): any => {
    const {
        forRoles,
        children
    } = props;
    const userRole = useAppSelector(s => s.auth.user!.role);
    const isPermitted = forRoles.includes(userRole as UserType);

    if(!isPermitted) {
        return null
    }

    return children;
}

export default Permission;