import React from 'react';
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    Button
} from '@mui/material';

interface DeleteDialogProps {
    title: string;
    body: string;
    onDelete: () => void,
    onClose: () => void,
}

const DeleteDialog = (props: DeleteDialogProps) => {
    return (
        <Dialog open={true} onClose={props.onClose} style={{zIndex: 1}} transitionDuration={0}>
            <DialogTitle>
                {props.title}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {props.body}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onDelete}>Да</Button>
                <Button onClick={props.onClose}>
                    Нет
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default DeleteDialog;