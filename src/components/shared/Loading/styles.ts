import {makeStyles} from '@mui/styles';

const useStyles = makeStyles(() => ({
    background: {
        position: 'fixed',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'rgba(0, 0, 0, .5)',
        zIndex: 10000
    },
    backgroundContainer: {
        position: 'relative',
        width: '100%',
        height: '100%'
    },
    loadingIndicator: {
        position: 'absolute',
        top: '50%',
        right: '50%',
        transform: 'transform(-50%, -50%)',
    }
}));

export default useStyles;