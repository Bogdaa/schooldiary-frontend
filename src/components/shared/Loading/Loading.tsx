import React from 'react';
import {Box, CircularProgress} from '@mui/material';
import useStyles from './styles';
import {useAppSelector} from '../../../reducers/rootReducer';

const Loading = () => {
    const styles = useStyles();
    const loading = useAppSelector(s => s.loading.isLoading);

    return loading ? (
        <Box className={styles.background}>
            <Box className={styles.backgroundContainer}>
                <CircularProgress
                    className={styles.loadingIndicator}
                    style={{color: 'white', width: '55px', height: '55px'}}
                />
            </Box>
        </Box>
    ) : null;
};

export default Loading;