import React, {SyntheticEvent, useEffect, useState} from 'react';
import {AutocompleteValue, CircularProgress, TextField} from '@mui/material';
import {Autocomplete} from '@mui/lab';

interface ServerAutocompleteProps {
    onChange: (event: SyntheticEvent, value: AutocompleteValue<any, undefined, undefined, undefined>) => void;
    getDataRequest: () => Promise<any[]>;
    placeholder: string;
    multiple?: boolean;
    fullWidth?: boolean;
    size?: 'small' | 'medium';
    className?: string;
}

const ServerAutocomplete = (props: ServerAutocompleteProps) => {
    const {
        onChange,
        getDataRequest,
        placeholder,
        fullWidth = true,
        size = 'small',
        className,
        multiple = false
    } = props;
    const [open, setOpen] = useState(false);
    const [options, setOptions] = useState<Array<any> | null>(null);
    const loading = open && !options;

    useEffect(() => {
        if(open) {
            getDataRequest().then(data => {
                setOptions([...data]);
            })
        }
    }, [open]);

    const handleOpen = () => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    }

    const autocompleteOptions = !options ? [] : options;

    return (
        <Autocomplete
            open={open}
            onOpen={handleOpen}
            onClose={handleClose}
            isOptionEqualToValue={(option, value) => option.label === value.label}
            getOptionLabel={(option) => option.label}
            options={autocompleteOptions}
            loading={loading}
            onChange={onChange}
            multiple={multiple}
            renderInput={(params) => (
                <TextField
                    {...params}
                    className={className}
                    placeholder={placeholder}
                    fullWidth={fullWidth}
                    size={size}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    )
}

export default ServerAutocomplete;