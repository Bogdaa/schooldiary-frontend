import React from 'react';
import {Outlet} from 'react-router-dom';
import {Box, Container} from '@mui/material';
import Header from '../Header';
import useStyles from './styles';
import Loading from '../Loading';

const Layout = () => {

    const styles = useStyles();

    return (
        <Box>
            <Loading/>
            <Box>
                <Header/>
                <main className={styles.mainContent}>
                    <Container maxWidth='md'>
                        <Outlet/>
                    </Container>
                </main>
            </Box>
        </Box>
    );
};

export default Layout;