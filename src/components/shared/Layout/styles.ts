import {makeStyles} from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    mainContent: {
        paddingTop: '25px'
    }
}));

export default useStyles;