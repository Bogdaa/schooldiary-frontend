import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter as Router} from 'react-router-dom';
import {ThemeProvider, CssBaseline} from '@mui/material';
import {Provider} from 'react-redux';
import {SnackbarProvider} from 'notistack';
import {NotificationService} from './components/shared/NotificationService';
import theme from './theme';
import store from './reducers/rootReducer';
import App from './App';
import {LocalizationProvider} from '@mui/x-date-pickers';
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';


const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <ThemeProvider theme={theme}>
        <CssBaseline/>
        <Router>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <Provider store={store}>
                    <SnackbarProvider
                        maxSnack={3}
                        autoHideDuration={2500}
                        anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
                        <NotificationService/>
                        <App/>
                    </SnackbarProvider>
                </Provider>
            </LocalizationProvider>
        </Router>
    </ThemeProvider>
);
