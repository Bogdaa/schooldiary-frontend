import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import deleteDateScheduleAPI from '../../api/dateSchedule/deleteDateSchedule';
import {DeleteDateScheduleModel} from '../../pages/ScheduleList/types/DeleteDateScheduleModel';

export const SCHEDULES_DELETE_DATE_SCHEDULE = '@schedules/DELETE_DATE_SCHEDULE';

export type SetDeleteDateSchedule = ActionCreator<typeof SCHEDULES_DELETE_DATE_SCHEDULE, DeleteDateScheduleModel>;

export const setDeleteDateSchedule = (payload: DeleteDateScheduleModel): SetDeleteDateSchedule => ({
    type: SCHEDULES_DELETE_DATE_SCHEDULE,
    payload
});

const deleteDateSchedule =
    (dateSchedule: DeleteDateScheduleModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteDateScheduleAPI(dateSchedule.id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setDeleteDateSchedule({
                        id: data,
                        scheduleId: dateSchedule.scheduleId
                    }));
                    notification.invoke(
                        'Расписание было создано отвязано от даты!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время отвязывания расписания от даты. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        }

export default deleteDateSchedule;
