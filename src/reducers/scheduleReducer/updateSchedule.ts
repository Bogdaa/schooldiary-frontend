import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import updateScheduleAPI from '../../api/schedule/updateSchedule';
import {UpdateScheduleModel} from '../../pages/ScheduleList/types/UpdateScheduleModel';
import {ListScheduleModel} from '../../pages/ScheduleList/types/ListScheduleModel';
import ActionCreator from '../../types/ActionCreator';

export const SCHEDULES_UPDATE_SCHEDULE = '@schedules/UPDATE_SCHEDULE';

export type SetUpdateSchedule = ActionCreator<typeof SCHEDULES_UPDATE_SCHEDULE, ListScheduleModel>;

export const setUpdateSchedule = (payload: ListScheduleModel): SetUpdateSchedule => ({
    type: SCHEDULES_UPDATE_SCHEDULE,
    payload
});

const updateSchedule =
    (schedule: UpdateScheduleModel, id: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            updateScheduleAPI(schedule, id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setUpdateSchedule({
                        id: data.id,
                        lessonNumber: data.lessonNumber,
                        weekday: data.weekday,
                        subject: data.subject,
                        dateSchedule: data.dateSchedule,
                        teacher: data.teacher,
                        studyCourse: data.studyCourse
                    }))
                    notification.invoke(
                        'Рапсписание было обновлено успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время обновления расписания. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default updateSchedule;