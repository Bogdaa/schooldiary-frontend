import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import createScheduleAPI from '../../api/schedule/createSchedule';
import {CreateScheduleModel} from '../../pages/ScheduleList/types/CreateScheduleModel';
import {ListScheduleModel} from '../../pages/ScheduleList/types/ListScheduleModel';

export const SCHEDULES_CREATE_SCHEDULE = '@schedules/CREATE_SCHEDULE';

export type SetCreateSchedule = ActionCreator<typeof SCHEDULES_CREATE_SCHEDULE, ListScheduleModel>;

export const setCreateSchedule = (payload: ListScheduleModel): SetCreateSchedule => ({
    type: SCHEDULES_CREATE_SCHEDULE,
    payload
});

const createSchedule =
    (course: CreateScheduleModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createScheduleAPI(course)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateSchedule({
                        id: data.id,
                        lessonNumber: data.lessonNumber,
                        weekday: data.weekday,
                        subject: {
                            id: data.subject.id,
                            name: data.subject.name,
                        },
                        dateSchedule: data.dateSchedule,
                        teacher: {
                            id: data.teacher.id,
                            firstName: data.teacher.firstName,
                            lastName: data.teacher.lastName,
                            patronymic: data.teacher.patronymic,
                        },
                        studyCourse: data.studyCourse
                    }));
                    notification.invoke(
                        'Расписание было создано успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания расписания. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        }

export default createSchedule;
