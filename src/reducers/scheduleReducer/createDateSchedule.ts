import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import createDateScheduleAPI from '../../api/dateSchedule/createDateSchedule';
import {CreateDateScheduleModel} from '../../pages/ScheduleList/types/CreateDateScheduleModel';

export const SCHEDULES_CREATE_DATE_SCHEDULE = '@schedules/CREATE_DATE_SCHEDULE';

export type SetCreateDateSchedule = ActionCreator<typeof SCHEDULES_CREATE_DATE_SCHEDULE, {
    date: string;
    id: number;
    scheduleId: number;
}>;

export const setCreateDateSchedule = (payload: any): SetCreateDateSchedule => ({
    type: SCHEDULES_CREATE_DATE_SCHEDULE,
    payload
});

const createDateSchedule =
    (dateSchedule: CreateDateScheduleModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createDateScheduleAPI(dateSchedule)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateDateSchedule({
                        id: data.id,
                        date: data.date,
                        scheduleId: dateSchedule.scheduleId
                    }));
                    notification.invoke(
                        'Расписание было создано подвязано к дате!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время подвязания расписания к дате. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        }

export default createDateSchedule;
