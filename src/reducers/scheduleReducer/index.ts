import produce from 'immer';
import {SCHEDULES_GET_ALL_SCHEDULES, SetGetAllSchedules} from './getAllSchedules';
import {SCHEDULE_DELETE_SCHEDULE, SetDeleteSchedule} from './deleteSchedule';
import {ListScheduleModel} from '../../pages/ScheduleList/types/ListScheduleModel';
import {SCHEDULES_CREATE_SCHEDULE, SetCreateSchedule} from './createSchedule';
import {SCHEDULES_UPDATE_SCHEDULE, SetUpdateSchedule} from './updateSchedule';
import {SCHEDULES_CREATE_DATE_SCHEDULE, SetCreateDateSchedule} from './createDateSchedule';
import {SCHEDULES_DELETE_DATE_SCHEDULE, SetDeleteDateSchedule} from './deleteDateSchedule';

interface ScheduleStateInterface {
    dataList: Array<ListScheduleModel> | null,
}

const initialState: ScheduleStateInterface = {
    dataList: null,
};

type ScheduleActions = SetGetAllSchedules | SetDeleteSchedule
    | SetCreateSchedule | SetCreateDateSchedule
    | SetDeleteDateSchedule | SetUpdateSchedule;

const scheduleReducer = produce((draft: ScheduleStateInterface, action: ScheduleActions) => {
    switch (action.type) {
        case SCHEDULES_GET_ALL_SCHEDULES:
            draft.dataList = action.payload;
            return;
        case SCHEDULES_CREATE_SCHEDULE:
            draft.dataList?.unshift(action.payload);
            return;
        case SCHEDULE_DELETE_SCHEDULE:
            const index = draft.dataList?.findIndex(teacher => teacher.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        case SCHEDULES_CREATE_DATE_SCHEDULE: {
            const scheduleIndex = draft.dataList?.findIndex(schedule => schedule.id === action.payload.scheduleId);
            draft.dataList![scheduleIndex!] = {
                ...draft.dataList![scheduleIndex!],
                dateSchedule: [
                    ...draft.dataList![scheduleIndex!].dateSchedule,
                    { id: action.payload.id, date: action.payload.date }
                ]
            }
            return;
        }
        case SCHEDULES_UPDATE_SCHEDULE: {
            const scheduleIndex = draft.dataList!.findIndex(schedule => schedule.id === action.payload.id)
            draft.dataList = [
                ...draft.dataList!.slice(0, scheduleIndex),
                action.payload,
                ...draft.dataList!.slice(scheduleIndex + 1)
            ]
            return;
        }
        case SCHEDULES_DELETE_DATE_SCHEDULE: {
            const scheduleIndex = draft.dataList?.findIndex(schedule => schedule.id === action.payload.scheduleId);
            const schedule = draft.dataList![scheduleIndex!];
            const dateScheduleIndex = schedule.dateSchedule.findIndex(dateSchedule => dateSchedule.id === action.payload.id)
            draft.dataList![scheduleIndex!] = {
                ...schedule,
                dateSchedule: [
                    ...schedule.dateSchedule.slice(0, dateScheduleIndex),
                    ...schedule.dateSchedule.slice(dateScheduleIndex + 1)
                ]
            }
            return;
        }
        default:
            return;
    }
}, initialState);

export default scheduleReducer;