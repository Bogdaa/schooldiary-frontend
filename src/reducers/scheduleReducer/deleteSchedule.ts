import {AppThunk} from '../../types/AppThunk';
import deleteScheduleAPI from '../../api/schedule/deleteSchedule';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';

export const SCHEDULE_DELETE_SCHEDULE = '@schedules/DELETE_SCHEDULE';

export type SetDeleteSchedule = ActionCreator<typeof SCHEDULE_DELETE_SCHEDULE, number>;

export const setDeleteSchedule = (payload: number): SetDeleteSchedule => ({
    type: SCHEDULE_DELETE_SCHEDULE,
    payload
});

const deleteSchedule =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch,getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteScheduleAPI(id)
                .then((id) => {
                    dispatch(setDeleteSchedule(id));
                    extraArgument.loadingService.stop(requestId);
                    onLoad();
                    notification.invoke(
                        'Расписание было удалёно успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления расписания. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteSchedule;