import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import getAllSchedulesAPI from '../../api/schedule/getAllSchedules';
import {notification} from '../../components/shared/NotificationService';
import {ListScheduleModel} from '../../pages/ScheduleList/types/ListScheduleModel';
import {ScheduleFilters} from '../../pages/ScheduleList/types/ScheduleFilters';
import getAllSchedulesForCurrentStudent from '../../api/schedule/getAllSchedulesForCurrentStudent';

export const SCHEDULES_GET_ALL_SCHEDULES = '@schedules/GET_ALL_SCHEDULES';

export type SetGetAllSchedules = ActionCreator<typeof SCHEDULES_GET_ALL_SCHEDULES, ListScheduleModel[] | null>;

export const setGetAllSchedules = (payload: ListScheduleModel[] | null): SetGetAllSchedules => ({
    type: SCHEDULES_GET_ALL_SCHEDULES,
    payload
});

const getAllSchedules = (filters: ScheduleFilters): AppThunk =>
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        (getState().auth.user!.role === 'student' ? getAllSchedulesForCurrentStudent() : getAllSchedulesAPI(filters))
            .then(data => {
                dispatch(setGetAllSchedules(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка учителей. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllSchedules;