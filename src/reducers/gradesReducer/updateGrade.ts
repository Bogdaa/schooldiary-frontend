import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import {ListGradeModel} from '../../pages/GradesList/types/ListGradeModel';
import ActionCreator from '../../types/ActionCreator';
import UpdateGradeModel from '../../pages/GradesList/types/UpdateGradeModel';
import updateGradeAPI from '../../api/grades/updateGrade';

export const GRADES_UPDATE_GRADES = '@grades/UPDATE_GRADE';

export type SetUpdateGrade = ActionCreator<typeof GRADES_UPDATE_GRADES, ListGradeModel>;

export const setUpdateGrade = (payload: ListGradeModel): SetUpdateGrade => ({
    type: GRADES_UPDATE_GRADES,
    payload
});

const updateGrade =
    (grade: UpdateGradeModel, id: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            updateGradeAPI(grade, id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setUpdateGrade({
                        id: data.id,
                        gradeType: data.gradeType,
                        dateSchedule: data.dateSchedule,
                        value: data.value,
                        student: data.student
                    }))
                    notification.invoke(
                        'Оценка была обновлена успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время обновления оценки. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default updateGrade;