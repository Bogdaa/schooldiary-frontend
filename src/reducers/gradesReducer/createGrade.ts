import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import createGradeAPI from '../../api/grades/createGrade';
import ActionCreator from '../../types/ActionCreator';
import {ListGradeModel} from '../../pages/GradesList/types/ListGradeModel';
import {CreateGradeModel} from '../../pages/GradesList/types/CreateGradeModel';

export const GRADE_CREATE_GRADE = '@grades/CREATE_GRADE';

export type SetCreateGrade = ActionCreator<typeof GRADE_CREATE_GRADE, ListGradeModel>;

export const setCreateGrade = (payload: ListGradeModel): SetCreateGrade => ({
    type: GRADE_CREATE_GRADE,
    payload
});

const createGrade =
    (grade: CreateGradeModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createGradeAPI(grade)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateGrade({
                        id: data.id,
                        gradeType: data.gradeType,
                        dateSchedule: data.dateSchedule,
                        value: data.value,
                        student: data.student
                    }))
                    notification.invoke(
                        'Дз было создано успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания дз. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default createGrade;