import produce from 'immer';
import {ListGradeModel} from '../../pages/GradesList/types/ListGradeModel';
import {GRADE_GET_ALL_GRADES, SetGetAllGrades} from './getAllGrades';
import {GRADES_DELETE_GRADE, SetDeleteGrade} from './deleteGrade';
import {GRADE_CREATE_GRADE, SetCreateGrade} from './createGrade';
import {HOMEWORK_UPDATE_HOMEWORK} from '../homeworkReducer/updateHomework';
import {GRADES_UPDATE_GRADES, SetUpdateGrade} from './updateGrade';

interface GradeStateInterface {
    dataList: Array<ListGradeModel> | null,
}

const initialState: GradeStateInterface = {
    dataList: null,
};

type GradeActions = SetGetAllGrades | SetDeleteGrade | SetCreateGrade | SetUpdateGrade;

const gradeReducer = produce((draft: GradeStateInterface, action: GradeActions) => {
    switch (action.type) {
        case GRADES_DELETE_GRADE:
            const index = draft.dataList?.findIndex(teacher => teacher.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        case GRADE_GET_ALL_GRADES:
            draft.dataList = action.payload;
            return;
        case GRADE_CREATE_GRADE:
            draft.dataList?.unshift(action.payload);
            return;
        case GRADES_UPDATE_GRADES:
            const gradeIndex = draft.dataList!.findIndex(grade => grade.id === action.payload.id)
            draft.dataList = [
                ...draft.dataList!.slice(0, gradeIndex),
                action.payload,
                ...draft.dataList!.slice(gradeIndex + 1)
            ]
            return;
        default:
            return;
    }
}, initialState);

export default gradeReducer;