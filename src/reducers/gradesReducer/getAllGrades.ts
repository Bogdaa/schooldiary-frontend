import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import getAllGradesAPI from '../../api/grades/getAllGrades';
import {ListGradeModel} from '../../pages/GradesList/types/ListGradeModel';
import {GradesFilter} from '../../pages/GradesList/types/GradesFilter';
import getAllGradesForCurrentStudent from '../../api/grades/getAllGradesForCurrentStudent';

export const GRADE_GET_ALL_GRADES = '@grades/GET_ALL_GRADES';

export type SetGetAllGrades = ActionCreator<typeof GRADE_GET_ALL_GRADES, ListGradeModel[] | null>;

export const setGetAllGrades = (payload: ListGradeModel[] | null): SetGetAllGrades => ({
    type: GRADE_GET_ALL_GRADES,
    payload
});

const getAllGrades = (filters: GradesFilter): AppThunk =>
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        (getState().auth.user!.role === 'student' ? getAllGradesForCurrentStudent() : getAllGradesAPI(filters))
            .then(data => {
                dispatch(setGetAllGrades(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка оценок. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllGrades;