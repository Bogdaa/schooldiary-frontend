import {AppThunk} from '../../types/AppThunk';
import deleteGradeAPI from '../../api/grades/deleteGrade';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';

export const GRADES_DELETE_GRADE = '@grades/DELETE_GRADE';

export type SetDeleteGrade = ActionCreator<typeof GRADES_DELETE_GRADE, number>;

export const setDeleteGrade = (payload: number): SetDeleteGrade => ({
    type: GRADES_DELETE_GRADE,
    payload
});

const deleteGrade =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteGradeAPI(id)
                .then((id) => {
                    dispatch(setDeleteGrade(id));
                    extraArgument.loadingService.stop(requestId);
                    onLoad();
                    notification.invoke(
                        'Оценка была удалена успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления оценки. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteGrade;