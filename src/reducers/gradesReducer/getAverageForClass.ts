import getAverageGradeForClassAPI from '../../api/grades/getAverageForClass';
import {ClassAverageGradeModel} from '../../pages/GradesList/types/ClassAverageGradeModel';
import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';

const getAverageForClass =
    (averageForClassModel: ClassAverageGradeModel): AppThunk => (dispatch, getState, extraArgument) => {
    const requestId = extraArgument.loadingService.start();
    getAverageGradeForClassAPI(averageForClassModel)
        .then(data => {
            extraArgument.loadingService.stop(requestId);
            notification.invoke(
                `Средний балл для данного класса: ${data.averageGrade}`,
                {variant: 'success'}
            );
        })
        .catch((e) => {
            extraArgument.loadingService.stop(requestId);
            notification.invoke(
                'Что то пошло не так во время получения среднего балла. Пожалуйста, повторите действие позже',
                {variant: 'error'}
            );
        });
}

export default getAverageForClass;