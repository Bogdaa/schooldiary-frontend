import {createStore, applyMiddleware, combineReducers, AnyAction} from 'redux';
import {TypedUseSelectorHook, useDispatch, useSelector} from 'react-redux';
import thunkMiddleware, {ThunkDispatch} from 'redux-thunk';
import LoadingService from '../helpers/LoadingService';
import authReducer from './authReducer';
import loadingReducer from './loadingReducer';
import teacherReducer from './teacherReducer';
import studentReducer from './studentReducer';
import classReducer from './classReducer';
import yearReducer from './yearReducer';
import subjectReducer from './subjectReducer';
import studyCourseReducer from './studyCourseReducer';
import scheduleReducer from './scheduleReducer';
import homeworkReducer from './homeworkReducer';
import gradeReducer from './gradesReducer';
import naReducer from './naReducer';

const rootReducer = combineReducers({
    auth: authReducer,
    loading: loadingReducer,
    teacher: teacherReducer,
    student: studentReducer,
    class: classReducer,
    year: yearReducer,
    subject: subjectReducer,
    studyCourse: studyCourseReducer,
    schedule: scheduleReducer,
    homework: homeworkReducer,
    grade: gradeReducer,
    na: naReducer
});

export type ExtraArgument = {
    loadingService: LoadingService
}

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware.withExtraArgument({
    loadingService: new LoadingService()
})));

export type RootState = ReturnType<typeof rootReducer>;
export type TypedDispatch = ThunkDispatch<RootState, any, AnyAction>;

export const useAppDispatch = () => useDispatch<TypedDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export default store;