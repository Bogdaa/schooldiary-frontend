import {AppThunk} from '../../types/AppThunk';
import createYearAPI from '../../api/year/createYear';
import {notification} from '../../components/shared/NotificationService';
import {CreateYearModel} from '../../pages/YearList/types/CreateYearModel';
import ActionCreator from '../../types/ActionCreator';
import {ListYearModel} from '../../pages/YearList/types/ListYearModel';

export const YEAR_CREATE_YEAR = '@years/CREATE_YEAR';

export type SetCreateYear = ActionCreator<typeof YEAR_CREATE_YEAR, ListYearModel>;

export const setCreateYear = (payload: ListYearModel): SetCreateYear => ({
    type: YEAR_CREATE_YEAR,
    payload
});

const createYear =
    (year: CreateYearModel, id: number | null): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createYearAPI(year)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateYear({
                        id: data.id,
                        startDate: data.start_date,
                        endDate: data.end_date
                    }));
                    notification.invoke(
                        'Период был создан успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания периода. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default createYear;