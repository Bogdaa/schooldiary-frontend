import {AppThunk} from '../../types/AppThunk';
import deleteYearAPI from '../../api/year/deleteYear';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';

export const YEAR_DELETE_YEAR = '@years/DELETE_YEAR';

export type SetDeleteYear = ActionCreator<typeof YEAR_DELETE_YEAR, number>;

export const setDeleteYear = (payload: number): SetDeleteYear => ({
    type: YEAR_DELETE_YEAR,
    payload
});

const deleteYear =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteYearAPI(id)
                .then((id) => {
                    dispatch(setDeleteYear(id));
                    extraArgument.loadingService.stop(requestId);
                    onLoad();
                    notification.invoke(
                        'Период был удалён успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления периода. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteYear;