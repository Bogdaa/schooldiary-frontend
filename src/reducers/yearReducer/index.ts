import produce from 'immer';
import {SetGetAllYears, YEAR_GET_ALL_YEARS} from './getAllYears';
import {SetCreateYear, YEAR_CREATE_YEAR} from './createYear';
import {ListYearModel} from '../../pages/YearList/types/ListYearModel';
import {SetDeleteYear, YEAR_DELETE_YEAR} from './deleteYear';
import {SetUpdateYear, YEAR_UPDATE_YEAR} from './updateYear';

interface YearStateInterface {
    dataList: Array<ListYearModel> | null,
}

const initialState: YearStateInterface = {
    dataList: null,
};

type YearActions = SetGetAllYears | SetCreateYear | SetDeleteYear | SetUpdateYear;

const yearReducer = produce((draft: YearStateInterface, action: YearActions) => {
    switch (action.type) {
        case YEAR_DELETE_YEAR:
            const index = draft.dataList?.findIndex(teacher => teacher.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        case YEAR_GET_ALL_YEARS:
            draft.dataList = action.payload;
            return;
        case YEAR_CREATE_YEAR:
            draft.dataList?.unshift(action.payload);
            return;
        case YEAR_UPDATE_YEAR:
            const yearIndex = draft.dataList!.findIndex(subject => subject.id === action.payload.id)
            draft.dataList = [
                ...draft.dataList!.slice(0, yearIndex),
                action.payload,
                ...draft.dataList!.slice(yearIndex + 1)
            ]
            return;
        default:
            return;
    }
}, initialState);

export default yearReducer;