import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import {ListYearModel} from '../../pages/YearList/types/ListYearModel';
import {CreateYearModel} from '../../pages/YearList/types/CreateYearModel';
import updateYearAPI from '../../api/year/updateYear';

export const YEAR_UPDATE_YEAR = '@years/UPDATE_YEAR';

export type SetUpdateYear = ActionCreator<typeof YEAR_UPDATE_YEAR, ListYearModel>;

export const setUpdateYear = (payload: ListYearModel): SetUpdateYear => ({
    type: YEAR_UPDATE_YEAR,
    payload
});

const updateYear =
    (subject: CreateYearModel, id: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            updateYearAPI(subject, id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setUpdateYear({
                        id: data.id,
                        startDate: data.start_date,
                        endDate: data.end_date
                    }))
                    notification.invoke(
                        'Период был обновлен успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время обновления периода. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default updateYear;