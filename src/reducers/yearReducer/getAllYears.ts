import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import getAllYearsAPI from '../../api/year/getAllYears';
import {ListYearModel} from '../../pages/YearList/types/ListYearModel';

export const YEAR_GET_ALL_YEARS = '@years/GET_ALL_YEARS';

export type SetGetAllYears = ActionCreator<typeof YEAR_GET_ALL_YEARS, ListYearModel[] | null>;

export const setGetAllYears = (payload: ListYearModel[] | null): SetGetAllYears => ({
    type: YEAR_GET_ALL_YEARS,
    payload
});

const getAllYears: AppThunk =
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        getAllYearsAPI()
            .then(data => {
                dispatch(setGetAllYears(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка периодов. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllYears;