import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import getAllClassesAPI from '../../api/class/getAllClasses';
import {notification} from '../../components/shared/NotificationService';
import {ListClassModel} from '../../pages/ClassList/types/ListClassModel';

export const CLASSES_GET_ALL_CLASSES = '@classes/GET_ALL_CLASSES';

export type SetGetAllClasses = ActionCreator<typeof CLASSES_GET_ALL_CLASSES, ListClassModel[] | null>;

export const setGetAllClasses = (payload: ListClassModel[] | null): SetGetAllClasses => ({
    type: CLASSES_GET_ALL_CLASSES,
    payload
});

const getAllClasses: AppThunk =
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        getAllClassesAPI()
            .then(data => {
                dispatch(setGetAllClasses(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка классов. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllClasses;