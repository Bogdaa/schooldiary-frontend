import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import createClassAPI from '../../api/class/createClass';
import {CreateClassModel} from '../../pages/ClassList/types/CreateClassModel';
import ActionCreator from '../../types/ActionCreator';
import {ListClassModel} from '../../pages/ClassList/types/ListClassModel';

export const CLASS_CREATE_CLASS = '@teachers/CREATE_CLASS';

export type SetCreateClass = ActionCreator<typeof CLASS_CREATE_CLASS, ListClassModel>;

export const setCreateClass = (payload: ListClassModel): SetCreateClass => ({
    type: CLASS_CREATE_CLASS,
    payload
});

const createClass =
    (clazz: CreateClassModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createClassAPI(clazz)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateClass({
                        id: data.id,
                        name: data.name,
                        studyCourses: []
                    }))
                    notification.invoke(
                        'Класс был создан успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    console.log(e);
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания класса. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default createClass;