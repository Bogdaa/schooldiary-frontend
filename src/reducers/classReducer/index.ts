import produce from 'immer';
import {SetGetAllClasses, CLASSES_GET_ALL_CLASSES} from './getAllClasses';
import {SetCreateClass, CLASS_CREATE_CLASS} from './createClass';
import {ListClassModel} from '../../pages/ClassList/types/ListClassModel';
import {CLASS_DELETE_CLASS, SetDeleteClass} from './deleteClass';

interface ClassStateInterface {
    dataList: Array<ListClassModel> | null
}

const initialState: ClassStateInterface = {
    dataList: null
}

type ClassActions = SetGetAllClasses | SetCreateClass | SetDeleteClass;

const classReducer = produce((draft: ClassStateInterface, action: ClassActions) => {
    switch (action.type) {
        case CLASSES_GET_ALL_CLASSES:
            draft.dataList = action.payload;
            break;
        case CLASS_CREATE_CLASS:
            draft.dataList?.unshift(action.payload);
            return;
        case CLASS_DELETE_CLASS:
            const index = draft.dataList?.findIndex(teacher => teacher.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        default:
            break;
    }
}, initialState);

export default classReducer;