import {AppThunk} from '../../types/AppThunk';
import deleteClassAPI from '../../api/class/deleteClass';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';

export const CLASS_DELETE_CLASS = '@classes/DELETE_CLASS';

export type SetDeleteClass = ActionCreator<typeof CLASS_DELETE_CLASS, number>;

export const setDeleteClass = (payload: number): SetDeleteClass => ({
    type: CLASS_DELETE_CLASS,
    payload
});

const deleteTeacher =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteClassAPI(id)
                .then((id) => {
                    dispatch(setDeleteClass(id));
                    extraArgument.loadingService.stop(requestId);
                    onLoad();
                    notification.invoke(
                        'Класс был удалён успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления класса. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteTeacher;