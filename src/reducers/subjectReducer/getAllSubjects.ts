import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import getAllSubjectsAPI from '../../api/subject/getAllSubjects';
import {notification} from '../../components/shared/NotificationService';
import {ListSubjectModel} from '../../pages/SubjectList/types/ListSubjectModel';
import {SubjectFilters} from '../../pages/SubjectList/types/SubjectFilters';

export const SUBJECT_GET_ALL_SUBJECTS = '@subjects/GET_ALL_SUBJECTS';

export type SetGetAllSubjects = ActionCreator<typeof SUBJECT_GET_ALL_SUBJECTS, ListSubjectModel[] | null>;

export const setGetAllSubjects = (payload: ListSubjectModel[] | null): SetGetAllSubjects => ({
    type: SUBJECT_GET_ALL_SUBJECTS,
    payload
});

const getAllSubjects = (filters: SubjectFilters): AppThunk =>
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        getAllSubjectsAPI(filters)
            .then(data => {
                dispatch(setGetAllSubjects(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка предметов. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllSubjects;