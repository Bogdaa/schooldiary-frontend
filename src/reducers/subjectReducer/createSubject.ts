import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import {CreateSubjectModel} from '../../pages/SubjectList/types/CreateSubjectModel';
import createSubjectAPI from '../../api/subject/createSubject';
import {ListSubjectModel} from '../../pages/SubjectList/types/ListSubjectModel';

export const SUBJECT_CREATE_SUBJECT = '@subjects/CREATE_SUBJECT';

export type SetCreateSubject = ActionCreator<typeof SUBJECT_CREATE_SUBJECT, ListSubjectModel>;

export const setCreateSubject = (payload: ListSubjectModel): SetCreateSubject => ({
    type: SUBJECT_CREATE_SUBJECT,
    payload
});

const createSubject =
    (subject: CreateSubjectModel, id: number | null): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createSubjectAPI(subject)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateSubject({
                        name: data.name,
                        id: data.id,
                        teachers: data.teachers.map(teacher => ({
                            id: teacher.id,
                            firstName: teacher.firstName,
                            lastName: teacher.lastName,
                            patronymic: teacher.patronymic
                        }))
                    }));
                    notification.invoke(
                        'Предмет был создан успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания предмета. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        }

export default createSubject;
