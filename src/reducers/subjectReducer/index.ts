import produce from 'immer';
import {ListSubjectModel} from '../../pages/SubjectList/types/ListSubjectModel';
import {SetGetAllSubjects, SUBJECT_GET_ALL_SUBJECTS} from './getAllSubjects';
import {SetCreateSubject, SUBJECT_CREATE_SUBJECT} from './createSubject';
import {SetDeleteSubject, SUBJECT_DELETE_SUBJECT} from './deleteSubject';
import {SetUpdateSubject, SUBJECT_UPDATE_SUBJECT} from './updateSubject';

interface SubjectStateInterface {
    dataList: Array<ListSubjectModel> | null,
}

const initialState: SubjectStateInterface = {
    dataList: null,
};

type SubjectActions = SetGetAllSubjects | SetCreateSubject | SetDeleteSubject | SetUpdateSubject;

const subjectReducer = produce((draft: SubjectStateInterface, action: SubjectActions) => {
    switch (action.type) {
        case SUBJECT_GET_ALL_SUBJECTS:
            draft.dataList = action.payload;
            return;
        case SUBJECT_CREATE_SUBJECT:
            draft.dataList?.unshift(action.payload);
            return;
        case SUBJECT_DELETE_SUBJECT:
            const index = draft.dataList?.findIndex(teacher => teacher.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        case SUBJECT_UPDATE_SUBJECT:
            const subjectIndex = draft.dataList!.findIndex(subject => subject.id === action.payload.id)
            draft.dataList = [
                ...draft.dataList!.slice(0, subjectIndex),
                action.payload,
                ...draft.dataList!.slice(subjectIndex + 1)
            ]
            return;
        default:
            return;
    }
}, initialState);

export default subjectReducer;