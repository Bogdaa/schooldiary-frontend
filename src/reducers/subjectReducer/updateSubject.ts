import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import updateSubjectAPI from '../../api/subject/updateSubject';
import {ListSubjectModel} from '../../pages/SubjectList/types/ListSubjectModel';
import ActionCreator from '../../types/ActionCreator';
import {CreateSubjectModel} from '../../pages/SubjectList/types/CreateSubjectModel';

export const SUBJECT_UPDATE_SUBJECT = '@subjects/UPDATE_SUBJECT';

export type SetUpdateSubject = ActionCreator<typeof SUBJECT_UPDATE_SUBJECT, ListSubjectModel>;

export const setUpdateSubject = (payload: ListSubjectModel): SetUpdateSubject => ({
    type: SUBJECT_UPDATE_SUBJECT,
    payload
});

const updateSubject =
    (subject: CreateSubjectModel, id: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            updateSubjectAPI(subject, id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setUpdateSubject({
                        id: data.id,
                        name: data.name,
                        teachers: data.teachers
                    }))
                    notification.invoke(
                        'Предмет был обновлен успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время обновления предмета. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default updateSubject;