import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import deleteSubjectAPI from '../../api/subject/deleteSubject';

export const SUBJECT_DELETE_SUBJECT = '@subjects/DELETE_SUBJECT';

export type SetDeleteSubject = ActionCreator<typeof SUBJECT_DELETE_SUBJECT, number>;

export const setDeleteSubject = (payload: number): SetDeleteSubject => ({
    type: SUBJECT_DELETE_SUBJECT,
    payload
});

const deleteSubject =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteSubjectAPI(id)
                .then((id) => {
                    dispatch(setDeleteSubject(id));
                    extraArgument.loadingService.stop(requestId);
                    onLoad();
                    notification.invoke(
                        'Предмет был удалён успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления предмета. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteSubject;