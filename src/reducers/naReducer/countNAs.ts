import countNAsAPI from '../../api/NAs/countNAs';
import {CountNAModel} from '../../pages/NAList/types/CountNAModel';
import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';

const countNAs =
    (countNAModel: CountNAModel): AppThunk => (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        countNAsAPI(countNAModel)
            .then(data => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    `Количество пропусков для данного студента: ${data[1]}`,
                    {variant: 'success'}
                );
            })
            .catch((e) => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения количества пропусков. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    }

export default countNAs;