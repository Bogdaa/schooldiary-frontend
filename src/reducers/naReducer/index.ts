import produce from 'immer';
import {NAS_GET_ALL_NAS, SetGetAllNAs} from './getAllNAs';
import {NAS_DELETE_NA, SetDeleteNA} from './deleteNA';
import {NAS_CREATE_NA, SetCreateNA} from './createNA';
import {ListNAModel} from '../../pages/NAList/types/ListNAModel';
import {NAS_UPDATE_NA, SetUpdateNA} from './updateNA';

interface NAsStateInterface {
    dataList: Array<ListNAModel> | null,
}

const initialState: NAsStateInterface = {
    dataList: null,
};

type NAsActions = SetGetAllNAs | SetDeleteNA | SetCreateNA | SetUpdateNA;

const naReducer = produce((draft: NAsStateInterface, action: NAsActions) => {
    switch (action.type) {
        case NAS_GET_ALL_NAS:
            draft.dataList = action.payload;
            return;
        case NAS_DELETE_NA:
            const index = draft.dataList?.findIndex(teacher => teacher.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        case NAS_CREATE_NA:
            draft.dataList?.unshift(action.payload);
            return;
        case NAS_UPDATE_NA:
            const naIndex = draft.dataList!.findIndex(na => na.id === action.payload.id)
            draft.dataList = [
                ...draft.dataList!.slice(0, naIndex),
                action.payload,
                ...draft.dataList!.slice(naIndex + 1)
            ]
            return;
        default:
            return;
    }
}, initialState);

export default naReducer;