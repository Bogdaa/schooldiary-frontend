import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import deleteNAAPI from '../../api/NAs/deleteNA';

export const NAS_DELETE_NA = '@NAs/DELETE_NA';

export type SetDeleteNA = ActionCreator<typeof NAS_DELETE_NA, number>;

export const setDeleteNA = (payload: number): SetDeleteNA => ({
    type: NAS_DELETE_NA,
    payload
});

const deleteNA =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteNAAPI(id)
                .then(id => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setDeleteNA(id));
                    onLoad();
                    notification.invoke(
                        'Пропуск был успешно удалён!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления пропуска. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        }

export default deleteNA;
