import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import updateNAAPI from '../../api/NAs/updateNA';
import {UpdateNAModel} from '../../pages/NAList/types/UpdateNAModel';
import {ListNAModel} from '../../pages/NAList/types/ListNAModel';
import ActionCreator from '../../types/ActionCreator';

export const NAS_UPDATE_NA = '@NAs/UPDATE_NA';

export type SetUpdateNA = ActionCreator<typeof NAS_UPDATE_NA, ListNAModel>;

export const setUpdateNA = (payload: ListNAModel): SetUpdateNA => ({
    type: NAS_UPDATE_NA,
    payload
});

const updateNA =
    (na: UpdateNAModel, id: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            updateNAAPI(na, id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setUpdateNA({
                        id: data.id,
                        reason: data.reason,
                        student: {
                            id: data.student.id,
                            firstName: data.student.firstName,
                            lastName: data.student.lastName,
                            patronymic: data.student.patronymic
                        },
                        dateSchedule: data.dateSchedule
                    }))
                    notification.invoke(
                        'Дз было обновлено успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время обновления дз. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default updateNA;