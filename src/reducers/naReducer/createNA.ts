import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import createNAAPI from '../../api/NAs/createNA';
import {CreateNAModel} from '../../pages/NAList/types/CreateNAModel';
import {ListNAModel} from '../../pages/NAList/types/ListNAModel';

export const NAS_CREATE_NA = '@NAs/CREATE_NA';

export type SetCreateNA = ActionCreator<typeof NAS_CREATE_NA, ListNAModel>;

export const setCreateNA = (payload: ListNAModel): SetCreateNA => ({
    type: NAS_CREATE_NA,
    payload
});

const createNA =
    (course: CreateNAModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createNAAPI(course)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateNA({
                        id: data.id,
                        reason: data.reason,
                        student: {
                            id: data.student.id,
                            firstName: data.student.firstName,
                            lastName: data.student.lastName,
                            patronymic: data.student.patronymic
                        },
                        dateSchedule: data.dateSchedule
                    }));
                    notification.invoke(
                        'Пропуск был создан успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания пропуска. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        }

export default createNA;
