import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import getAllNAsAPI from '../../api/NAs/getAllNAs';
import {notification} from '../../components/shared/NotificationService';
import {ListNAModel} from '../../pages/NAList/types/ListNAModel';
import {NAFilters} from '../../pages/NAList/types/NAFilters';
import getAllNAsForCurrentStudentAPI from '../../api/NAs/getAllNAsForCurrentStudent';

export const NAS_GET_ALL_NAS = '@NAs/GET_ALL_NAS';

export type SetGetAllNAs = ActionCreator<typeof NAS_GET_ALL_NAS, ListNAModel[] | null>;

export const setGetAllNAs = (payload: ListNAModel[] | null): SetGetAllNAs => ({
    type: NAS_GET_ALL_NAS,
    payload
});

const getAllNAs = (filters: NAFilters): AppThunk =>
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        (getState().auth.user!.role === 'student' ? getAllNAsForCurrentStudentAPI() : getAllNAsAPI(filters))
            .then(data => {
                dispatch(setGetAllNAs(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка пропусков. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllNAs;