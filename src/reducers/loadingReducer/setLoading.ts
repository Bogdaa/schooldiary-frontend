import ActionCreator from '../../types/ActionCreator';

export const LOADING_SET_LOADING = '@loading/SET_LOADING';

export type SetLoadingAction = ActionCreator<typeof LOADING_SET_LOADING, boolean>

export const setLoading = (payload: boolean): SetLoadingAction => ({
    type: LOADING_SET_LOADING,
    payload
});