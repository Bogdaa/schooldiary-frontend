import produce from 'immer';
import {LOADING_SET_LOADING, SetLoadingAction} from './setLoading';

interface LoadingStateInterface {
    isLoading: boolean;
}

const initialState: LoadingStateInterface = {
    isLoading: false,
};

const loadingReducer = produce((draft: LoadingStateInterface, action: SetLoadingAction) => {
    switch (action.type) {
        case LOADING_SET_LOADING:
            draft.isLoading = action.payload;
            return;
        default:
            return;
    }
}, initialState);

export default loadingReducer;