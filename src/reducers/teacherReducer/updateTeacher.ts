import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import updateTeacherAPI from '../../api/teacher/updateTeacher';
import {ListTeacherModel} from '../../pages/TeacherList/types/ListTeacherModel';
import ActionCreator from '../../types/ActionCreator';
import {UpdateTeacherModel} from '../../pages/TeacherList/types/UpdateTeacherModel';

export const TEACHER_UPDATE_TEACHER = '@teachers/UPDATE_TEACHER';

export type SetUpdateTeacher = ActionCreator<typeof TEACHER_UPDATE_TEACHER, ListTeacherModel>;

export const setUpdateTeacher = (payload: ListTeacherModel): SetUpdateTeacher => ({
    type: TEACHER_UPDATE_TEACHER,
    payload
});

const updateTeacher =
    (subject: UpdateTeacherModel, id: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            updateTeacherAPI(subject, id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setUpdateTeacher({
                        firstName: data.firstName,
                        lastName: data.lastName,
                        patronymic: data.patronymic,
                        id: data.id,
                        subjects: data.subjects
                    }))
                    notification.invoke(
                        'Учитель был обновлен успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время обновления учителя. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default updateTeacher;