import {AppThunk} from '../../types/AppThunk';
import deleteTeacherAPI from '../../api/teacher/deleteTeacher';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';

export const TEACHER_DELETE_TEACHER = '@teachers/DELETE_TEACHER';

export type SetDeleteTeacher = ActionCreator<typeof TEACHER_DELETE_TEACHER, number>;

export const setDeleteTeacher = (payload: number): SetDeleteTeacher => ({
    type: TEACHER_DELETE_TEACHER,
    payload
});

const deleteTeacher =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteTeacherAPI(id)
                .then((id) => {
                    dispatch(setDeleteTeacher(id));
                    extraArgument.loadingService.stop(requestId);
                    onLoad();
                    notification.invoke(
                        'Учитель был удалён успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления учителя. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteTeacher;