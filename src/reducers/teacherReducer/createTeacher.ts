import {AppThunk} from '../../types/AppThunk';
import createTeacherAPI from '../../api/teacher/createTeacher';
import {notification} from '../../components/shared/NotificationService';
import {CreateTeacherModel} from '../../pages/TeacherList/types/CreateTeacherModel';
import ActionCreator from '../../types/ActionCreator';
import {ListTeacherModel} from '../../pages/TeacherList/types/ListTeacherModel';

export const TEACHER_CREATE_TEACHER = '@teachers/CREATE_TEACHER';

export type SetCreateTeacher = ActionCreator<typeof TEACHER_CREATE_TEACHER, ListTeacherModel>;

export const setCreateTeacher = (payload: ListTeacherModel): SetCreateTeacher => ({
    type: TEACHER_CREATE_TEACHER,
    payload
});

const createTeacher =
    (teacher: CreateTeacherModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createTeacherAPI({
                user: {
                    email: teacher.email,
                    password: teacher.password,
                },
                teacher: {
                    firstName: teacher.firstName,
                    lastName: teacher.lastName,
                    patronymic: teacher.patronymic
                }
            })
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateTeacher({
                        firstName: data.teacher.firstName,
                        lastName: data.teacher.lastName,
                        patronymic: data.teacher.patronymic,
                        id: data.teacher.id,
                        subjects: []
                    }));
                    notification.invoke(
                        'Учитель был создан успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания учителя. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default createTeacher;