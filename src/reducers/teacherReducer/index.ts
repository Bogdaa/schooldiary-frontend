import produce from 'immer';
import {SetGetAllTeachers, TEACHER_GET_ALL_TEACHERS} from './getAllTeachers';
import {SetDeleteTeacher, TEACHER_DELETE_TEACHER} from './deleteTeacher';
import {SetCreateTeacher, TEACHER_CREATE_TEACHER} from './createTeacher';
import {SetUpdateTeacher, TEACHER_UPDATE_TEACHER} from './updateTeacher';
import {ListTeacherModel} from '../../pages/TeacherList/types/ListTeacherModel';

interface TeacherStateInterface {
    dataList: Array<ListTeacherModel> | null,
}

const initialState: TeacherStateInterface = {
    dataList: null,
};

type TeacherActions = SetGetAllTeachers | SetDeleteTeacher | SetCreateTeacher | SetUpdateTeacher;

const teacherReducer = produce((draft: TeacherStateInterface, action: TeacherActions) => {
    switch (action.type) {
        case TEACHER_GET_ALL_TEACHERS:
            draft.dataList = action.payload;
            return;
        case TEACHER_CREATE_TEACHER:
            draft.dataList?.unshift(action.payload);
            return;
        case TEACHER_DELETE_TEACHER:
            const index = draft.dataList?.findIndex(teacher => teacher.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        case TEACHER_UPDATE_TEACHER:
            const teacherIndex = draft.dataList!.findIndex(teacher => teacher.id === action.payload.id)
            draft.dataList = [
                ...draft.dataList!.slice(0, teacherIndex),
                action.payload,
                ...draft.dataList!.slice(teacherIndex + 1)
            ];
            return;
        default:
            return;
    }
}, initialState);

export default teacherReducer;