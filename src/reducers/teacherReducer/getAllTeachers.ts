import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import getAllTeachersAPI from '../../api/teacher/getAllTeachers';
import {notification} from '../../components/shared/NotificationService';
import {ListTeacherModel} from '../../pages/TeacherList/types/ListTeacherModel';
import {TeacherFilters} from '../../pages/TeacherList/types/TeacherFilters';

export const TEACHER_GET_ALL_TEACHERS = '@teachers/GET_ALL_TEACHERS';

export type SetGetAllTeachers = ActionCreator<typeof TEACHER_GET_ALL_TEACHERS, ListTeacherModel[] | null>;

export const setGetAllTeachers = (payload: ListTeacherModel[] | null): SetGetAllTeachers => ({
    type: TEACHER_GET_ALL_TEACHERS,
    payload
});

const getAllTeachers = (filters: TeacherFilters):AppThunk =>
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        getAllTeachersAPI(filters)
            .then(data => {
                dispatch(setGetAllTeachers(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка учителей. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllTeachers;