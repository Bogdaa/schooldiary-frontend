import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import createCourseAPI from '../../api/course/createCourse';
import {ListStudyCourseModel} from '../../pages/StudyCourseList/types/ListStudyCourseModel';
import {CreateCourseModel} from '../../pages/StudyCourseList/types/CreateCourseModel';

export const COURSE_CREATE_COURSE = '@courses/CREATE_COURSE';

export type SetCreateCourse = ActionCreator<typeof COURSE_CREATE_COURSE, any>;

export const setCreateCourse = (payload: ListStudyCourseModel): SetCreateCourse => ({
    type: COURSE_CREATE_COURSE,
    payload
});

const createCourse =
    (course: CreateCourseModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createCourseAPI(course)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateCourse({
                        id: data.id,
                        studyClass: {
                            id: data.studyClass.id,
                            name: data.studyClass.name
                        },
                        studyYear: {
                            id: data.studyYear.id,
                            startDate: data.studyYear.startDate,
                            endDate: data.studyYear.endDate
                        }
                    }));
                    notification.invoke(
                        'Курс был создан успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания курса. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        }

export default createCourse;
