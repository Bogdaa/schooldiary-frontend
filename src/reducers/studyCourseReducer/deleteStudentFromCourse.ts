import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';
import deleteStudentFromCourseAPI from '../../api/course/deleteStudentFromCourse';

export const COURSE_DELETE_STUDENT_FROM_COURSE = '@courses/DELETE_STUDENT_FROM_COURSE';

export type SetDeleteStudentFromCourse = ActionCreator<typeof COURSE_DELETE_STUDENT_FROM_COURSE, number>;

export const setDeleteStudentFromCourse = (payload: number): SetDeleteStudentFromCourse => ({
    type: COURSE_DELETE_STUDENT_FROM_COURSE,
    payload
});

const deleteStudentFromCourse =
    (studentId: number, studyCourseId: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteStudentFromCourseAPI(studentId, studyCourseId)
                .then(({ studyCourseId, studentId }) => {
                    dispatch(setDeleteStudentFromCourse(studyCourseId));
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Студент был удалён успешно из учебного курса!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления студента из учебного курса. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteStudentFromCourse;