import produce from 'immer';
import {COURSE_GET_ALL_COURSES, SetGetAllCourses} from './gelAllCourses';
import {COURSE_CREATE_COURSE, SetCreateCourse} from './createCourse';
import {ListStudyCourseModel} from '../../pages/StudyCourseList/types/ListStudyCourseModel';

interface StudyCourseInterface {
    dataList: Array<ListStudyCourseModel> | null,
}

const initialState: StudyCourseInterface = {
    dataList: null,
};

type CoursesActions = SetGetAllCourses | SetCreateCourse;

const studyCourseReducer = produce((draft: StudyCourseInterface, action: CoursesActions) => {
    switch (action.type) {
        case COURSE_GET_ALL_COURSES:
            draft.dataList = action.payload;
            return;
        case COURSE_CREATE_COURSE:
            draft.dataList?.unshift(action.payload);
            return;
        default:
            return;
    }
}, initialState);

export default studyCourseReducer;