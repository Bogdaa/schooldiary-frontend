import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import getAllCoursesAPI from '../../api/course/getAllCourses';
import {notification} from '../../components/shared/NotificationService';
import {ListStudyCourseModel} from '../../pages/StudyCourseList/types/ListStudyCourseModel';
import {StudyCourseFilters} from '../../pages/StudyCourseList/types/StudyCourseFilters';

export const COURSE_GET_ALL_COURSES = '@courses/GET_ALL_COURSES';

export type SetGetAllCourses = ActionCreator<typeof COURSE_GET_ALL_COURSES, ListStudyCourseModel[] | null>;

export const setGetAllCourses = (payload: ListStudyCourseModel[] | null): SetGetAllCourses => ({
    type: COURSE_GET_ALL_COURSES,
    payload
});

const getAllCourses = (filters: StudyCourseFilters): AppThunk =>
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        getAllCoursesAPI(filters)
            .then(data => {
                dispatch(setGetAllCourses(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка курсов. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllCourses;