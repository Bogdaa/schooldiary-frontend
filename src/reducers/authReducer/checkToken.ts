import {AppThunk} from '../../types/AppThunk';
import checkTokenAPI from '../../api/auth/checkToken';
import {setAuthLogin} from './login';

const checkToken = (onError: () => void): AppThunk =>
    (dispatch, getState, extraArgument) => {
    const requestId = extraArgument.loadingService.start();
    checkTokenAPI()
        .then(data => {
            dispatch(setAuthLogin({
                role: data.role,
                id: data.id,
                email: data.email
            }))
            extraArgument.loadingService.stop(requestId);
        })
        .catch((e) => {
            dispatch(setAuthLogin(null))
            extraArgument.loadingService.stop(requestId);
            onError();
        })
}

export default checkToken;