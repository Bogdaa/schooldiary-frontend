import ActionCreator from '../../types/ActionCreator';
import {User} from '../../types/User';
import {LoginModel} from '../../pages/Authorization/types/LoginModel';
import {AppThunk} from '../../types/AppThunk';
import loginAPI from '../../api/auth/login';
import {notification} from '../../components/shared/NotificationService';

export const AUTH_LOGIN = '@auth/LOGIN';

export type SetAuthLoginAction = ActionCreator<typeof AUTH_LOGIN, User | null>

export const setAuthLogin = (payload: User | null): SetAuthLoginAction => ({
    type: AUTH_LOGIN,
    payload
});

const login =
    (user: LoginModel, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            loginAPI(user)
                .then(data => {
                    dispatch(setAuthLogin(data.data));
                    onLoad();
                    extraArgument.loadingService.stop(requestId);
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время логина. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default login;