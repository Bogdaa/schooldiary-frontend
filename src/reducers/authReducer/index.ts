import produce from 'immer';
import {User} from '../../types/User';
import {AUTH_LOGIN, SetAuthLoginAction} from './login';

interface AuthStateInterface {
    user: User | null
}

const initialState: AuthStateInterface = {
    user: null
};

const authReducer = produce((draft: AuthStateInterface, action: SetAuthLoginAction) => {
    switch (action.type) {
        case AUTH_LOGIN:
            draft.user = action.payload;
            return;
        default:
            return;
    }
}, initialState);

export default authReducer;