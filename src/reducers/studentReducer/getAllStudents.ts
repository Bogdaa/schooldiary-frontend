import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import getAllStudentsAPI from '../../api/student/getAllStudents';
import {notification} from '../../components/shared/NotificationService';
import {Student} from '../../types/Student';
import StudentFilters from '../../pages/StudentList/types/StudentFilters';

export const STUDENTS_GET_ALL_STUDENTS = '@students/GET_ALL_STUDENTS';

export type SetGetAllTeachers = ActionCreator<typeof STUDENTS_GET_ALL_STUDENTS, Student[] | null>;

export const setGetAllStudents = (payload: Student[] | null): SetGetAllTeachers => ({
    type: STUDENTS_GET_ALL_STUDENTS,
    payload
});

const getAllStudents = (filters: StudentFilters): AppThunk =>
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        getAllStudentsAPI(filters)
            .then(data => {
                dispatch(setGetAllStudents(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка учителей. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllStudents;