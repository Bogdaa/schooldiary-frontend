import produce from 'immer';
import {Student} from '../../types/Student';
import {SetGetAllTeachers, STUDENTS_GET_ALL_STUDENTS} from './getAllStudents';
import {SetDeleteStudent, STUDENT_DELETE_STUDENT} from './deleteStudent';
import {SetUpdateStudent, STUDENT_UPDATE_STUDENT} from './updateStudent';

interface StudentStateInterface {
    dataList: Array<Student> | null,
}

const initialState: StudentStateInterface = {
    dataList: null,
};

type StudentActions = SetGetAllTeachers | SetDeleteStudent | SetUpdateStudent;

const studentReducer = produce((draft: StudentStateInterface, action: StudentActions) => {
    switch (action.type) {
        case STUDENTS_GET_ALL_STUDENTS:
            draft.dataList = action.payload;
            return;
        case STUDENT_DELETE_STUDENT:
            const index = draft.dataList?.findIndex(teacher => teacher.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        case STUDENT_UPDATE_STUDENT:
            const studentIndex = draft.dataList!.findIndex(student => student.id === action.payload.id)
            draft.dataList = [
                ...draft.dataList!.slice(0, studentIndex),
                action.payload,
                ...draft.dataList!.slice(studentIndex + 1)
            ]
            return;
        default:
            return;
    }
}, initialState);

export default studentReducer;