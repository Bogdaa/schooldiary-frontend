import {AppThunk} from '../../types/AppThunk';
import deleteStudentAPI from '../../api/student/deleteStudent';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';

export const STUDENT_DELETE_STUDENT = '@students/DELETE_STUDENT';

export type SetDeleteStudent = ActionCreator<typeof STUDENT_DELETE_STUDENT, number>;

export const setDeleteStudent = (payload: number): SetDeleteStudent => ({
    type: STUDENT_DELETE_STUDENT,
    payload
});

const deleteStudent =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch,getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteStudentAPI(id)
                .then((id) => {
                    dispatch(setDeleteStudent(id));
                    extraArgument.loadingService.stop(requestId);
                    onLoad();
                    notification.invoke(
                        'Студент был удалён успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления студента. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteStudent;