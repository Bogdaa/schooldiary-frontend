import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import updateStudentAPI from '../../api/student/updateStudent';
import ActionCreator from '../../types/ActionCreator';
import {Student} from '../../types/Student';
import {UpdateStudentModel} from '../../pages/StudentList/types/UpdateStudentModel';

export const STUDENT_UPDATE_STUDENT = '@students/UPDATE_STUDENT';

export type SetUpdateStudent = ActionCreator<typeof STUDENT_UPDATE_STUDENT, Student>;

export const setUpdateStudent = (payload: Student): SetUpdateStudent => ({
    type: STUDENT_UPDATE_STUDENT,
    payload
});

const updateStudent =
    (subject: UpdateStudentModel, id: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            updateStudentAPI(subject, id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setUpdateStudent({
                        id: data.id,
                        firstName: data.firstName,
                        studyCourses: data.studyCourses,
                        lastName: data.lastName,
                        patronymic: data.patronymic,
                        NAs: data.NAs,
                        grades: data.grades
                    }))
                    notification.invoke(
                        'Студент был обновлен успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время обновления студента. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default updateStudent;