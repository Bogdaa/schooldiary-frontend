import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import createStudentAPI from '../../api/student/createStudent';
import CreateStudentModel from '../../pages/StudentCreatePage/types/CreateStudentModel';

const createStudent =
    (student: CreateStudentModel, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createStudentAPI({
                user: {
                    email: student.email,
                    password: student.password,
                },
                student: {
                    firstName: student.firstName,
                    lastName: student.lastName,
                    patronymic: student.patronymic,
                    studyYearId: student.studyYearId!,
                    studyClassId: student.studyClassId!
                }
            })
                .then(data => {
                    onLoad();
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Студент был создан успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания студента. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default createStudent;