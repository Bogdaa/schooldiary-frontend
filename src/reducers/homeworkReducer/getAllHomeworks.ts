import ActionCreator from '../../types/ActionCreator';
import {AppThunk} from '../../types/AppThunk';
import getAllHomeworksAPI from '../../api/homework/getAllHomeworks';
import {notification} from '../../components/shared/NotificationService';
import {ListHomeworkModel} from '../../pages/HomeworksList/types/ListHomeworkModel';
import {HomeworksFilter} from '../../pages/HomeworksList/types/HomeworksFilter';
import getAllHomeworksForCurrentStudentAPI from '../../api/homework/getAllHomeworksForCurrentStudent';

export const HOMEWORKS_GET_ALL_HOMEWORKS = '@homeworks/GET_ALL_HOMEWORKS';

export type SetGetAllHomework = ActionCreator<typeof HOMEWORKS_GET_ALL_HOMEWORKS, ListHomeworkModel[] | null>;

export const setGetAllHomeworks = (payload: ListHomeworkModel[] | null): SetGetAllHomework => ({
    type: HOMEWORKS_GET_ALL_HOMEWORKS,
    payload
});

const getAllHomeworks = (filters: HomeworksFilter): AppThunk =>
    (dispatch, getState, extraArgument) => {
        const requestId = extraArgument.loadingService.start();
        (getState().auth.user!.role === 'student' ? getAllHomeworksForCurrentStudentAPI() : getAllHomeworksAPI(filters))
            .then(data => {
                dispatch(setGetAllHomeworks(data));
                extraArgument.loadingService.stop(requestId);
            })
            .catch(() => {
                extraArgument.loadingService.stop(requestId);
                notification.invoke(
                    'Что то пошло не так во время получения списка домашних заданий. Пожалуйста, повторите действие позже',
                    {variant: 'error'}
                );
            });
    };

export default getAllHomeworks;