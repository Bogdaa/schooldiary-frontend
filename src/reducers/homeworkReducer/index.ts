import produce from 'immer';
import {HOMEWORKS_GET_ALL_HOMEWORKS, SetGetAllHomework} from './getAllHomeworks';
import {HOMEWORK_DELETE_HOMEWORK, SetDeleteHomework} from './deleteHomework';
import {HOMEWORK_CREATE_HOMEWORK, SetCreateHomework} from './createHomework';
import {ListHomeworkModel} from '../../pages/HomeworksList/types/ListHomeworkModel';
import {HOMEWORK_UPDATE_HOMEWORK, SetUpdateHomework} from './updateHomework';

interface HomeworkStateInterface {
    dataList: Array<ListHomeworkModel> | null,
}

const initialState: HomeworkStateInterface = {
    dataList: null,
};

type HomeworkActions = SetGetAllHomework | SetDeleteHomework | SetCreateHomework | SetUpdateHomework;

const homeworkReducer = produce((draft: HomeworkStateInterface, action: HomeworkActions) => {
    switch (action.type) {
        case HOMEWORKS_GET_ALL_HOMEWORKS:
            draft.dataList = action.payload;
            return;
        case HOMEWORK_DELETE_HOMEWORK:
            const index = draft.dataList!.findIndex(homework => homework.id === action.payload);
            if (index! > -1) {
                draft.dataList = [...draft.dataList!.slice(0, index), ...draft.dataList!.slice(index! + 1)];
            }
            return;
        case HOMEWORK_UPDATE_HOMEWORK:
            const homeworkIndex = draft.dataList!.findIndex(homework => homework.id === action.payload.id)
            draft.dataList = [
                ...draft.dataList!.slice(0, homeworkIndex),
                action.payload,
                ...draft.dataList!.slice(homeworkIndex + 1)
            ]
            return;
        case HOMEWORK_CREATE_HOMEWORK:
            draft.dataList!.unshift(action.payload);
            return;
        default:
            return;
    }
}, initialState);

export default homeworkReducer;