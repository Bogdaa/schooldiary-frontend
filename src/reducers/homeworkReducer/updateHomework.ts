import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import updateHomeworkAPI from '../../api/homework/updateHomework';
import {UpdateHomeworkModel} from '../../pages/HomeworksList/types/UpdateHomeworkModel';
import {ListHomeworkModel} from '../../pages/HomeworksList/types/ListHomeworkModel';
import ActionCreator from '../../types/ActionCreator';

export const HOMEWORK_UPDATE_HOMEWORK = '@homeworks/UPDATE_HOMEWORK';

export type SetUpdateHomework = ActionCreator<typeof HOMEWORK_UPDATE_HOMEWORK, ListHomeworkModel>;

export const setUpdateHomework = (payload: ListHomeworkModel): SetUpdateHomework => ({
    type: HOMEWORK_UPDATE_HOMEWORK,
    payload
});

const updateHomework =
    (homework: UpdateHomeworkModel, id: number): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            updateHomeworkAPI(homework, id)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setUpdateHomework({
                        id: data.id,
                        deadline: data.deadline,
                        dateSchedule: data.dateSchedule,
                        description: data.description,

                    }))
                    notification.invoke(
                        'Дз было обновлено успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время обновления дз. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default updateHomework;