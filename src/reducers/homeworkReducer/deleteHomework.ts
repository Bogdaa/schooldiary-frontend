import {AppThunk} from '../../types/AppThunk';
import deleteHomeworkAPI from '../../api/homework/deleteHomework';
import {notification} from '../../components/shared/NotificationService';
import ActionCreator from '../../types/ActionCreator';

export const HOMEWORK_DELETE_HOMEWORK = '@homeworks/DELETE_HOMEWORK';

export type SetDeleteHomework = ActionCreator<typeof HOMEWORK_DELETE_HOMEWORK, number>;

export const setDeleteHomework = (payload: number): SetDeleteHomework => ({
    type: HOMEWORK_DELETE_HOMEWORK,
    payload
});

const deleteHomework =
    (id: number, onLoad: () => void): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            deleteHomeworkAPI(id)
                .then((id) => {
                    dispatch(setDeleteHomework(id));
                    extraArgument.loadingService.stop(requestId);
                    onLoad();
                    notification.invoke(
                        'Дз было удалено успешно!',
                        {variant: 'success'}
                    );
                })
                .catch(() => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время удаления дз. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default deleteHomework;