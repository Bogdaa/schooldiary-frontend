import {AppThunk} from '../../types/AppThunk';
import {notification} from '../../components/shared/NotificationService';
import createHomeworkAPI from '../../api/homework/createHomework';
import {CreateHomeworkModel} from '../../pages/HomeworksList/types/CreateHomeworkModel';
import {ListHomeworkModel} from '../../pages/HomeworksList/types/ListHomeworkModel';
import ActionCreator from '../../types/ActionCreator';

export const HOMEWORK_CREATE_HOMEWORK = '@homeworks/CREATE_HOMEWORK';

export type SetCreateHomework = ActionCreator<typeof HOMEWORK_CREATE_HOMEWORK, ListHomeworkModel>;

export const setCreateHomework = (payload: ListHomeworkModel): SetCreateHomework => ({
    type: HOMEWORK_CREATE_HOMEWORK,
    payload
});

const createHomework =
    (homework: CreateHomeworkModel): AppThunk =>
        (dispatch, getState, extraArgument) => {
            const requestId = extraArgument.loadingService.start();
            createHomeworkAPI(homework)
                .then(data => {
                    extraArgument.loadingService.stop(requestId);
                    dispatch(setCreateHomework({
                        id: data.id,
                        deadline: data.deadline,
                        dateSchedule: data.dateSchedule,
                        description: data.description,
                    }))
                    notification.invoke(
                        'Дз было создано успешно!',
                        {variant: 'success'}
                    );
                })
                .catch((e) => {
                    extraArgument.loadingService.stop(requestId);
                    notification.invoke(
                        'Что то пошло не так во время создания дз. Пожалуйста, повторите действие позже',
                        {variant: 'error'}
                    );
                });
        };

export default createHomework;