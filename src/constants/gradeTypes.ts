const gradeTypes = [
    { value: 'Homework', label: 'ДЗ' },
    { value: 'Behavior', label: 'Поведение'},
    { value: 'Classwork', label: 'Классная работа'},
    { value: 'Test', label: 'Тест' },
    { value: 'Quarterly', label: 'Четверть'},
    { value: 'Годовая', label: 'Yearly'}
]

export default gradeTypes;