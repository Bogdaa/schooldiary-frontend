const roleMap = {
    student: 'cтудент',
    admin: 'админ',
    teacher: 'учитель'
}

export default roleMap;