const gradeTypeMap = {
    'Homework': 'ДЗ',
    'Behaviour': 'Поведение',
    'Classwork': 'Классная работа',
    'Test': 'Тест',
    'Quarterly': 'Четверть',
    'Yearly': 'Годовая',
}

export default gradeTypeMap;