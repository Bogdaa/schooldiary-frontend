const subjects = [
    {
        title: 'Физика',
        value: 'physics'
    },
    {
        title: 'Химия',
        value: 'chemistry'
    },
    {
        title: 'География',
        value: 'geography'
    },
    {
        title: 'Алгебра',
        value: 'algebra'
    },
    {
        title: 'Геометрия',
        value: 'geometry'
    },
    {
        title: 'Английский',
        value: 'english'
    },
    {
        title: 'Украинский',
        value: 'ukrainian'
    }
];

export default subjects;