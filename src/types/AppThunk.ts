import {ThunkAction} from 'redux-thunk';
import {Action} from 'redux';
import {ExtraArgument, RootState} from '../reducers/rootReducer';

export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
    RootState,
    ExtraArgument,
    Action<string>>;
