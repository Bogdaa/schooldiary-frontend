export default interface ActionCreator<T, G> {
    type: T;
    payload: G;
}