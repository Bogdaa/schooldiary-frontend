import UserType from './UserType';
import {Teacher} from './Teacher';
import {Student} from './Student';

export type User = {
    [index in UserType]?: Teacher | Student;
} & {
    id: number;
    email: string;
    password?: string;
    role: UserType;
};