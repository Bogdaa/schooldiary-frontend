export interface Student {
    id: number;
    firstName: string;
    lastName: string;
    patronymic: string;
    studyCourses: Array<any>
    grades: Array<any>
    NAs: Array<any>
}