type UserType = 'teacher' | 'student' | 'admin';

export default UserType;