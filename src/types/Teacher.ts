export interface Teacher {
    id: number;
    first_name: string;
    last_name: string;
    patronymic: string;
    teacher_subjects: Array<any>;
}